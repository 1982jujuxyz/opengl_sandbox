if _ACTION == "clean" then
  os.rmdir("../bin")
  os.rmdir("./projects")
  os.rmdir("./ipch")
  os.rmdir(".vs")
  os.rmdir("Debug")
  os.remove("OpenGLSandbox.sln")
  os.remove("OpenGLSandbox.sdf")
  os.remove("OpenGLSandbox.suo")
  os.remove("OpenGLSandbox.v11.suo")
  os.remove("OpenGLSandbox.v12.suo")
  os.remove("Application.sln")
  os.remove("Application.sdf")
  os.remove("Application.suo")
  os.remove("Application.v11.suo")
  os.remove("Application.v12.suo")
  os.remove("*.DB")
  os.exit()
end

--------- Solution ----------
solution "OpenGLSandbox"
  startproject "Application"

  --------- Application -----------
  configurations { "Debug", "Release" }
  platforms {"x64"}
  toolset("msc-llvm")
  project("Application")
    systemversion("latest")
    targetname("Application")
    kind "ConsoleApp"
    language "C++"
    cppdialect "c++17"
    location "projects"
    pchsource "../files/application/src/Precompiled.cpp"
    pchheader "Precompiled.h"
    includedirs { "../files/application/inc", "../dep", "../files/engine/inc", "../files/scene/inc", "../files/math/inc", "../files/common/inc", "../files/input/inc" }
    libdirs { "../dep/glfw-3.2.1", "../dep/glew-2.1.0", "../dep/AntTweakBar-1.16"}
    links { "opengl32", "glfw3", "glew32", "AntTweakBar64", "Engine", "Scene", "Math", "Common", "Input" }
    files { "../files/application/inc/**.h", "../files/application/src/**.cpp" }

    configuration "Debug"
      libdirs {"../bin/debug" }
      -- debugdir "path"
      targetdir "../bin/debug"
      symbols "On"
      postbuildcommands {
        "copy ..\\..\\dep\\glew-2.1.0\\glew32.dll ..\\..\\bin\\debug\\",
        "copy ..\\..\\dep\\AntTweakBar-1.16\\AntTweakBar64.dll ..\\..\\bin\\debug\\",
        "copy ..\\..\\dep\\glfw-3.2.1\\glfw3.dll  ..\\..\\bin\\debug\\"
      }
    
   
    -- configuration "ReleaseSymbols"
    --   targetdir "../bin/release"
    --   defines { "NDEBUG", "ASSET_PATH=\"../../assets/\"" }
    --   optimize "On"
    --   symbols "On"
    --   linkoptions { "/LTCG" } -- allows whole-program optimization
    --   postbuildcommands {
    --     "copy ..\\..\\dep\\glew-2.1.0\\glew32.dll ..\\..\\bin\\release\\",
    --     "copy ..\\..\\dep\\AntTweakBar-1.16\\AntTweakBar64.dll ..\\..\\bin\\release\\",
    --     "copy ..\\..\\dep\\glfw-3.2.1\\glfw3.dll  ..\\..\\bin\\release\\" }

    configuration "Release"
      libdirs {"../bin/release" }
      targetdir "../bin/release"
      optimize "On"
      linkoptions { "/LTCG" } -- allows whole-program optimization
      postbuildcommands {
        "copy ..\\..\\dep\\GLEW\\glew32.dll ..\\..\\bin\\release\\",
        "copy ..\\..\\dep\\AntTweakBar-1.16\\AntTweakBar64.dll ..\\..\\bin\\release\\",
        "copy ..\\..\\dep\\glfw-3.2.1\\glfw3.dll ..\\..\\bin\\release\\" }
  ---------------------------------

  --------- Common ----------------
  configurations { "Debug", "Release" }
  platforms {"x64"}
  toolset("msc-llvm")
  project("Common")
    systemversion("latest")
    targetname("Common")
    kind "StaticLib"
    language "C++"
    cppdialect "c++17"
    location "projects"
    pchsource "../files/common/src/Precompiled.cpp"
    pchheader "Precompiled.h"
    includedirs { "../files/common/inc" }
    files { "../files/common/inc/**.h", "../files/common/src/**.cpp" }

    configuration "Debug"
      libdirs {"../bin/debug" }
      targetdir "../bin/debug"
      defines { "_DEBUG"}
      symbols "On"

    -- configuration "ReleaseSymbols"
    --   targetdir "../bin/release"
    --   defines { "NDEBUG", "ASSET_PATH=\"../../assets/\"" }
    --   optimize "On"
    --   symbols "On"
    --   linkoptions { "/LTCG" } -- allows whole-program optimization
    
    configuration "Release"
      libdirs {"../bin/release" }
      targetdir "../bin/release"
      defines { "NDEBUG" }
      optimize "On"
      --linkoptions { "/LTCG" } -- allows whole-program optimization
  ---------------------------------

  --------- Engine Lib ------------
  configurations { "Debug", "Release" }
  platforms {"x64"}
  toolset("msc-llvm")
  project("Engine")
    systemversion("latest")
    targetname("Engine")
    kind "StaticLib"
    language "C++"
    cppdialect "c++17"
    location "projects"
    pchsource "../files/engine/src/Precompiled.cpp"
    pchheader "Precompiled.h"
    includedirs { "../files/engine/inc", "../files/math/inc", "../files/loader/inc", "../files/opengl/inc", "../files/common/inc", "../files/physics/inc",  "../files/scene/inc", "../files/input/inc" }
    files { "../files/engine/inc/**.h", "../files/engine/src/**.cpp" }
    links { "Math", "OpenGl", "Loader", "Common", "Physics", "Scene", "Input"}

    configuration "Debug"
      libdirs {"../bin/debug" }
      targetdir "../bin/debug"
      defines { "_DEBUG"}
      symbols "On"

    -- configuration "ReleaseSymbols"
    --   targetdir "../bin/release"
    --   defines { "NDEBUG", "ASSET_PATH=\"../../assets/\"" }
    --   optimize "On"
    --   symbols "On"
    --   linkoptions { "/LTCG" } -- allows whole-program optimization
    
    configuration "Release"
      libdirs {"../bin/release" }
      targetdir "../bin/release"
      defines { "NDEBUG" }
      optimize "On"
      linkoptions { "/LTCG" } -- allows whole-program optimization
  ---------------------------------

  --------- Input Handler ----------
  configurations { "Debug", "Release" }
  platforms {"x64"}
  toolset("msc-llvm")
  project("Input")
      systemversion("latest")
      targetname("input")
      kind "StaticLib"
      language "C++"
      cppdialect "c++17"
      location "projects"
      pchsource "../files/input/src/Precompiled.cpp"
      pchheader "Precompiled.h"
      includedirs { "../files/input/inc" }
      files { "../files/input/inc/**.h", "../files/input/src/**.cpp" }

      configuration "Debug"
        targetdir "../bin/debug"
        defines { "_DEBUG"}
        symbols "On"

      -- configuration "ReleaseSymbols"
      --   targetdir "../bin/release"
      --   defines { "NDEBUG", "ASSET_PATH=\"../../assets/\"" }
      --   optimize "On"
      --   symbols "On"
      --   linkoptions { "/LTCG" } -- allows whole-program optimization
      
      configuration "Release"
        defines { "NDEBUG" }
        optimize "On"
        linkoptions { "/LTCG" } -- allows whole-program optimization
  ---------------------------------

  --------- Asset Loader ----------
  configurations { "Debug", "Release" }
  platforms {"x64"}
  toolset("msc-llvm")
  project("Loader")
      systemversion("latest")
      targetname("loader")
      kind "StaticLib"
      language "C++"
      cppdialect "c++17"
      location "projects"
      pchsource "../files/loader/src/Precompiled.cpp"
      pchheader "Precompiled.h"
      includedirs { "../files/loader/inc" }
      files { "../files/loader/inc/**.h", "../files/loader/src/**.cpp" }

      configuration "Debug"
        targetdir "../bin/debug"
        defines { "_DEBUG"}
        symbols "On"

      -- configuration "ReleaseSymbols"
      --   targetdir "../bin/release"
      --   defines { "NDEBUG", "ASSET_PATH=\"../../assets/\"" }
      --   optimize "On"
      --   symbols "On"
      --   linkoptions { "/LTCG" } -- allows whole-program optimization
      
      configuration "Release"
        defines { "NDEBUG" }
        optimize "On"
        linkoptions { "/LTCG" } -- allows whole-program optimization
  ---------------------------------

  ----------- Math Lib ------------
  configurations { "Debug", "Release" }
  platforms {"x64"}
  toolset("msc-llvm")
  project("Math")
    systemversion("latest")
    targetname("math")
    kind "StaticLib"
    language "C++"
    cppdialect "c++17"
    location "projects"
    pchsource "../files/math/src/Precompiled.cpp"
    pchheader "Precompiled.h"
    includedirs { "../files/math/inc" }
    files { "../files/math/inc/**.h", "../files/math/src/**.cpp" }

    configuration "Debug"
      targetdir "../bin/debug"
      defines { "_DEBUG"}
      symbols "On"

    -- configuration "ReleaseSymbols"
    --   targetdir "../bin/release"
    --   defines { "NDEBUG", "ASSET_PATH=\"../../assets/\"" }
    --   optimize "On"
    --   symbols "On"
    --   linkoptions { "/LTCG" } -- allows whole-program optimization
    
    configuration "Release"
      defines { "NDEBUG" }
      optimize "On"
      linkoptions { "/LTCG" } -- allows whole-program optimization
  ---------------------------------

  --------- Graphics Lib ----------
  configurations { "Debug", "Release" }
  platforms {"x64"}
  toolset("msc-llvm")
  project("OpenGL")
      systemversion("latest")
      targetname("opengl")
      kind "StaticLib"
      language "C++"
      cppdialect "c++17"
      location "projects"
      pchsource "../files/opengl/src/Precompiled.cpp"
      pchheader "Precompiled.h"
      includedirs { "../files/opengl/inc", "../dep",  "../files/math/inc", "../files/common/inc", "../files/scene/inc", "../files/loader/inc", }
      libdirs { "../dep/glfw-3.2.1", "../dep/glew-2.1.0", "../dep/stb-1.41" , "../dep/AntTweakBar-1.16"}
      links { "opengl32", "glfw3", "glew32",  "AntTweakBar64", "Math", "Common", "Scene", "Loader"}
      files { "../files/opengl/inc/**.h", "../files/opengl/src/**.cpp" }

      configuration "Debug"
        libdirs {"../bin/debug" }
        targetdir "../bin/debug"
        defines { "_DEBUG", "ASSET_PATH=\"../../assets/\"" }
        symbols "On"
        links {"AntTweakBar64"}

      -- configuration "ReleaseSymbols"
      --   targetdir "../bin/release"
      --   defines { "NDEBUG", "ASSET_PATH=\"../../assets/\"" }
      --   optimize "On"
      --   symbols "On"
      --   links {"AntTweakBar64"}
      --   linkoptions { "/LTCG" } -- allows whole-program optimization
      
      configuration "Release"
        libdirs {"../bin/release" }
        targetdir "../bin/release"
        defines { "NDEBUG", "ASSET_PATH=\"../../assets/\"" }
        optimize "On"
        links {"AntTweakBar64"}
        linkoptions { "/LTCG" } -- allows whole-program optimization
  ---------------------------------

  --------- Physics Lib -----------
  configurations { "Debug", "Release" }
  platforms {"x64"}
  toolset("msc-llvm")
  project("Physics")
    systemversion("latest")
    targetname("Physics")
    kind "StaticLib"
    language "C++"
    cppdialect "c++17"
    location "projects"
    pchsource "../files/physics/src/Precompiled.cpp"
    pchheader "Precompiled.h"
    includedirs { "../files/physics/inc", "../files/math/inc", "../files/loader/inc", "../files/common/inc", "../files/scene/inc" }
    files { "../files/physics/inc/**.h", "../files/physics/src/**.cpp" }
    links { "Math", "Loader", "Common", "Scene"}

    configuration "Debug"
      libdirs {"../bin/debug" }
      targetdir "../bin/debug"
      defines { "_DEBUG"}
      symbols "On"

    -- configuration "ReleaseSymbols"
    --   targetdir "../bin/release"
    --   defines { "NDEBUG", "ASSET_PATH=\"../../assets/\"" }
    --   optimize "On"
    --   symbols "On"
    --   linkoptions { "/LTCG" } -- allows whole-program optimization
    
    configuration "Release"
      libdirs {"../bin/release" }
      targetdir "../bin/release"
      defines { "NDEBUG" }
      optimize "On"
      linkoptions { "/LTCG" } -- allows whole-program optimization
  ---------------------------------

  --------- Scene System ----------
  configurations { "Debug", "Release" }
  platforms {"x64"}
  toolset("msc-llvm")
  project("Scene")
    systemversion("latest")
    targetname("Scene")
    kind "StaticLib"
    language "C++"
    cppdialect "c++17"
    location "projects"
    pchsource "../files/scene/src/Precompiled.cpp"
    pchheader "Precompiled.h"
    includedirs { "../files/scene/inc", "../files/loader/inc", "../files/math/inc", "../dep", "../files/common/inc", "../files/input/inc"}
    libdirs {"../dep/glfw-3.2.1", "../dep/glew-2.1.0", "../dep/AntTweakBar-1.16"}
    files { "../files/scene/inc/**.h", "../files/scene/src/**.cpp" }
    links { "Loader", "Math",  "glfw3", "glew32" , "AntTweakBar64",  "Common", "Input"}

    configuration "Debug"
      libdirs {"../bin/debug" }
      targetdir "../bin/debug"
      defines { "_DEBUG"}
      symbols "On"

    -- configuration "ReleaseSymbols"
    --   targetdir "../bin/release"
    --   defines { "NDEBUG", "ASSET_PATH=\"../../assets/\"" }
    --   optimize "On"
    --   symbols "On"
    --   linkoptions { "/LTCG" } -- allows whole-program optimization
    
    configuration "Release"
      libdirs {"../bin/release" }
      targetdir "../bin/release"
      defines { "NDEBUG" }
      optimize "On"
      linkoptions { "/LTCG" } -- allows whole-program optimization
  ---------------------------------
-----------------------------------


