/*!***************************************************************************************
\file       ShaderProgram.cpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      A CPU representation of the GPU program object that hold
			all shaders for pipline.
*****************************************************************************************/
#include "Precompiled.h"


//========Self Include==================================================================//
#include "rendering/ShaderProgram.h"
//========1st Party Includes============================================================//
#include "rendering/TypeData.h"
#include "Vector4.h"
#include "Vector3.h"
#include "Matrix4.h"
#include "Color.h"
#include "rendering/GLDebug.h"
#include "rendering/Shader.h"

//========3rd Party Includes============================================================//
//========Types=========================================================================//


//========Defines=======================================================================//
//========Static Deceleration===========================================================//

  ///////========================================================================///////
  ///////   Public                                                               ///////
  ///////========================================================================///////

	//==============================================================================//
	//        Constructor                                                           //
	//==============================================================================//
ShaderProgram::ShaderProgram() : m_handle(0)
{
}
//==============================================================================//
//        Destructor                                                            //
//==============================================================================//

//==============================================================================//
//        Getters & Setters                                                     //
//==============================================================================//
void ShaderProgram::setUniforms(const std::string  & p_name, Math::Vector4 p_vec4)
{
	// TODO(cody): store location for later use.
	int location = glGetUniformLocation(m_handle, p_name.c_str());
	if (location < 0)
		std::cout << "ShaderProgram: " << p_name << ": " << p_vec4.toStringPtr() << " loc: " << location;


	glUniform4fv(location, 1, p_vec4.toFloatPtr());
}

void ShaderProgram::setUniforms(const std::string  & p_name, Math::Color p_colr)
{
	// TODO(cody): store location for later use.

	int location = glGetUniformLocation(m_handle, p_name.c_str());
	if (location < 0)
		std::cout << "ShaderProgram: " << p_name << ": " << p_colr.toStringPtr() << " loc: " << location;
	glUniform4fv(location, 1, p_colr.toFloatPtr());
}

void ShaderProgram::setUniforms(const std::string & p_name, Math::Vector3 p_vec3)
{

	//TODO(cody) store location for later use.
	int location = glGetUniformLocation(m_handle, p_name.c_str());
	if (location < 0)
		std::cout << "ShaderProgram: " << p_name << ": " << p_vec3.toStringPtr() << " loc: " << location;
	//GLDebug::getLastError();
	glUniform3fv(location, 1, p_vec3.toFloatPtr());

}

void ShaderProgram::setUniforms(const std::string & p_name, Math::Matrix4 p_mat4)
{

	// TODO(cody): store location for later use.    
	int location = glGetUniformLocation(m_handle, p_name.c_str());
	if (location < 0)
		std::cout << "ShaderProgram: " << p_name << ": " << p_mat4.toStringPtr() << " loc: " << location;

	float * l_floatptr = p_mat4.toFloatPtr();
	//GLDebug::getLastError();
	glProgramUniformMatrix4fv(m_handle, location, 1, GL_TRUE, l_floatptr);
}

void ShaderProgram::setUniforms(std::string const & p_name, int p_value)
{
	// TODO(cody): store location for later use.    
	// uploads the raw integer value to the GPU
	int location = glGetUniformLocation(m_handle, p_name.c_str());
	if (location < 0)
		std::cout << "ShaderProgram: " << p_name << ": " << p_value << " loc: " << location;
	//GLDebug::getLastError();
	glUniform1i(location, p_value);
}

void ShaderProgram::setUniforms(std::string const & p_name, float p_value)
{
	// TODO(cody): store location for later use.    
	// uploads the raw integer value to the GPU
	int location = glGetUniformLocation(m_handle, p_name.c_str());
	if (location < 0)
		std::cout << "ShaderProgram: " << p_name << ": " << p_value << " loc: " << location;
	
	glUniform1f(location, p_value);
}
std::weak_ptr<Shader> ShaderProgram::getShader(ShaderType p_type)
{

	switch (p_type)
	{
	case ShaderType::enm_vertex:
		return m_VertexShader.lock();
		break;

	case ShaderType::enm_fragment:
		return m_fragmentShader.lock();
		break;
	default:
		return std::weak_ptr<Shader>();

	}
}

//////======================================================================//////
//////    Non-Static                                                        //////
/////     Functions                                                          /////
/////========================================================================/////
void ShaderProgram::process()
{
	//std::stringstream ssv;
	//ssv << ASSET_PATH << "shaders/" << m_VertexShader.getFileName();
	//std::stringstream ssf;
	//ssf << ASSET_PATH << "shaders/" << m_fragmentShader.getFileName();

	//m_VertexShader.setSourceData();
	//m_fragmentShader.setSourceData(ReadInFile(ssf.str()));
}

void ShaderProgram::build()
{
	// create the shader on the gpu
	if(m_VertexShader.use_count())
	{
		m_VertexShader.lock()->setHandle(glCreateShader(GL_VERTEX_SHADER));
	}
	else
	{
	  //logger you need to have a vertex shader to build
		static_assert(" you need to have a vertex shader to build");
	}

	if (m_fragmentShader.use_count())
	{
		m_fragmentShader.lock()->setHandle(glCreateShader(GL_FRAGMENT_SHADER));
	}

	 // change source data for shaders
	const GLchar * l_vertStr = m_VertexShader.lock()->getSourceData().c_str();
	auto l_vertSize = static_cast<GLint>(m_VertexShader.lock()->getSourceData().length());

	glShaderSource(m_VertexShader.lock()->getHandle(), 1, &l_vertStr, &l_vertSize);

	const char * l_fragStr = m_fragmentShader.lock()->getSourceData().c_str();
	auto l_fragSize = static_cast<GLint>(m_fragmentShader.lock()->getSourceData().length());

	glShaderSource(m_fragmentShader.lock()->getHandle(), 1, &l_fragStr, &l_fragSize);

	//Compile shaders on gpu
	glCompileShader(m_VertexShader.lock()->getHandle());
	GLDebug::GetLastShaderError(m_VertexShader.lock()->getHandle());

	//Error Check
	glCompileShader(m_fragmentShader.lock()->getHandle());
	GLDebug::GetLastShaderError(m_fragmentShader.lock()->getHandle());

	// create shader program on gpu
	m_handle = glCreateProgram();

	// Attach shaders to program
	int shaderCount;
	glAttachShader(m_handle, m_VertexShader.lock()->getHandle());
	glGetProgramiv(m_handle, GL_ATTACHED_SHADERS, &shaderCount);
	glAttachShader(m_handle, m_fragmentShader.lock()->getHandle());
	glGetProgramiv(m_handle, GL_ATTACHED_SHADERS, &shaderCount);

	// Link
	glLinkProgram(m_handle);
	GLDebug::GetLastProgramLinkError(m_handle);


	// Delet shaders on gpu
	glDeleteShader(m_VertexShader.lock()->getHandle());
	glDeleteShader(m_fragmentShader.lock()->getHandle());

}

void ShaderProgram::bind() const
{
	// std::cout << "ShaderProgram: " << << "bind ProgrameHnadle: " << m_handle;
	glUseProgram(m_handle);
	// GLDebug::getLastError();
}

void ShaderProgram::unbind() const
{
	glUseProgram(NULL);
}

///////========================================================================///////
  ///////   Private                                                              ///////
  ///////========================================================================///////

	//////======================================================================//////
	//////    Non-Static                                                        //////
	//////======================================================================//////

	/////========================================================================/////
	/////     Functions                                                          /////
	/////========================================================================/////

	////==========================================================================////
	////      Non-Virtual                                                         ////
	////==========================================================================////

	//==============================================================================//
	//        Helper                                                                //
	//==============================================================================//









