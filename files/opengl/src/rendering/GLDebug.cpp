#include "Precompiled.h"
#include "rendering/GLDebug.h"




/*!***************************************************************************************
\brief  Prints Error from defines
\param error - error number
\return string of the error
*****************************************************************************************/

std::string GLErrorToString(GLenum error)
{
    switch (error)
    {
    case GL_NO_ERROR:
        return "GL_NO_ERROR";
    case GL_INVALID_ENUM:
        return "GL_INVALID_ENUM";
    case GL_INVALID_VALUE:
        return "GL_INVALID_VALUE";
    case GL_INVALID_OPERATION:
        return "GL_INVALID_OPERATION";
    case GL_INVALID_FRAMEBUFFER_OPERATION:
        return "GL_INVALID_FRAMEBUFFER_OPERATION";
    case GL_OUT_OF_MEMORY:
        return "GL_OUT_OF_MEMORY";
    default:
        return "unknown error";
    }
}

std::string GLFramebufferErrorToString(GLenum error)
{
    switch (error)
    {
    case GL_FRAMEBUFFER_COMPLETE:
        return "GL_FRAMEBUFFER_COMPLETE";
    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
        return "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT";
    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
        return "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT";
    case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
        return "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER";
    case GL_INVALID_FRAMEBUFFER_OPERATION:
        return "GL_INVALID_FRAMEBUFFER_OPERATION";
    case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
        return "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER";
    case GL_FRAMEBUFFER_UNSUPPORTED:
        return "GL_FRAMEBUFFER_UNSUPPORTED";
    default:
        return "unknown error";
    }
}





void GLDebug::GetLastShaderError(GLint p_handle)
{

    GLint l_success = 0;
    GLint l_legth = 0;
   


    glGetShaderiv(p_handle, GL_COMPILE_STATUS, &l_success);
    if (l_success == GL_FALSE)
    {
       std::cout << GLErrorToString(glGetError()) << std::endl;
        glGetShaderiv(p_handle, GL_INFO_LOG_LENGTH, &l_legth);

        GLchar * l_infoLog = new GLchar[l_legth];

        glGetShaderInfoLog(p_handle, l_legth, &l_legth, l_infoLog);
       std::cout << "ERROR::SHADER_" << p_handle << "::COMPILATION_FAILED\n" << l_infoLog << std::endl;

        delete[] l_infoLog;
    }
    else
    {
       std::cout << "SHADER_" << p_handle << ":no error\n" << std::endl;
    }
}


void GLDebug::GetLastProgramError(GLint p_handle)
{
    GLint l_success = 0;
    GLint l_legth = 512;
    GLchar l_infoLog[512];

    glGetProgramiv(p_handle, GL_COMPILE_STATUS, &l_success);

    if (l_success == GL_FALSE)
    {
       std::cout << GLErrorToString(glGetError()) << std::endl;
        glGetProgramiv(p_handle, GL_INFO_LOG_LENGTH, &l_legth);

        //bool isShader = glIsShader(handle);
        glGetProgramInfoLog(p_handle, l_legth, &l_legth, l_infoLog);
       std::cout << "ERROR::PROGRAM::COMPILATION_FAILED\n" << l_infoLog << std::endl;
    }
}

void GLDebug::GetLastProgramLinkError(GLint p_handle)
{
    GLint l_success = 0;
    GLint l_legth = 512;
    GLchar l_infoLog[512];

    glGetProgramiv(p_handle, GL_LINK_STATUS, &l_success);

    if (l_success == GL_FALSE)
    {
       std::cout << GLErrorToString(glGetError()) << std::endl;
        glGetProgramiv(p_handle, GL_INFO_LOG_LENGTH, &l_legth);

        //bool isShader = glIsShader(handle);
        glGetProgramInfoLog(p_handle, l_legth, &l_legth, l_infoLog);
       std::cout << "ERROR::PROGRAM::LINKING_FAILED\n" << l_infoLog << std::endl;
    }
}

void GLDebug::getLastError()
{
   std::cout << GLErrorToString(glGetError()) << std::endl;
}


void GLDebug::getLastFrameBufferError()
{
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    std::cout << GLFramebufferErrorToString(status);
   
}

