/*!***************************************************************************************
\file       CubeTexture.hpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      Holds all information about a texture
*****************************************************************************************/
#include "Precompiled.h"
//========Self Include==================================================================//

#include "rendering/CubeTexture.h"
#include "rendering/GLDebug.h"
#include "rendering/TypeData.h"

static unsigned char const UnbuiltTexture = 0;
static char const UnboundTexture = -1;


CubeTexture::CubeTexture(){

  

}

void CubeTexture::build()
{
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &m_CubeTextureHandle);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_CubeTextureHandle);

    
    for (unsigned int i = 0; i < m_textures.size(); i++)
    {
            //https://learnopengl.com/Advanced-OpenGL/Cubemaps
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, m_textures[i].lock()->getWidth(), m_textures[i].lock()->getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, m_textures[i].lock()->getPixelData());
    }

    //https://www.opengl.org/discussion_boards/showthread.php/173152-Texture-looks-strange
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

}

void CubeTexture::bind(unsigned slot)
{
    

    //TODO(cody):Check if already bound
    if (m_boundSlot != UnboundTexture)
        //logger("CubeTexture").debug() << "ERROR: texture already bound";

    //http://stackoverflow.com/questions/8866904

    // sets current active object
    glActiveTexture(GL_TEXTURE0 + slot);

    // binds texture to that object
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_CubeTextureHandle);

    //GLDebug::getLastError();

    // save slot its in
    m_boundSlot = slot;
}

void CubeTexture::unbind()
{

    if (m_boundSlot == UnboundTexture)
    {
       // logger("CubeTexture").debug() << "ERROR: texture already unbound";
        return;
    }
        
    // unbind texture from slot
    glActiveTexture(GL_TEXTURE0 + m_boundSlot);
    glBindTexture(GL_TEXTURE_2D, 0);
    m_boundSlot = UnboundTexture; // unbound = -1
}

CubeTexture::~CubeTexture()
{
    m_textures.clear();
}
