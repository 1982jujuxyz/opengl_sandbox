#include "Precompiled.h"
#include "rendering/FramebufferManager.h"
#include "rendering/Framebuffer.h"

std::shared_ptr<Framebuffer> FramebufferManager::createFramebuffer(FramebufferType p_type, unsigned int p_width, unsigned int p_height)
{

    m_Framebuffers.insert(std::make_pair(p_type, std::shared_ptr<Framebuffer>(new Framebuffer(p_width, p_height))));
    return getFramebuffer(p_type);
}

std::shared_ptr<Framebuffer> FramebufferManager::getFramebuffer(FramebufferType p_type)
{
    return  m_Framebuffers.find(p_type)->second;
}

int FramebufferManager::createBufferPool(const unsigned p_bufferSize)
{

    renderPool l_renderPool;

    for (unsigned i = 0; i < p_bufferSize; ++i)
    {
        l_renderPool.pool.emplace_back(new Framebuffer(1024, 768));

        l_renderPool.pool.back()->addColorAttachment("screenSampler");
        l_renderPool.pool.back()->build();
    }

    l_renderPool.m_used = false;

    m_renderPools.push_back(l_renderPool);

    return static_cast<int>(m_renderPools.size() - 1);

}



bool FramebufferManager::addFrameToBufferPool(unsigned p_index)
{
    if(m_renderPools[p_index].pool.size() < MAXFRAMES)
    {
        m_renderPools[p_index].pool.emplace_front(new Framebuffer(1024, 768));
        m_renderPools[p_index].pool.front()->addColorAttachment("screenSampler");
        m_renderPools[p_index].pool.front()->build();

        return true;
    }
  
    return false;
}

int FramebufferManager::attachRenderPool()
{
    for (int i = 0; i < m_renderPools.size(); ++i)
    {
        if(!m_renderPools[i].m_used)
        {
            m_renderPools[i].m_used = true;
            return i;
        }
    }

    int index = createBufferPool(MAXFRAMES);
    m_renderPools[index].m_used = true;

    return index;

}

unsigned FramebufferManager::createCapturePool(unsigned p_index)
{
    m_capturePools.push_back(m_renderPools[p_index].pool);
    resetBufferPool(p_index);
    return static_cast<unsigned>(m_capturePools.size() - 1);
    
}

void FramebufferManager::saveRenderToCapturePool(unsigned p_captureIndex, unsigned p_tapeIndex)
{
    m_capturePools[p_captureIndex].clear();
    m_capturePools[p_captureIndex] = m_renderPools[p_tapeIndex].pool;
    resetBufferPool(p_tapeIndex);
}

void FramebufferManager::clearBuffer(unsigned p_index)
{
    m_renderPools[p_index].pool.clear();
}

void FramebufferManager::resetBufferPool(unsigned p_index)
{
   
   clearBuffer(p_index);

   for (unsigned i = 0; i < MAXFRAMES; ++i)
   {
       m_renderPools[p_index].pool.emplace_back(new Framebuffer(1024, 768));

       m_renderPools[p_index].pool.back()->addColorAttachment("screenSampler");
       m_renderPools[p_index].pool.back()->build();
   }

   m_renderPools[p_index].m_used = false;

}


std::deque<std::shared_ptr<Framebuffer>> & FramebufferManager::getBufferPool(const unsigned p_id)
{
    return m_renderPools[p_id].pool;   
}

std::deque<std::shared_ptr<Framebuffer>>& FramebufferManager::getCapturePool(const unsigned p_id)
{
    return m_capturePools[p_id];
}
