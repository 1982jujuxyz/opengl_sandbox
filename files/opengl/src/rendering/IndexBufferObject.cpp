/*!***************************************************************************************
\file       IndexBufferObject.cpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      The CPU side repersentation of the GPU buffer object that hold index data
*****************************************************************************************/
#include "Precompiled.h"
//========Self Include==================================================================//
#include "rendering\IndexBufferObject.h"
//========1st Party Includes============================================================//
//========3rd Party Includes============================================================//
//========Types=========================================================================//
//========Defines=======================================================================//
//========Static Deceleration===========================================================//

	///////========================================================================///////
	///////   Public                                                               ///////
	///////========================================================================///////

		//==============================================================================//
		//        Constructor                                                           //
		//==============================================================================//
IndexBufferObject::IndexBufferObject(Topology p_Topology, std::size_t p_primitiveCount)
	: m_topology(p_Topology),
	m_indexCount(static_cast<int>(p_Topology) * p_primitiveCount),
	m_insertOffset(0),
	m_bufferSize(sizeof(unsigned) * m_indexCount),
	m_buffer(new char[m_bufferSize]),
	m_handle(0)
{
}
//==============================================================================//
//        Destructor                                                            //
//==============================================================================//

IndexBufferObject::~IndexBufferObject()
{
	// cleanup on gpu side
	IndexBufferObject::shutdown();
	delete[] m_buffer;
}

//==============================================================================//
//          Helper Destructor                                                   //
//==============================================================================//
void IndexBufferObject::shutdown()
{
	glDeleteBuffers(1, &m_handle);
}

//==============================================================================//
//        Getters & Setters                                                     //
//==============================================================================//
std::size_t IndexBufferObject::getBufferSize() const
{
	return m_bufferSize;
}

////==========================================================================////
////      Non-Virtual                                                         ////
////==========================================================================////

//////======================================================================//////
//////    Non-Static                                                        //////
//////======================================================================//////

/////========================================================================/////
/////     Functions                                                          /////
/////========================================================================/////
void IndexBufferObject::build()
{
	glGenBuffers(1, &m_handle);
	bind();
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_bufferSize, m_buffer, GL_STATIC_DRAW);

}

void IndexBufferObject::bind()
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_handle);
}

void IndexBufferObject::unbind() const
{
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}


void IndexBufferObject::addLine(unsigned p_a, unsigned p_b)
{

	// needs a count check

	unsigned * l_indexBuffer = reinterpret_cast<unsigned*>(m_buffer);
	l_indexBuffer[m_insertOffset++] = p_a;
	l_indexBuffer[m_insertOffset++] = p_b;

}

void IndexBufferObject::addTriangle(unsigned p_a, unsigned p_b, unsigned p_c)
{
	//needs a count check

	auto * l_indexBuffer = reinterpret_cast<unsigned*>(m_buffer);

	l_indexBuffer[m_insertOffset++] = p_a;
	l_indexBuffer[m_insertOffset++] = p_b;
	l_indexBuffer[m_insertOffset++] = p_c;

}










