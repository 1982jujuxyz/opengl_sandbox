///*!***************************************************************************************
//\file       ShaderManager.cpp
//\author     Cannell, Cody
//\date       6/19/18
//\copyright  All content © 2018-2019 DigiPen (USA) Corporation, all rights reserved.
//\par        Project: Boomerang
//\brief      Manages shaders and shader programes.
//*****************************************************************************************/
//
#include "Precompiled.h"
////========Self Include==================================================================//
//#include "rendering/ShaderManager.h"
////========1st Party Includes============================================================//
//#include "rendering/ShaderProgram.h"
//#include "rendering/TypeData.h"
////========3rd Party Includes============================================================//
////========Types=========================================================================//
////========Defines=======================================================================//
////========Static Deceleration===========================================================//
//
//	///////========================================================================///////
//	///////   Public                                                               ///////
//	///////========================================================================///////
//
//		//==============================================================================//
//		//        Constructor                                                           //
//		//==============================================================================//
//
//		//==============================================================================//
//		//        Destructor                                                            //
//		//==============================================================================//
//		ShaderManager::~ShaderManager()
//		{
//		    m_ShaderPrograms.clear();
//		}
//		//==============================================================================//
//		//        Getters & Setters                                                     //
//		//==============================================================================//
//		std::shared_ptr<ShaderProgram> ShaderManager::getShaderProgram(ProgramType p_ProgramType) const
//		{
//		    return m_ShaderPrograms.find(p_ProgramType)->second;
//		}
//
//		std::weak_ptr<Shader>  ShaderManager::getShader(ProgramType p_ProgramType, ShaderType p_ShaderType) const
//		{
//		    return m_ShaderPrograms.find(p_ProgramType)->second->getShader(p_ShaderType);
//		}
//
//		/*std::shared_ptr<programPipeline> ShaderManager::getProgramPipeline(ProgramType p_ProgramType) const
//		{
//			return m_programPipeline.find(p_ProgramType)->second;
//		}*/
//
//		////==========================================================================////
//		////      Non-Virtual                                                         ////
//		////==========================================================================////
//
//		//////======================================================================//////
//		//////    Non-Static                                                        //////
//		//////======================================================================//////
//
//		/////========================================================================/////
//		/////     Functions                                                          /////
//		/////========================================================================/////
//		std::shared_ptr<ShaderProgram> ShaderManager::createShaderProgram(ProgramType p_ProgramType, std::weak_ptr<Shader> p_vertexName, std::weak_ptr<Shader> p_fragmentName)
//		{
//		    m_ShaderPrograms.insert(std::make_pair(p_ProgramType, std::make_shared<ShaderProgram>(p_vertexName, p_fragmentName)));
//
//		    return getShaderProgram(p_ProgramType);
//
//		}
//
//
//		/*std::shared_ptr<programPipeline> ShaderManager::createProgramPipelineObject( ProgramType p_ProgramType, std::string p_vertexName, std::string p_fragmentName)
//		{
//			m_programPipeline.insert(std::make_pair(p_ProgramType, std::make_shared<programPipeline>(m_assetPath, p_vertexName, p_fragmentName)));
//
//			return getProgramPipeline(p_ProgramType);
//		}*/
//
//	///////========================================================================///////
//	///////   Private                                                              ///////
//	///////========================================================================///////
//
//		//////======================================================================//////
//		//////    Non-Static                                                        //////
//		//////======================================================================//////
//
//		/////========================================================================/////
//		/////     Functions                                                          /////
//		/////========================================================================/////
//
//		////==========================================================================////
//		////      Non-Virtual                                                         ////
//		////==========================================================================////
//
//		//==============================================================================//
//		//        Helper                                                                //
//		//==============================================================================//
//
//
//
//
//
//
//
//// This is for creating multiple shader programes at the same time
////std::shared_ptr<ShaderProgram> meshManager::LoadShaderGroup(ProgramType type,
////    std::vector<std::pair<std::string, std::string>> const& shaderFiles)
////{
////    return nullptr;
////}
//
//
//
//
//
//
//
//
//
