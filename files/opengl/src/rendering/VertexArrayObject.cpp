/*!***************************************************************************************
\file       VertexArrayObject.cpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: 350
\brief      The CPU repersentation of the VAO that holds all shaders for pipeline
*****************************************************************************************/

#include "Precompiled.h"
#include "rendering/VertexArrayObject.h"
#include "rendering/Mesh.h"



VertexArrayObject::VertexArrayObject() :
m_handle(NULL)
{
}

VertexArrayObject::~VertexArrayObject()
{
	m_ibo->shutdown();
	delete m_ibo;
	m_vbo->shutdown();
	delete m_vbo;
	glDeleteVertexArrays(1, &m_handle);
}



void VertexArrayObject::build(std::weak_ptr<Mesh> p_mesh)
{


	//generate Vertex array on gpu side
	glGenVertexArrays(1, &m_handle);
	bind();

	size_t l_offset = 0;
	size_t l_attributeCount = p_mesh.lock()->getAttributeCount();
	size_t const l_vertexSize = p_mesh.lock()->getVertexSize();

	m_vbo->build();

	for (size_t i = 0; i < l_attributeCount; ++i)
	{

		glEnableVertexAttribArray(static_cast<GLuint>(i));

		// send the attribute data to shader
		glVertexAttribPointer(static_cast<GLuint>(i),
			static_cast<GLint>(p_mesh.lock()->getAttributeElementCounts()[i]), GL_FLOAT, GL_FALSE,
			static_cast<GLuint>(l_vertexSize), reinterpret_cast<GLvoid *>(l_offset));


		//check open gl for errors

		l_offset += p_mesh.lock()->getAttributeElementSize()[i]; // skip to the next attribute

	}

	m_ibo->build();

	unbind();

}

void VertexArrayObject::bind() const
{
	glBindVertexArray(m_handle);
}

void VertexArrayObject::render()
{
	switch (m_ibo->getTopology())
	{
	case Topology::enm_lines:
		glDrawElements(GL_LINES, static_cast<GLsizei>(m_ibo->getIndexCount()), GL_UNSIGNED_INT, nullptr);
		break;
	case Topology::enm_triangle:
		glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(m_ibo->getIndexCount()), GL_UNSIGNED_INT, nullptr);
		break;
	}
}

void VertexArrayObject::unbind() const
{
	glBindVertexArray(0);
}




