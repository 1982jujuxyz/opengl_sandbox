/*!***************************************************************************************
\file       TriangleMesh.cpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      This holds all data for constructing a mesh made of triangles.
*****************************************************************************************/

#include "Precompiled.h"
//========Self Include==================================================================//

#include "rendering/TriangleMesh.h"

//========1st Party Includes============================================================//


#include "rendering/TypeData.h"
#include "Vector2.h"
#include "Vector3.h"

//========3rd Party Includes============================================================//  

//========Types=========================================================================//
//========Defines=======================================================================//
//========Static Deceleration===========================================================//

	///////========================================================================///////
	///////   Public                                                               ///////
	///////========================================================================///////

		//==============================================================================//
		//        Constructor                                                           //
		//==============================================================================//


void TriangleMesh::loadInFile(std::string const & p_assetPath, std::string const& p_objFileName)
{
	

}

TriangleMesh::TriangleMesh(const TriangleMesh& p_other) : Mesh(p_other)
{

}

//==============================================================================//
//        Destructor                                                            //
//==============================================================================//

//==============================================================================//
//        Getters & Setters                                                     //
//==============================================================================//

////==========================================================================////
////      Non-Virtual                                                         ////
////==========================================================================////

//////======================================================================//////
//////    Non-Static                                                        //////
//////======================================================================//////

/////========================================================================/////
/////     Functions                                                          /////
/////========================================================================/////
void TriangleMesh::addFaceToMesh(unsigned p_a, unsigned p_b, unsigned p_c)
{
	m_faces.emplace_back(p_a, p_b, p_c);
}

void TriangleMesh::addUVToMesh(float p_u, float p_v)
{
	m_uvs.emplace_back(p_u, p_v);
}

void TriangleMesh::addPositionToMesh(float p_x, float p_y, float p_z)
{
	m_position.emplace_back(p_x, p_y, p_z);
}

void TriangleMesh::preprocess(DefaultUvType p_uvType)
{
	// if normals did not come from file calculate them
	if (!normalsSet)
	{
		centerMesh();
		// normalizeVertices();
		// Get all the normals for each triangle

		/*
			Nx = UyVz - UzVy
			Ny = UzVx - UxVz
			Nz = UxVy - UyVx
		*/
		m_triangleNormals.clear();

		for (unsigned i = 0; i < m_faces.size(); i++)
		{
			Math::Vector3 l_u = m_vertices[m_faces[i].b].m_position - m_vertices[m_faces[i].a].m_position;
			Math::Vector3  l_v = m_vertices[m_faces[i].c].m_position - m_vertices[m_faces[i].a].m_position;

			// cross product
			 Math::Vector3  l_normal(
				l_u.y * l_v.z - l_u.z * l_v.y,
				l_u.z * l_v.x - l_u.x * l_v.z,
				l_u.x * l_v.y - l_u.y * l_v.x
			);


			// accumulate adjacent normals
			l_normal.normalize();
			m_vertices[m_faces[i].a].m_normal += l_normal;
			m_vertices[m_faces[i].b].m_normal += l_normal;
			m_vertices[m_faces[i].c].m_normal += l_normal;


			m_triangleNormals.push_back(l_normal);
		}

		// normalize all the Vertex normals
		for (auto& i : m_vertices)
		{
			i.m_normal.normalize();

		}

		// Normalize all the triangle normals
		float l_epsilon = 0.000001f;
		float l_epsilonSq = l_epsilon * l_epsilon;

		for (auto & l_i : m_triangleNormals)
		{
			float l_lengthSq = l_i.x * l_i.x + l_i.y * l_i.y + l_i.z * l_i.z;

			if (l_lengthSq >= l_epsilonSq)
			{
				float l_length = std::sqrt(l_lengthSq);
				l_i /= l_length;
			}
		}
	}

	// if uvs did not come from file calculate them
	if (!uvsSet)
	{
		//TODO(cody): calculate UV and tangents
		switch (p_uvType)
		{
		case DefaultUvType::None:
			break;
		case DefaultUvType::Box:
			CalcUvBox();
			break;
		case DefaultUvType::Cylindrical:
			//CalcUvCylindrical();
			break;
		case DefaultUvType::Spherical:
		default:
			// CalcUvSpherical();
			break;
		}
	}
}




	//for (auto normalIndex : accumulateNormalIntex)
	//{
	//    Vector3 sum(0, 0, 0);
	//    //get the sum 
	//    for (auto index : normalIndex)
	//    {
	//        sum += m_vertices[index - 1].m_normal;
	//    }

	//    sum.normalize();
	//     
	//    //apply the sum
	//    for (auto index : normalIndex)
	//    {
	//        m_vertices[index - 1].m_normal = sum;
	//    }
	//}



//void TriangleMesh::AddFaces(unsigned* faces)
		//{
		//}

		//void TriangleMesh::build()
		//{
		//    //m_VertexArrayObject = std::make_shared<VertexArrayObject>(vertices.size(), faces.size(), sizeof(Vector3), topology::enm_triangle);
		//
		//    // Step 2: add all of the vertices to the VBO
		//    //std::memcpy(&vao->getVertexBufferObject(),&m_vertices, sizeof(m_vertices));
		//    //for (Vertex vert : vertices)
		//    //{
		//    //    VertexArrayObject->getVertexBufferObject().addVertex(vert.position);
		//    //}
		//    //// Step 3: add all indices (per triangle) to the IBO
		//    //for (Face face : faces)
		//    //{
		//    //    VertexArrayObject->getIndexBufferObject().addTriangle(face.a, face.b, face.c);
		//    //}
		//
		//    //// Step 4: upload the contents of the VBO and IBO to the GPU and build the VAO
		//    //VertexArrayObject->build(this);
		//
		//    // Step 5: wrap the VAO into a shared_ptr and save it locally to be rendered               
		//    //m_isBuilt = true;
		//}


	///////========================================================================///////
	///////   Private                                                              ///////
	///////========================================================================///////

		//////======================================================================//////
		//////    Non-Static                                                        //////
		//////======================================================================//////

		/////========================================================================/////
		/////     Functions                                                          /////
		/////========================================================================/////

		////==========================================================================////
		////      Non-Virtual                                                         ////
		////==========================================================================////

void TriangleMesh::addNormalToMesh(float x, float y, float z)
{
	m_triangleNormals.emplace_back(x, y, z);
}

//======================================================================================//
//        Operators                                                                     //
//======================================================================================//

TriangleMesh& TriangleMesh::operator=(const TriangleMesh& p_other)
{
	if (this == &p_other)
	{
		return *this;
	}

	Mesh::operator=(p_other);
	m_center = p_other.m_center;
	m_faces = p_other.m_faces;
	m_position = p_other.m_position;
	m_triangleNormals = p_other.m_triangleNormals;
	m_uvs = p_other.m_uvs;
	m_indices = p_other.m_indices;

	normalsSet = p_other.normalsSet;
	uvsSet = p_other.uvsSet;

	return *this;
}

//==============================================================================//
		//        Helper                                                                //
		//==============================================================================//
void TriangleMesh::centerMesh()
{
	// find the centroid of the entire mesh 
	for (auto l_vert : m_vertices)
	{
		m_center += l_vert.m_position;
	}

	// get average
	m_center *= 1 / static_cast<float>(m_vertices.size());

	for (auto & l_vert : m_vertices)
	{
		l_vert.m_position += -(m_center);
	}
}



//https://en.wikipedia.org/wiki/Cube_mapping
TriangleMesh* TriangleMesh::CalcUvBox()
{
	for (auto & v : m_vertices)
	{
		Math::Vector3 position = v.m_position * 2;

		float absX = fabs(position.x);
		float absY = fabs(position.y);
		float absZ = fabs(position.z);

		int isXPositive = position.x > 0 ? 1 : 0;
		int isYPositive = position.y > 0 ? 1 : 0;
		int isZPositive = position.z > 0 ? 1 : 0;

		float maxAxis = 0, uc = 0, vc = 0, u_ = 0, v_ = 0;


		// POSITIVE X
		if (isXPositive && absX >= absY && absX >= absZ) {
			// u (0 to 1) goes from +z to -z
			// v (0 to 1) goes from -y to +y
			maxAxis = absX;
			uc = -position.z;
			vc = position.y;
		}
		// NEGATIVE X
		else if (!isXPositive && absX >= absY && absX >= absZ) {
			// u (0 to 1) goes from -z to +z
			// v (0 to 1) goes from -y to +y
			maxAxis = absX;
			uc = position.z;
			vc = position.y;

		}
		// POSITIVE Y
		else if (isYPositive && absY >= absX && absY >= absZ) {
			// u (0 to 1) goes from -x to +x
			// v (0 to 1) goes from +z to -z
			maxAxis = absY;
			uc = position.x;
			vc = -position.z;
		}
		// NEGATIVE Y
		else if (!isYPositive && absY >= absX && absY >= absZ) {
			// u (0 to 1) goes from -x to +x
			// v (0 to 1) goes from -z to +z
			maxAxis = absY;
			uc = position.x;
			vc = position.z;
		}
		// POSITIVE Z
		else if (isZPositive && absZ >= absX && absZ >= absY) {
			// u (0 to 1) goes from -x to +x
			// v (0 to 1) goes from -y to +y
			maxAxis = absZ;
			uc = position.x;
			vc = position.y;
		}
		// NEGATIVE Z
		else if (!isZPositive && absZ >= absX && absZ >= absY) {
			// u (0 to 1) goes from +x to -x
			// v (0 to 1) goes from -y to +y
			maxAxis = absZ;
			uc = -position.x;
			vc = position.y;
		}

		// Convert range from -1 to 1 to 0 to 1
		u_ = 0.5f * (uc / maxAxis + 1.0f);
		v_ = 0.5f * (vc / maxAxis + 1.0f);

		//m_boxUV.push_back(Math::Vec2(u, v));

		v.m_uv = Math::Vector2(u_, v_);
	}

	return this;
}


//void TriangleMesh::normalizeVertices()
//{
//    // find the extent of this mesh and normalize all vertices by scaling them
//    // by the inverse of the smallest value of the extent (that isn't zero)
//    Vector3 minimum = m_vertices[0].m_position;
//    Vector3 maximum = m_vertices[0].m_position;
//    for (auto& vert : m_vertices)
//    {
//        float x = vert.m_position.getX(), y = vert.m_position.getY(), z = vert.m_position.getZ();
//        minimum.setX(std::min(minimum.getX(), x));
//        minimum.setY(std::min(minimum.getY(), y));
//        minimum.setZ(std::min(minimum.getZ(), z));
//        maximum.setX(std::max(maximum.getX(), x));
//        maximum.setY(std::max(maximum.getY(), y));
//        maximum.setZ(std::max(maximum.getZ(), z));
//    }
//    Vector3 extent = maximum - minimum;
//    float minExtentLength = 0.f;
//    bool xZero = abs(extent.getX()) <= 0.000001f;
//    bool yZero = abs(extent.getY()) <= 0.000001f;
//    bool zZero = abs(extent.getZ()) <= 0.000001f;
//    if (xZero && yZero && zZero)
//        return; // cannot normalize 1 point mesh
//    else if (xZero) // 2 or less may be zero
//    {
//        if (yZero)
//            minExtentLength = extent.getZ(); // only non-zero
//        else if (zZero)
//            minExtentLength = extent.getY();
//        else // only x is zero
//            minExtentLength = std::min(extent.getY(), extent.getZ());
//    }
//    else if (yZero)
//    {
//        if (xZero)
//            minExtentLength = extent.getZ(); // only non-zero
//        else if (zZero)
//            minExtentLength = extent.getX();
//        else // only x is zero
//            minExtentLength = std::min(extent.getX(), extent.getZ());
//    }
//    else if (zZero)
//    {
//        if (xZero)
//            minExtentLength = extent.getY(); // only non-zero
//        else if (yZero)
//            minExtentLength = extent.getX();
//        else // only x is zero
//            minExtentLength = std::min(extent.getX(), extent.getY());
//    }
//    else
//        minExtentLength = std::min(std::min(extent.getX(), extent.getY()), extent.getZ());
//    float scalar = 1.f / minExtentLength; // guaranteed to not be 1/0
//    for (auto& vert : m_vertices)
//        vert.m_position *= scalar;
//}

// calculate the T and B 
//TriangleMesh* TriangleMesh::CalcTanBitan()
//{
//    Vector3 * tangentContainer = new Vector3[m_vertices.size()];
//    Vector3 * bitangentContainer = new Vector3[m_vertices.size()];
//    int * count = new int[m_vertices.size()]();

//    for (size_t i = 0, j = m_faces.size(); i < j; ++i)
//    {
//        face& tri = m_faces[i];
//        //Vertex& vert = m_vertices[i];

//        vertex P = m_vertices[tri.b].m_position - m_vertices[tri.a].m_position;
//        vertex Q = m_vertices[tri.c].m_position - m_vertices[tri.a].m_position;

//        float Sp = m_vertices[tri.b].m_uv[0] - m_vertices[tri.a].m_uv[0];
//        float Tp = m_vertices[tri.b].m_uv[1] - m_vertices[tri.a].m_uv[1];

//        float Sq = m_vertices[tri.c].m_uv[0] - m_vertices[tri.a].m_uv[0];
//        float Tq = m_vertices[tri.c].m_uv[1] - m_vertices[tri.a].m_uv[1];

//        // P = Sp*T + Tp*B
//        // Q = Sq2*T + Tq*B
//        float arr[4] = { Sp, Tp ,  Sq, Tq };
//        Matrix2 origMat = { arr };
//        Matrix2 pMat = origMat;
//        Matrix2 qMat = origMat;

//        // get the det
//        float det = origMat.determinate();
//        float detx, dety, detz;

//        Vector3 t, b;

//        // Px
//        pMat[0][0] = P.m_position.x;
//        pMat[1][0] = Q.m_position.x;

//        detx = pMat.determinate();

//        t.x = detx / det;

//        // Py

//        pMat[0][0] = P.m_position.y;
//        pMat[1][0] = Q.m_position.y;

//        dety = pMat.determinate();

//        t.y = dety / det;

//        // Pz

//        pMat[0][0] = P.m_position.z;
//        pMat[1][0] = Q.m_position.z;

//        detz = pMat.determinate();

//        t.z = detz / det;

//        // Qx

//        qMat[0][1] = P.m_position.x;
//        qMat[1][1] = Q.m_position.x;

//        detx = qMat.determinate();

//        b.x = detx / det;

//        // Qy

//        qMat[0][1] = P.m_position.y;
//        qMat[1][1] = Q.m_position.y;

//        dety = qMat.determinate();

//        b.y = dety / det;

//        // Qz
//        qMat[0][1] = P.m_position.z;
//        qMat[1][1] = Q.m_position.z;

//        detz = qMat.determinate();

//        b.z = detz / det;


//        tangentContainer[tri.a] += t;
//        tangentContainer[tri.b] += t;
//        tangentContainer[tri.c] += t;

//        bitangentContainer[tri.a] += b;
//        bitangentContainer[tri.b] += b;
//        bitangentContainer[tri.c] += b;

//        count[tri.a]++;
//        count[tri.b]++;
//        count[tri.c]++;
//    }

//    // average out the calculation 
//    for (int i = 0; i < m_vertices.size(); ++i)
//    {
//        m_vertices[i].m_tangent = tangentContainer[i] / static_cast<float>(count[i]);
//        m_vertices[i].m_bitangent = bitangentContainer[i] / static_cast<float>(count[i]);
//    }

//    delete[] tangentContainer;
//    delete[] bitangentContainer;
//    delete[] count;

//    return this;
//}





