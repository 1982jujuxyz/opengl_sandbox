#include "Precompiled.h"
#include "rendering/GraphicsEngine.h"
#include "rendering/VertexArrayObject.h"
//#include "rendering/ShaderManager.h"
//#include "rendering/MeshManager.h"
//#include "rendering/VertexArrayManager.h"
//#include "rendering/MaterialManager.h"
//#include "rendering/TextureManager.h"
#include "rendering/FrameBufferManager.h"
#include "Scene.h"
#include "ResourceManager.h"
#include "system/Loaders.h"	
#include "rendering/TriangleMesh.h"
#include "rendering/Material.h"
#include "rendering/MeshBuilder.h"
#include "ComponentPool.h"
#include "Components/Camera.h"
#include "Components/Transform.h"
#include "Components/Render.h"
#include "Components/Light.h"
#include "Object.h"
#include "Components/Material.h"


void GraphicsEngine::Initialize()
{
	//m_shaderManager = new ShaderManager();
	//m_meshManager = new MeshManager();
	//m_vertexArrayManager = new VertexArrayManager();
	//m_MaterialManager = new MaterialManager();
	//m_textureManager = new TextureManager();
	m_framebufferManager = new FramebufferManager();

	// RegisterFunctions
	ResourceManager::RegisterFunction<TriangleMesh>(TriangleMeshLoader);
	ResourceManager::RegisterFunction<Material>(MaterialLoader);
	ResourceManager::RegisterFunction<SimpleTexture>(TextureLoader);
	ResourceManager::RegisterFunction<CubeTexture>(CubeTextureLoader);
	ResourceManager::RegisterFunction<Shader>(ShaderLoader);
	ResourceManager::RegisterFunction<ShaderProgram>(ShaderProgramLoader);
	ResourceManager::RegisterFunction<VertexArrayObject>(VertexArrayLoader);

	// load models
	//const auto l_chicken_model = ResourceManager::GetResource<TriangleMesh>("models/HEN_ry.obj");
	const auto l_chicken_model = ResourceManager::GetResource<VertexArrayObject>("models/HEN_ry.obj");
	const auto l_forward_program = ResourceManager::GetResource<ShaderProgram>("shaders/LightingProgram.ppk");

	const auto l_pass_through_program = ResourceManager::GetResource<ShaderProgram>("shaders/PassThroughProgram.ppk");

	const auto l_texture = ResourceManager::GetResource<SimpleTexture>("textures/texture_uv.png");
	const auto l_cubeTexture = ResourceManager::GetResource<CubeTexture>("textures/skybox_1.tpk");

  l_cubeTexture.lock()->build();
	l_texture.lock()->build();
	l_forward_program.lock()->build();


	//auto l_material = ResourceManager::GetResource<Material>("materials/basic.mtl");

	//auto l_texture = ResourceManager::GetResource<SimpleTexture>("textures/TextureUVC.png");
    

	//std::shared_ptr<VertexArrayObject> l_vao = getVertexArrayManager()->createArrayBuffer(l_chicken_model.lock());

	//std::shared_ptr<VertexArrayObject> l_chickenVao = getVertexArrayManager()->createArrayBuffer("HEN_ry.obj");

	//std::shared_ptr<VertexArrayObject> l_duckVao = getVertexArrayManager()->createArrayBuffer("duck.obj");

	//std::shared_ptr<VertexArrayObject> l_quadMesh = getVertexArrayManager()->buildFullScreenQuad("fullScreenQuad");


	
	/*std::shared_ptr<ShaderProgram> l_deferedProgram = getShaderManager()->createShaderProgram(ProgramType::enm_passthrough, "PassThrough.vert", "PassThrough.frag");

	std::shared_ptr<ShaderProgram> l_screenSpace = getShaderManager()->createShaderProgram( ProgramType::enm_screenSpace, "ScreenSpace.vert", "ScreenSpace.frag");

	std::shared_ptr<ShaderProgram> l_screenSpaceHighlight = getShaderManager()->createShaderProgram( ProgramType::enm_screenSpaceHighlight, "ScreenSpaceHighlight.vert", "ScreenSpaceHighlight.frag");

	std::shared_ptr<Framebuffer> l_buffer = getFramebufferManager()->createFramebuffer(FramebufferType::CaptureBuffer, 1024, 768);*/

}

void GraphicsEngine::Update(Scene * s)
{

	//// send model matrix
	//l_forward_program.lock()->setUniforms();

	// send projection matrix

	RenderSkybox(s);

	auto l_vectorOfLights = s->GetObjectsByComponent<Component::Light>();

	auto l_allGameObjects = s->getAllGameObjects();

	auto l_mainCamera = s->GetMainCamera();

	l_mainCamera->GetComponent<Component::Camera>()->process();

	for(auto obj : l_allGameObjects)
	{

		if(!obj->HasComponentType<Component::Render>())
			continue;

		auto l_renderComponent = obj->GetComponent<Component::Render>();

		const auto l_vao = ResourceManager::GetResource<VertexArrayObject>(l_renderComponent->m_modelName);
		const auto l_program = ResourceManager::GetResource<ShaderProgram>(l_renderComponent->m_shaderProgram);

		l_program.lock()->bind();

		auto l_transformComponent = obj->GetComponent<Component::Transform>();

		auto modelM = l_transformComponent->getGlobalMatrix();

		l_program.lock()->setUniforms("ModelMatrix", modelM);


		SetCameraUniforms(l_program, l_mainCamera);

	  SetLightUniform(l_program, l_vectorOfLights);
		
		setMaterialUniforms(l_program, l_renderComponent);
	
	
		setTextureUniforms(l_program, l_renderComponent);
	


		//l_renderComponent.m_preRenderGLCalls();

		l_vao.lock()->bind();
		l_vao.lock()->render();
		l_vao.lock()->unbind();

		//l_renderComponent.m_postRenderGLCalls();



	}
	
}

void GraphicsEngine::Shutdown()
{
}

void GraphicsEngine::RenderObjects(std::shared_ptr<ShaderProgram> p_ShaderProgram, Camera * p_camera)
{
    // pass in render
}

void GraphicsEngine::ForwardRender()
{
    
}

void GraphicsEngine::DefferedRender()
{
	
}


// set uniforms
void GraphicsEngine::SetCameraUniforms(std::weak_ptr<ShaderProgram> p_program, Object * p_camera)
{

	auto l_cameraComponent = p_camera->GetComponent<Component::Camera>();
	auto l_transformComponent = p_camera->GetComponent<Component::Transform>();

	l_cameraComponent->setViewMatrix(l_transformComponent->getGlobalMatrix().inverse());



	
	p_program.lock()->setUniforms("camera.position", Math::Vector4(l_transformComponent->getPosition(), 1));
	p_program.lock()->setUniforms("camera.i_fog", l_cameraComponent->m_fogColor);
	p_program.lock()->setUniforms("camera.z_far", l_cameraComponent->m_farPlaneDist);
	p_program.lock()->setUniforms("camera.z_near", l_cameraComponent->m_nearPlaneDist);
	p_program.lock()->setUniforms("ViewMatrix", l_cameraComponent->m_viewMatrix);
	p_program.lock()->setUniforms("ProjectionMatrix", l_cameraComponent->m_projectionMatrix);
}

void GraphicsEngine::SetLightUniform(std::weak_ptr<ShaderProgram> p_program, std::vector<Object*> & p_lights)
{

	p_program.lock()->setUniforms("lightCount", static_cast<int>(p_lights.size()));

	for(int i = 0; i < p_lights.size(); ++i)
	{
		auto l_transform_component = p_lights[i]->GetComponent<Component::Transform>();
		auto l_light_component = p_lights[i]->GetComponent<Component::Light>();

		static std::unordered_map<int /*index*/, std::unordered_map<std::string /*attribute*/, std::string /*uniform_name*/>> s_uniform_strings;
		p_program.lock()->setUniforms("globalAmbient", Component::Light::m_globalAmbient);


		// get the map of uniform strings (map is an optimization so we only do the concatenation once)
		auto l_iter = s_uniform_strings.find(i);
		if (l_iter == s_uniform_strings.end())
		{
			// build a map for this index
			std::unordered_map<std::string, std::string> l_map;
			std::stringstream str;
			str << "Lights[" << i << "].";

			l_map["ambient"] = str.str() + "ambient";
			l_map["diffuse"] = str.str() + "diffuse";
			l_map["specular"] = str.str() + "specular";
			l_map["isActive"] = str.str() + "isActive";
			l_map["direction"] = str.str() + "direction";
			l_map["position"] = str.str() + "position";
			l_map["type"] = str.str() + "type";
			l_map["intensity"] = str.str() + "intensity";
			l_map["ifDecay"] = str.str() + "ifDecay";
			l_map["disAtten"] = str.str() + "disAtten";
			l_map["theta"] = str.str() + "theta";
			l_map["phi"] = str.str() + "phi";
			l_map["falloff"] = str.str() + "falloff";
			l_map["disAtten"] = str.str() + "disAtten";
			s_uniform_strings[i] = l_map;
			l_iter = s_uniform_strings.find(i);
		}
		auto& l_uniform_map = l_iter->second;

		//colors
		p_program.lock()->setUniforms(l_uniform_map["ambient"], l_light_component->m_ambientColor);
		p_program.lock()->setUniforms(l_uniform_map["diffuse"], l_light_component->m_diffuseColor);
		p_program.lock()->setUniforms(l_uniform_map["specular"], l_light_component->m_specularColor);

		//other attribute      
		//transform* l_transform = getGameObject().lock()->getComponent<transform>();
		p_program.lock()->setUniforms(l_uniform_map["isActive"], l_light_component->m_isActive);
		p_program.lock()->setUniforms(l_uniform_map["direction"], Math::Vector4(l_transform_component->getGlobalRotation().getForward(), 0.0f));
		p_program.lock()->setUniforms(l_uniform_map["position"], Math::Vector4(l_transform_component->getGlobalPosition(), 0.0f));
		p_program.lock()->setUniforms(l_uniform_map["type"], static_cast<int>(l_light_component->m_type));
		p_program.lock()->setUniforms(l_uniform_map["intensity"], l_light_component->m_intensity);
		p_program.lock()->setUniforms(l_uniform_map["ifDecay"], l_light_component->m_ifDecay);

		switch (l_light_component->m_type)
		{
		case lighttype::enm_directional:
			break;
		case lighttype::enm_point:

			p_program.lock()->setUniforms(l_uniform_map["disAtten"], l_light_component->m_disAtten);
			break;
		case lighttype::enm_spot:

			p_program.lock()->setUniforms(l_uniform_map["theta"], l_light_component->m_innerAngle);
			p_program.lock()->setUniforms(l_uniform_map["phi"], l_light_component->m_outerAngle);
			p_program.lock()->setUniforms(l_uniform_map["falloff"], l_light_component->m_spotFalloff);
			p_program.lock()->setUniforms(l_uniform_map["disAtten"], l_light_component->m_disAtten);
			break;

		default:
			break;
		}
	}
	
}

void GraphicsEngine::setMaterialUniforms(std::weak_ptr<ShaderProgram> p_program, Component::Render* p_renderComponent)
{

	const auto l_material = ResourceManager::GetResource<Material>(p_renderComponent->m_materialName);

	p_program.lock()->setUniforms("skybox", false);

	//send all the material data
	p_program.lock()->setUniforms("material.ambientColor", l_material.lock()->getAmbientColor());
	p_program.lock()->setUniforms("material.diffuseColor", l_material.lock()->getDiffuseColor());
	p_program.lock()->setUniforms("material.emissiveColor", l_material.lock()->getEmissiveColor());
	p_program.lock()->setUniforms("material.specularColor", l_material.lock()->getSpecularColor());
	p_program.lock()->setUniforms("material.specularExponent", l_material.lock()->getSpecularExponent());

	
}

void GraphicsEngine::setTextureUniforms(std::weak_ptr<ShaderProgram> p_program, Component::Render* p_renderComponent)
{

	const auto l_texture = ResourceManager::GetResource<SimpleTexture>(p_renderComponent->m_textureName);

	  p_program.lock()->setUniforms("skybox", false);

		p_program.lock()->setUniforms("material.hasDiffuseTexture", true);

		//l_texture.lock()->build();
	
		// get and bind texture
	  l_texture.lock()->bind(1);

		// set uniform data
		p_program.lock()->setUniforms("diffuseTexture", l_texture.lock()->getBoundSlot());


	//if (m_textureData.m_hasSpecularMap)
	//{
	//	// get and bind texture
	//	auto l_texture = p_textureManager->getSimpleTexture(m_textureData.m_specularMapName);
	//	l_texture->bind(2);

	//	// set uniform data
	//	p_program->setUniforms("material.hasSpecularTexture", m_textureData.m_hasSpecularMap);
	//	p_program->setUniforms("specularTexture", l_texture->getBoundSlot());

	//}
	//else
	//{
	//	p_program->setUniforms("material.hasSpecularTexture", m_textureData.m_hasSpecularMap);
	//}

	//if (m_textureData.m_hasSpecularMap)
	//{
	//	// get and bind texture
	//	auto l_texture = p_textureManager->getSimpleTexture(m_textureData.m_normalMapName);
	//	l_texture->bind(3);

	//	// set uniform data
	//	p_program->setUniforms("material.hasNormalTexture", m_textureData.m_hasNormalMap);
	//	p_program->setUniforms("normalTexture", l_texture->getBoundSlot());
	//}
	//else
	//{
	//	p_program->setUniforms("material.hasNormalTexture", m_textureData.m_hasNormalMap);
	//}


}

void GraphicsEngine::RenderSkybox(Scene* s)
{


	//glEnable(GL_NORMALIZE);
	//glDisable(GL_DEPTH_TEST);
	//glDepthMask(false);
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

	auto objRender = s->GetSkyBox()->GetComponent<Component::Render>();
	auto  objTransform = s->GetSkyBox()->GetComponent<Component::Transform>();
	//auto & objMaterial = s->GetSkyBox()->GetComponent<Component::Material>();
	Math::Matrix4 modelMatrix = objTransform->getGlobalMatrix();
	auto l_program = ResourceManager::GetResource<ShaderProgram>(objRender->m_shaderProgram);
	const auto l_vao = ResourceManager::GetResource<VertexArrayObject>(objRender->m_modelName);


  //const auto l_material = ResourceManager::GetResource<Material>(objMaterial.m_MaterialName);

	auto l_cubeTexture = ResourceManager::GetResource<CubeTexture>("textures/skybox_1.tpk");
	//l_cubeTexture.lock()->build();
	l_cubeTexture.lock()->bind(0);

	//todo(Cody):FixSybox not being infinite
  s->GetMainCamera()->GetComponent<Component::Camera>()->processNoTransform();
	
	SetCameraUniforms(l_program, s->GetMainCamera());

	l_program.lock()->setUniforms("skybox", true);
	l_program.lock()->setUniforms("ModelMatrix", modelMatrix);
	//l_program.lock()->setUniforms("material.diffuseColor", l_material.lock()->m_diffuseColor);
	l_program.lock()->setUniforms("cubeTexture", l_cubeTexture.lock()->getBoundSlot());

	l_vao.lock()->bind();
	l_vao.lock()->render();
	l_vao.lock()->unbind();

	glDisable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	//glDepthMask(true);
	//glEnable(GL_DEPTH_TEST);
}