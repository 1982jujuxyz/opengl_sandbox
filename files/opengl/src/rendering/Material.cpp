/*!***************************************************************************************
\file       Material.cpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content © 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      Holds iformation about the objects color and how light interacts with it.
*****************************************************************************************/

#include "Precompiled.h"
//========Self Include==================================================================//
#include "rendering/Material.h"
//========1st Party Includes============================================================//
//========3rd Party Includes============================================================//
//========Types=========================================================================//
//========Defines=======================================================================//
//========Static Deceleration===========================================================//

    ///////========================================================================///////
    ///////   Public                                                               ///////
    ///////========================================================================///////

        //==============================================================================//
        //        Getters & Setters                                                     //
        //==============================================================================//
        Material& Material::setMaterialName(std::string const& p_name)
        {
            m_MaterialName = p_name;
            return *this;
        }

        Material& Material::setAmbientColor(Math::Color const & p_color)
        {
            m_ambientColor = p_color;
            return *this;
        }

        Material& Material::setDiffuseColor(Math::Color const & p_color)
        {
            m_diffuseColor = p_color;
            return *this;
        }

        Material& Material::setSpecularColor(Math::Color const & p_color)
        {
            m_specularColor = p_color;
            return *this;
        }

        Material& Material::setEmissiveColor(Math::Color const & p_color)
        {
            m_emissiveColor = p_color;
            return *this;
        }

        Material& Material::setSpecularExponent(float p_pow)
        {
            m_specularExponent = p_pow;
            return *this;
        }

        Material& Material::setIlluminationModel(int p_index)
        {
            m_illumModel = p_index;
            return *this;
        }

        void Material::loadInFile(std::string const & p_assetPath, std::string const& p_fileName)
        {
           

        }




