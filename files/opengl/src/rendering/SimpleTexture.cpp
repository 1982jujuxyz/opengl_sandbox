/*!***************************************************************************************
\file       Texture.hpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      Holds all information about a texture
*****************************************************************************************/
#include "Precompiled.h"
#include "rendering/SimpleTexture.h"




//#include "stb-1.41/stb_image_write.h"

static unsigned char const UnbuiltTexture = 0;
static char const UnboundTexture = -1;

//========Types=========================================================================//
//========Defines=======================================================================//
//========Static Deceleration===========================================================//

///////========================================================================///////
///////   Public                                                               ///////
///////========================================================================///////

//==============================================================================//
//        Constructor                                                           //
//==============================================================================//
SimpleTexture::SimpleTexture() { }

SimpleTexture::SimpleTexture(unsigned p_width, unsigned p_height, FormatType p_format) : m_width(p_width), m_height(p_height), m_format(p_format)
{
    std::size_t l_textureDataSize = (p_format == FormatType::enm_rgb) ?
                    sizeof(unsigned char) * p_width * p_height * 3 :
                    sizeof(unsigned char) * p_width * p_height * 4;

    m_pixelData = new unsigned char[l_textureDataSize];


    std::memset(m_pixelData, 0, l_textureDataSize);
}

//==============================================================================//
//        Destructor                                                            //
//==============================================================================//
SimpleTexture::~SimpleTexture()
{
 
        delete[] m_pixelData;
    
}



void SimpleTexture::setPixelData(unsigned char* p_data)
{


  
        // create internal buffer
        auto l_pixelData = new unsigned char[m_width* m_height* m_bpp];

        // copy data into our buffer
        std::memcpy(l_pixelData, p_data, m_width * m_height * m_bpp);

        m_pixelData = l_pixelData;
    

} 

//==============================================================================//
//        Operators                                                             //
//==============================================================================//

//==============================================================================//
//        Getters & Setters                                                     //
//==============================================================================//

////==========================================================================////
////      Non-Virtual                                                         ////
////==========================================================================////

//////======================================================================//////
//////    Non-Static                                                        //////
//////======================================================================//////

/////========================================================================/////
/////     Functions                                                          /////
/////========================================================================/////

void SimpleTexture::build()
{

    
    GLenum l_format = (m_bpp == 3) ? GL_RGB : GL_RGBA;

    // generate new texture on GPU
    glGenTextures(1, &m_textureHandle);

    // bind to texture on GPU
    glBindTexture(GL_TEXTURE_2D, m_textureHandle);


    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);


    // send texture data    
    glTexImage2D(GL_TEXTURE_2D, 0, l_format, m_width, m_height, 0, l_format, GL_UNSIGNED_BYTE, m_pixelData);

    //GLDebug::getLastError();

    //unbind
    glBindTexture(GL_TEXTURE_2D, 0);


   


}

void SimpleTexture::bind(unsigned int slot)
{
    //TODO(cody):Check if already bound
    if (m_boundSlot != UnboundTexture)
    {
       // logger("SimpleTexture").debug() << "ERROR: texture already bound";
        return;
    }
        

    //http://stackoverflow.com/questions/8866904
    // sets current active object
    glActiveTexture(GL_TEXTURE0 + slot);

    // binds texture to that object
    glBindTexture(GL_TEXTURE_2D, m_textureHandle);

    //GLDebug::getLastError();

    // save slot its in
    m_boundSlot = slot;
}

void SimpleTexture::unbind()
{

    if (m_boundSlot == UnboundTexture)
    {
        //logger("SimpleTexture").debug() << "ERROR: texture already unbound";
        return;
    }
     
    // unbind texture from slot
    glActiveTexture(GL_TEXTURE0 + m_boundSlot);
    glBindTexture(GL_TEXTURE_2D, 0);
    m_boundSlot = UnboundTexture; // unbound = -1
}



///////========================================================================///////
///////   Private                                                              ///////
///////========================================================================///////

//////======================================================================//////
//////    Non-Static                                                        //////
//////======================================================================//////

/////========================================================================/////
/////     Functions                                                          /////
/////========================================================================/////

////==========================================================================////
////      Non-Virtual                                                         ////
////==========================================================================////

//==============================================================================//
//        Helper                                                                //
//==============================================================================//


