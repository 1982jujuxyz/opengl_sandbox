///*!***************************************************************************************
//\file       VertexArrayManager.cpp
//\author     Cannell, Cody
//\date       6/19/18
//\copyright  All content © 2018-2019 DigiPen (USA) Corporation, all rights reserved.
//\par        Project: Boomerang
//\brief      Manages all the VAO objects on the CPU.
//*****************************************************************************************/
//
#include "Precompiled.h"
////========Self Include==================================================================//
//#include "rendering/VertexArrayManager.h"
////========1st Party Includes============================================================//
//#include "rendering/Mesh.h"
//#include "rendering/TypeData.h"
//#include "rendering/IndexBufferObject.h"
//#include "rendering/VertexArrayObject.h"
//#include "rendering/TriangleMesh.h"
////========3rd Party Includes============================================================//
////========Types=========================================================================//
////========Defines=======================================================================//
////========Static Deceleration===========================================================//
//
//	///////========================================================================///////
//	///////   Public                                                               ///////
//	///////========================================================================///////
//
//		//==============================================================================//
//		//        Constructor                                                           //
//		//==============================================================================//
//
//		//==============================================================================//
//		//        Destructor                                                            //
//		//==============================================================================//
//		VertexArrayManager::~VertexArrayManager()
//		{
//		    m_vertexArrayBuffers.clear();
//		}
//
//		//==============================================================================//
//		//        Getters & Setters                                                     //
//		//==============================================================================//
//		std::weak_ptr<VertexArrayObject> const VertexArrayManager::getVertexArrayBuffer(std::string p_filename)
//		{
//			if (!p_filename.empty())
//			{
//				auto l_pos = m_vertexArrayBuffers.find(p_filename);
//				if(l_pos == m_vertexArrayBuffers.end())
//				{
//					//load the file
//					return createArrayBuffer(p_filename);
//				}
//				else
//				{
//					return l_pos->second;
//				}
//					
//			}
//			return 	std::weak_ptr<VertexArrayObject>();
//		}
//
//
//////==========================================================================////
//		////      Non-Virtual                                                         ////
//		////==========================================================================////
//
//		//////======================================================================//////
//		//////    Non-Static                                                        //////
//		//////======================================================================//////
//
//		/////========================================================================/////
//		/////     Functions                                                          /////
//		/////========================================================================/////
//		//std::shared_ptr<VertexArrayObject> VertexArrayManager::createArrayBuffer(std::weak_ptr<TriangleMesh> p_mesh)
//		//{
//		//	
//		//	
//		//	
//
//		//	std::weak_ptr<VertexArrayObject> l_vao = buildVAO(p_mesh.lock());
//
//	 //   m_vertexArrayBuffers.insert(std::make_pair(p_mesh.lock()->m_label, l_vao));
//	 //   return getVertexArrayBuffer(p_mesh.lock()->m_label);
//		//}
//
//		//std::weak_ptr<VertexArrayObject> VertexArrayManager::buildVAO(std::weak_ptr<TriangleMesh> p_mesh)
//		//{
//
//		//	std::vector<TriangleMesh::Vertex> l_vertices = p_mesh.lock()->getVertexData();
//		//	std::vector<TriangleMesh::Face> l_faces = p_mesh.lock()->GetFaceData();
//
//		//	std::shared_ptr<VertexArrayObject> l_vao = std::make_shared<VertexArrayObject>(p_mesh.lock()->getVertCount(), p_mesh.lock()->getFaceCount(), p_mesh.lock()->getVertexSize(), Topology::enm_triangle);
//
//		//	for (TriangleMesh::Vertex vert : l_vertices)
//		//	{
//		//		l_vao->getVertexBufferObject().addVertex(vert);
//		//	}
//
//		//	// Step 3: add all indices (per triangle) to the IBO
//		//	for (TriangleMesh::Face face : l_faces)
//		//	{
//		//		l_vao->getIndexBufferObject().addTriangle(face.a, face.b, face.c);
//		//	}
//
//		//	// Step 4: upload the contents of the VBO and IBO to the GPU and build the VAO
//		//	l_vao->build(p_mesh.lock());
//
//		//	return l_vao;
//		//}
//
//		
//
//
//
//
