#include "Precompiled.h"
#include "rendering/Framebuffer.h"
#include "rendering/TypeData.h"
#include "rendering/SimpleTexture.h"
#include "rendering/GLDebug.h"



Framebuffer::Framebuffer(const Framebuffer& p_rhs) : m_width(p_rhs.m_width), m_height(p_rhs.m_height), m_colorTextureAttachments(p_rhs.m_colorTextureAttachments)
{
    
    //build();

    glBindFramebuffer(GL_READ_FRAMEBUFFER, p_rhs.m_fboHandle);

    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_fboHandle);

    unsigned numAttachments = static_cast<unsigned>(m_colorTextureAttachments.size());

    for (unsigned  i = 0; i < numAttachments; ++i)
    {
        glReadBuffer(GL_COLOR_ATTACHMENT0 + i);
        glDrawBuffer(GL_COLOR_ATTACHMENT0 + i);
        glBlitFramebuffer(0, 0, m_width, m_height, 0, 0, m_width, m_height, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);
        GLenum err = glGetError();
        if (err != GL_NO_ERROR)
        {
            printf("Error while blitting\n");
        }

    }

    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    
}

void Framebuffer::addColorAttachment(std::string const& samplerUniformName)
{

    m_colorTextureAttachments.emplace_back(std::make_shared<SimpleTexture>(m_width, m_height, FormatType::enm_rgb), samplerUniformName );
}

void Framebuffer::bindColorAttachments(std::shared_ptr<ShaderProgram> p_program)
{

    int totalAttachment = static_cast<unsigned>(m_colorTextureAttachments.size());

    for (int i = 0; i < totalAttachment; ++i)
    {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, m_colorTextureAttachments[i].m_texture->getTextureHandle());
        p_program->setUniforms(m_colorTextureAttachments[i].m_samplerUniformName, i);
    }
}

size_t Framebuffer::getBufferSize() const
{
    return 0;
}

void Framebuffer::build()
{
    // build Framebuffer
    glGenFramebuffers(1, &m_fboHandle);
    glBindFramebuffer(GL_FRAMEBUFFER, m_fboHandle);

    // for color buffer use a texture so you can read and write
    int numAttachments = static_cast<unsigned>(m_colorTextureAttachments.size());


    //TODO(cody): Look into RGBA32F for more percision if needed

    for(int i = 0; i < numAttachments; ++i)
    {
        m_colorTextureAttachments[0].m_texture->build();
    }
       

    // for depth and stensal buffer use a Render buffer for read only
    glGenRenderbuffers(1, &m_depthStencilRenderBufferAttachment);
    glBindRenderbuffer(GL_RENDERBUFFER, m_depthStencilRenderBufferAttachment);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, m_width, m_height);
    glBindRenderbuffer(GL_RENDERBUFFER, 0); //unbind

    // attach render buffer
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_depthStencilRenderBufferAttachment);

   

    GLenum* drawBuffers = new GLenum[numAttachments];

    for (int i = 0; i < numAttachments; i++)
    {
        glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + 0, m_colorTextureAttachments[0].m_texture->getTextureHandle(), 0);
        drawBuffers[0] = GL_COLOR_ATTACHMENT0 + 0;
    }

    glDrawBuffers(numAttachments, drawBuffers);
   

   // GLDebug::getLastFramebufferError();

   
    glClearColor(0.f, 0.f, 0.f, 1.f); // default clear color
    glClearDepth(1.f);
    unbind();
    delete[] drawBuffers;
}

void Framebuffer::clear()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Framebuffer::bind()
{

    glBindFramebuffer(GL_FRAMEBUFFER, m_fboHandle);
    //glViewport(0, 0, m_width, m_height);
    //glClearColor(0.f, 0.f, 0.f, 1.1f); // default clear color
   // if (m_fboHandle == NULL) { logger::debug() << "Warning: Binding unbuilt Framebuffer." };
}

void Framebuffer::unbind() const
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0); // bind screen Framebuffer
}

void Framebuffer::shutdown()
{
    unbind();


    glDeleteTextures(1, &m_depthTextureHandle);
    glDeleteFramebuffers(1, &m_fboHandle);
    m_fboHandle = NULL;
    m_depthTextureHandle = NULL;
}

