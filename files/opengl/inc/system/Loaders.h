#pragma once

#define STB_IMAGE_IMPLEMENTATION    
#include "stb-1.41/stb_image.h"
#include "rendering/Material.h"
#include "rendering/TriangleMesh.h"
#include "rendering/Shader.h"
#include "rendering/CubeTexture.h"
#include "rendering/ShaderProgram.h"
#include "rendering/VertexArrayObject.h"
#include <iostream>
#include "ResourceManager.h"
#include "Color.h"

void TriangleMeshLoader(std::filesystem::path & p_pathToAsset, std::weak_ptr<TriangleMesh> p_triangleMesh)
{
	std::vector<float> l_verts;
	std::vector<unsigned> l_indices;
	std::vector<std::vector<unsigned>> l_facedata;
	std::string l_line;

	// Make file path
	std::stringstream l_strstr(p_pathToAsset.string());

	std::ifstream l_infile(l_strstr.str().c_str());


	if (l_infile.fail())
	{
		std::cout << "File with name " << p_pathToAsset.stem().string() << " cannot be opend for loading.\n";
		return;
	}



	while (std::getline(l_infile, l_line))
	{
		if (*l_line.c_str() == '#' || l_line.length() == 0)
		{
			continue;
		}

		std::istringstream l_iss(l_line);
		std::string l_word;
		//char * l_word = std::strtok(const_cast<char *>(l_line.c_str() + 1), " ");
		l_verts.clear();

		l_facedata.clear();
		// std::cout << l_line << std::endl;
		l_iss >> l_word;
		while (l_iss >> l_word)
		{
			if (l_line.c_str()[0] == 'f')
			{
				std::istringstream l_iss2(l_word);
				std::string l_number;

				std::vector<unsigned> temp;
				l_indices.clear();
				while (std::getline(l_iss2, l_number, '/'))
				{
					auto l_num = static_cast<unsigned>(std::strtof(l_number.c_str(), NULL));
					l_indices.push_back(l_num);
				}

				l_facedata.push_back(l_indices);
			}
			else
			{
				// std::cout << l_word << std::endl;
				auto l_num = static_cast<float>(std::strtof(l_word.c_str(), NULL));

				l_verts.push_back(l_num);
			}
		}

		if (l_line.c_str()[0] == 'v' && l_line.c_str()[1] == ' ')
		{
			p_triangleMesh.lock()->addPositionToMesh(l_verts[0], l_verts[1], l_verts[2]);
		}

		if (l_line.c_str()[0] == 'v' && l_line.c_str()[1] == 't')
		{
			p_triangleMesh.lock()->uvsSet = true;
			p_triangleMesh.lock()->addUVToMesh(l_verts[0], l_verts[1]);
		}

		if (l_line.c_str()[0] == 'v' && l_line.c_str()[1] == 'n')
		{
			p_triangleMesh.lock()->normalsSet = true;
			p_triangleMesh.lock()->addNormalToMesh(l_verts[0], l_verts[1], l_verts[2]);

		}

		if (l_line.c_str()[0] == 'f' && l_line.c_str()[1] == ' ')
		{
			p_triangleMesh.lock()->m_indices.push_back(l_facedata);

			//addFaceToMesh(static_cast<unsigned>(l_verts[0] - 1), static_cast<unsigned>(l_verts[1] - 1), static_cast<unsigned>(l_verts[2] - 1));
		}
	}


	//=============================== Process ==========================//

	p_triangleMesh.lock()->m_vertices.resize(p_triangleMesh.lock()->m_position.size());

	// checks if the vertex already exist and we need to make a new vertex
	std::vector<bool> setCheck(p_triangleMesh.lock()->m_position.size(), 0);

	// an index of normals that need to be added together 
	//std::vector<std::vector<int>> accumulateNormalIntex(8);

	for (auto faceData : p_triangleMesh.lock()->m_indices)
	{
		if (faceData.size() == 3)
		{
			unsigned indexA = faceData[0][0];
			unsigned indexB = faceData[1][0];
			unsigned indexC = faceData[2][0];

			if (!setCheck[faceData[0][0] - 1])
			{
				setCheck[faceData[0][0] - 1] = true;
				//accumulateNormalIntex[faceData[0][0] - 1].push_back(indexA);
			}
			else
			{
				p_triangleMesh.lock()->m_vertices.emplace_back();
				indexA = static_cast<unsigned>(p_triangleMesh.lock()->m_vertices.size());
				// accumulateNormalIntex[faceData[0][0] - 1].push_back(indexA);
			}

			if (!setCheck[faceData[1][0] - 1])
			{
				setCheck[faceData[1][0] - 1] = true;
				//accumulateNormalIntex[faceData[1][0] - 1].push_back(indexB);
			}
			else
			{
				p_triangleMesh.lock()->m_vertices.emplace_back();
				indexB = static_cast<unsigned>(p_triangleMesh.lock()->m_vertices.size());
				//accumulateNormalIntex[faceData[1][0] - 1].push_back(indexB);
			}

			if (!setCheck[faceData[2][0] - 1])
			{
				setCheck[faceData[2][0] - 1] = true;
				//accumulateNormalIntex[faceData[2][0] - 1].push_back(indexC);
			}
			else
			{
				p_triangleMesh.lock()->m_vertices.emplace_back();
				indexC = static_cast<unsigned>(p_triangleMesh.lock()->m_vertices.size());
				//accumulateNormalIntex[faceData[2][0] - 1].push_back(indexC);
			}


			p_triangleMesh.lock()->addFaceToMesh(indexA - 1, indexB - 1, indexC - 1);

			p_triangleMesh.lock()->m_vertices[indexA - 1].m_position = p_triangleMesh.lock()->m_position[faceData[0][0] - 1];
			p_triangleMesh.lock()->m_vertices[indexB - 1].m_position = p_triangleMesh.lock()->m_position[faceData[1][0] - 1];
			p_triangleMesh.lock()->m_vertices[indexC - 1].m_position = p_triangleMesh.lock()->m_position[faceData[2][0] - 1];

			p_triangleMesh.lock()->m_vertices[indexA - 1].m_uv = p_triangleMesh.lock()->m_uvs[faceData[0][1] - 1];
			p_triangleMesh.lock()->m_vertices[indexB - 1].m_uv = p_triangleMesh.lock()->m_uvs[faceData[1][1] - 1];
			p_triangleMesh.lock()->m_vertices[indexC - 1].m_uv = p_triangleMesh.lock()->m_uvs[faceData[2][1] - 1];

			p_triangleMesh.lock()->m_vertices[indexA - 1].m_normal = p_triangleMesh.lock()->m_triangleNormals[faceData[0][2] - 1];
			p_triangleMesh.lock()->m_vertices[indexB - 1].m_normal = p_triangleMesh.lock()->m_triangleNormals[faceData[1][2] - 1];
			p_triangleMesh.lock()->m_vertices[indexC - 1].m_normal = p_triangleMesh.lock()->m_triangleNormals[faceData[2][2] - 1];
		}

		if (faceData.size() == 4)
		{

			unsigned indexA = faceData[0][0];
			unsigned indexB = faceData[1][0];
			unsigned indexC = faceData[2][0];
			unsigned indexD = faceData[3][0];


			if (!setCheck[faceData[0][0] - 1])
			{
				setCheck[faceData[0][0] - 1] = true;
				// accumulateNormalIntex[faceData[0][0] - 1].push_back(indexA);
			}
			else
			{
				p_triangleMesh.lock()->m_vertices.emplace_back();
				indexA = static_cast<unsigned>(p_triangleMesh.lock()->m_vertices.size());
				// accumulateNormalIntex[faceData[0][0] - 1].push_back(indexA);
			}

			if (!setCheck[faceData[1][0] - 1])
			{
				setCheck[faceData[1][0] - 1] = true;
				//accumulateNormalIntex[faceData[1][0] - 1].push_back(indexB);
			}
			else
			{

				p_triangleMesh.lock()->m_vertices.emplace_back();
				indexB = static_cast<unsigned>(p_triangleMesh.lock()->m_vertices.size());
				//accumulateNormalIntex[faceData[1][0] - 1].push_back(indexB);
			}

			if (!setCheck[faceData[2][0] - 1])
			{
				setCheck[faceData[2][0] - 1] = true;
				//accumulateNormalIntex[faceData[2][0] - 1].push_back(indexC);
			}
			else
			{

				p_triangleMesh.lock()->m_vertices.emplace_back();
				indexC = static_cast<unsigned>(p_triangleMesh.lock()->m_vertices.size());
				//accumulateNormalIntex[faceData[2][0] - 1].push_back(indexC);
			}

			if (!setCheck[faceData[3][0] - 1])
			{
				setCheck[faceData[3][0] - 1] = true;
				// accumulateNormalIntex[faceData[3][0] - 1].push_back(indexD);
			}
			else
			{

				p_triangleMesh.lock()->m_vertices.emplace_back();
				indexD = static_cast<unsigned>(p_triangleMesh.lock()->m_vertices.size());
				//accumulateNormalIntex[faceData[3][0] - 1].push_back(indexD);
			}

			p_triangleMesh.lock()->addFaceToMesh(indexA - 1, indexB - 1, indexC - 1);
			p_triangleMesh.lock()->addFaceToMesh(indexC - 1, indexD - 1, indexA - 1);

			p_triangleMesh.lock()->m_vertices[indexA - 1].m_position = p_triangleMesh.lock()->m_position[faceData[0][0] - 1];
			p_triangleMesh.lock()->m_vertices[indexB - 1].m_position = p_triangleMesh.lock()->m_position[faceData[1][0] - 1];
			p_triangleMesh.lock()->m_vertices[indexC - 1].m_position = p_triangleMesh.lock()->m_position[faceData[2][0] - 1];
			p_triangleMesh.lock()->m_vertices[indexD - 1].m_position = p_triangleMesh.lock()->m_position[faceData[3][0] - 1];

			p_triangleMesh.lock()->m_vertices[indexA - 1].m_uv = p_triangleMesh.lock()->m_uvs[faceData[0][1] - 1];
			p_triangleMesh.lock()->m_vertices[indexB - 1].m_uv = p_triangleMesh.lock()->m_uvs[faceData[1][1] - 1];
			p_triangleMesh.lock()->m_vertices[indexC - 1].m_uv = p_triangleMesh.lock()->m_uvs[faceData[2][1] - 1];
			p_triangleMesh.lock()->m_vertices[indexD - 1].m_uv = p_triangleMesh.lock()->m_uvs[faceData[3][1] - 1];

			p_triangleMesh.lock()->m_vertices[indexA - 1].m_normal = p_triangleMesh.lock()->m_triangleNormals[faceData[0][2] - 1];
			p_triangleMesh.lock()->m_vertices[indexB - 1].m_normal = p_triangleMesh.lock()->m_triangleNormals[faceData[1][2] - 1];
			p_triangleMesh.lock()->m_vertices[indexC - 1].m_normal = p_triangleMesh.lock()->m_triangleNormals[faceData[2][2] - 1];
			p_triangleMesh.lock()->m_vertices[indexD - 1].m_normal = p_triangleMesh.lock()->m_triangleNormals[faceData[3][2] - 1];


		}

	}


}

void MaterialLoader(std::filesystem::path & p_pathToAsset, std::weak_ptr<Material> m_material)
{
	std::stringstream l_strstr(p_pathToAsset.string());

	//The Material file name is the file name under assets\Materials\ directory
	//once you load them in, make sure you call function empalceMaterial after a Material
	//is loaded and setup, so it will be stored in the container.


	std::ifstream l_infile(l_strstr.str());
	std::string l_line;

	if (l_infile.fail())
	{
		std::cout << "File with name " << p_pathToAsset.stem().string() << " cannot be opend for loading.\n";
		return;
	}


	while (std::getline(l_infile, l_line))
	{
		if (*l_line.c_str() == '#')
			continue;

		if (l_line.length() == 0)
			continue;

		std::stringstream  l_stringstream;
		l_stringstream << l_line;
		std::string word;
		float red, green, blue, ex;
		while (l_stringstream >> word)
		{
			//word.push_back( '\0');
			if (!word.compare("newmtl"))
			{
				l_stringstream >> word; ;

				//  logger::debug() << "setMaterialName: " << word << "\n";
				m_material.lock()->setMaterialName(word);


				break;
			}
			else if (!word.compare("illum"))
			{
				l_stringstream >> word;
				// logger::debug() << "setIlluminationModel: " << word << "\n";
				m_material.lock()->setIlluminationModel(std::stol(word.c_str(), NULL, 10));
				break;
			}
			else if (!word.compare("Ka"))
			{

				l_stringstream >> red;
				l_stringstream >> green;
				l_stringstream >> blue;

				//  logger::debug() << "setAmbientColor: (" << red <<", " << green << ", " << blue << ")\n";
				m_material.lock()->setAmbientColor(Math::Color(red, green, blue));
				break;
			}
			else if (!word.compare("Kd"))
			{

				l_stringstream >> red;
				l_stringstream >> green;
				l_stringstream >> blue;

				// logger::debug() << "setDiffuseColor: (" << red << ", " << green << ", " << blue << ")\n";
				m_material.lock()->setDiffuseColor(Math::Color(red, green, blue));
				break;
			}
			else if (!word.compare("Ks"))
			{

				l_stringstream >> red;
				l_stringstream >> green;
				l_stringstream >> blue;

				// logger::debug() << "setSpecularColor: (" << red << ", " << green << ", " << blue << ")\n";
				m_material.lock()->setSpecularColor(Math::Color(red, green, blue));
				break;
			}
			else if (!word.compare("Ke"))
			{

				l_stringstream >> red;
				l_stringstream >> green;
				l_stringstream >> blue;

				// logger::debug() << "setEmissiverColor: (" << red << ", " << green << ", " << blue << ")\n";
				m_material.lock()->setEmissiveColor(Math::Color(red, green, blue));
				break;
			}
			else if (!word.compare("Ns"))
			{
				l_stringstream >> ex;
				// logger::debug() << "setSpecularExponent: "<< ex <<"\n";
				m_material.lock()->setSpecularExponent(ex);
				break;
			}
			else if (!word.compare("map_Kd"))
			{
				l_stringstream >> word;
				//logger("Material").debug() << "AssignDiffuseTexture: " << word << "\n";
				m_material.lock()->m_diffuseMap = std::string(word.c_str());
				m_material.lock()->m_hasDiffuseMap = true;

				break;
			}
			else if (!word.compare("map_Ks"))
			{
				l_stringstream >> word;

				m_material.lock()->m_specularMap = std::string(word.c_str());
				m_material.lock()->m_hasSpecularMap = true;
				//logger::debug() << "AssignSpecularTexture: "<< word << "\n";


				break;
			}
			else if (!word.compare("map_Kn"))
			{
				l_stringstream >> word;

				m_material.lock()->m_normalMap = std::string(word.c_str());
				m_material.lock()->m_hasNormalMap = true;

				// logger::debug() << "AssignNormalMapTexture: "<< word << "\n";


				break;
			}
			else
			{
				break;
			}
		}

		word.clear();
	}
	m_material.lock()->m_isLoaded = true;
}

void TextureLoader(std::filesystem::path & p_pathToAsset, std::weak_ptr<SimpleTexture> p_texture)
{
    int l_width, l_height, l_bpp;

		

    //GLenum l_format = (m_format == FormatType::enm_rgb) ? STBI_rgb : STBI_rgb_alpha;

    stbi_set_flip_vertically_on_load(true);
    unsigned char* textureData = stbi_load(p_pathToAsset.string().c_str(), &l_width, &l_height, &l_bpp, 0);

    if (!textureData)
    {
      std::cout << "ERROR: stbi_load failed" << std::endl;
      return;
    }


		p_texture.lock()->m_width = l_width;
		p_texture.lock()->m_height = l_height;
		p_texture.lock()->m_bpp = l_bpp;

    if (!textureData)
    {
			//logger("SimpleTexture").debug() << "stbi_load failed";
    }

		p_texture.lock()->setPixelData(textureData);

    stbi_image_free(textureData);
}

void CubeTextureLoader(std::filesystem::path & p_pathToAsset, std::weak_ptr<CubeTexture> p_cubeTexture)
{
	std::vector<std::weak_ptr<Shader>>l_textureList;
	std::vector<std::string>l_shaderFiles;
	std::string l_line;

	std::stringstream l_strstr(p_pathToAsset.string());

	std::ifstream l_infile(l_strstr.str().c_str());

	if (l_infile.fail())
	{
		std::cout << "File with name %s cannot be opend for loading.\n";
		return;
	}

	while (std::getline(l_infile, l_line, ','))
	{
		auto l_texture = ResourceManager::GetResource<SimpleTexture>(l_line);
		p_cubeTexture.lock()->m_textures.push_back(l_texture);
	}
}

void ShaderLoader(std::filesystem::path & p_pathToAsset, std::weak_ptr<Shader> m_shader)
{
	std::ifstream file(p_pathToAsset.string(), std::ios::in | std::ios::binary);
	if (file.is_open())
	{
		std::stringstream buffer;
		buffer << file.rdbuf();
		m_shader.lock()->setSourceData(buffer.str());
	}
	else
	{
		// error code
	}
		
}

void ShaderProgramLoader(std::filesystem::path& p_pathToAsset, std::weak_ptr<ShaderProgram> m_shaderProgram)
{

	std::vector<std::weak_ptr<Shader>>l_shaderList;
	std::vector<std::string>l_shaderFiles;
	std::string l_line;

	// Make file path
	std::stringstream l_strstr(p_pathToAsset.string());


	std::ifstream l_infile(l_strstr.str().c_str());
	if (l_infile.fail())
	{
		std::cout << "File with name %s cannot be opend for loading.\n";
	}



	while (std::getline(l_infile, l_line, ','))
	{
		auto l_shader = ResourceManager::GetResource<Shader>(l_line);
		l_shaderList.push_back(l_shader);
	}
	


	m_shaderProgram.lock()->AddVertexShader(l_shaderList[0]);
	m_shaderProgram.lock()->AddFragmentShader(l_shaderList[1]);
}

void VertexArrayLoader(std::filesystem::path & p_pathToAsset, std::weak_ptr<VertexArrayObject> p_vao)
{
	auto l_mesh = std::make_shared<TriangleMesh>();
	TriangleMeshLoader(p_pathToAsset, l_mesh);

	std::vector<TriangleMesh::Vertex> l_vertices = l_mesh->getVertexData();
	std::vector<TriangleMesh::Face> l_faces = l_mesh->GetFaceData();


	p_vao.lock()->InitializeIBO(Topology::enm_triangle, l_mesh->getFaceCount());
	p_vao.lock()->InitializeVBO(l_mesh->getVertCount(), l_mesh->getVertexSize());
	
	for (TriangleMesh::Vertex vert : l_vertices)
	{
		p_vao.lock()->getVertexBufferObject()->addVertex(vert);
	}

	// Step 3: add all indices (per triangle) to the IBO
	for (TriangleMesh::Face face : l_faces)
	{
		p_vao.lock()->getIndexBufferObject()->addTriangle(face.a, face.b, face.c);
	}

	// Step 4: upload the contents of the VBO and IBO to the GPU and build the VAO
	p_vao.lock()->build(l_mesh);

	l_mesh.reset();
	
}
