//lowercase:
//doxygen:
//tablen:
#pragma once


#include <map>
#include <functional>
#include <filesystem>
#include <deque>
#include <memory>
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <typeindex>
#include <algorithm>

//#define GLEW_STATIC
#include <glew-2.1.0/glew.h> // GLuint
#include <glfw-3.2.1/glfw3.h>

#define _CRT_SECURE_NO_WARNINGS



#define UNREFERENCED_PARAMETER(X) (void)X

