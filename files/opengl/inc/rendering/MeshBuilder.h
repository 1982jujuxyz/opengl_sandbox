/*!***************************************************************************************
\file       MeshBuilder.hpp
\author     Last-Name, First-Name
\date       6/19/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      Static class that Builds all mesh types on the GPU and creates CPU objects.
*****************************************************************************************/

#pragma once
enum class DefaultUvType;
//========Self Include==================================================================//
//========1st Party Includes============================================================//
//========3rd Party Includes============================================================//
//========Types=========================================================================//
//========Defines=======================================================================//
//========Forward Deceleration==========================================================//
class TriangleMesh;
class VertexArrayObject;
class ShaderProgram;

/////////========================================================================/////////
///////// Namespace                                                              /////////
/////////========================================================================/////////

////////==========================================================================////////
////////  Struct / Class                                                          ////////
////////==========================================================================////////

/*!***************************************************************************************
\par class: meshBuilder(static)
\brief   builds all mesh types on the GPU and creates CPU objects
*****************************************************************************************/
class MeshBuilder
{
	///////============================================================================///////
	///////   Public                                                                   ///////
	///////============================================================================///////
public:

	//////==============================================================================//////
	//////    (Non-)Static                                                              //////
	//////==============================================================================//////

	/*!***************************************************************************************
	\brief  Builds a simple cube object
	\param  p_mesh - the mesh data
	\param  p_vao  - the GPU data object
	\param  program  the GPU program that holds the data
	*****************************************************************************************/
	//static void cubeBuilder(std::shared_ptr<TriangleMesh> p_mesh,
	//	std::shared_ptr<VertexArrayObject> p_vao);


	static std::shared_ptr<TriangleMesh> buildFullScreenQuad(std::string const &meshLabel);

	static std::shared_ptr<TriangleMesh> buildSphere(std::string const &meshLabel, DefaultUvType defaultUvType);

	//TODO(cody): create a line object builder
	/*!***************************************************************************************
	\brief  (Comming Soon) makes a line
	*****************************************************************************************/
	static void lineBuilder();
};
