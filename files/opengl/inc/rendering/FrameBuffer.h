#ifndef H_Framebuffer
#define H_Framebuffer
#include "Buffer.h"
#include "Color.h"

#include "SimpleTexture.h"
#include "ShaderProgram.h"


struct attachment
{
    attachment(std::shared_ptr<SimpleTexture> p_texture, std::string p_samplerUniformName) : m_texture(p_texture), m_samplerUniformName(p_samplerUniformName) {}
    std::shared_ptr<SimpleTexture> m_texture;
    std::string m_samplerUniformName;
   
};

class Framebuffer : public IBuffer
{
public:

    Framebuffer(unsigned p_width, unsigned p_height) : m_width(p_width), m_height(p_height) {}

    Framebuffer(const Framebuffer & p_rhs);

    void addColorAttachment(std::string const& samplerUniformName);
    void bindColorAttachments(std::shared_ptr<ShaderProgram> p_program);

    ~Framebuffer() { shutdown(); }

    size_t getBufferSize() const override;
    void build() override;
    void bind() override;
    void unbind() const override;
    void clear();
    void shutdown() override;

    bool isDirty() { return m_dirty; }
    void setDirty(bool p_dirty) { m_dirty = p_dirty; }

    bool isFirstFrame() { return m_firstFrame; }
    void setIsFirstFrame(bool p_firstFrame) { m_firstFrame = p_firstFrame; }
   

private:

    unsigned int m_width = 0;
    unsigned int m_height = 0;
    Math::Color m_clearColor = Math::Color::cyan;
    GLuint m_fboHandle = 0;
    GLuint m_depthTextureHandle = 0;

    std::vector<attachment> m_colorTextureAttachments;
    GLuint m_depthStencilRenderBufferAttachment = 0;

    bool m_dirty = false;

    bool m_firstFrame = false;
    
};

#endif //H_Framebuffer