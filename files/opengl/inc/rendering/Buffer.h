#ifndef H_BUFFER
#define H_BUFFER

class IBuffer 
{
public:
    virtual ~IBuffer() = default;

    virtual std::size_t getBufferSize() const = 0;

    virtual void build() = 0;

    virtual void bind() = 0;

    virtual void unbind() const = 0;

    virtual void shutdown() = 0;

};

#endif //H_BUFFER