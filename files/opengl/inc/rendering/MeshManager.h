///*!***************************************************************************************
//\file       MeshManager.hpp
//\author     Cannell, Cody
//\date       6/19/18
//\copyright  All content © 2018-2019 DigiPen (USA) Corporation, all rights reserved.
//\par        Project: Boomerang
//\brief      Builds and manages all the meshes used by the VAO
//*****************************************************************************************/
//#pragma once
//enum class DefaultUvType;
////========Self Include==================================================================//
////========1st Party Includes============================================================//
////========3rd Party Includes============================================================//
////========Types=========================================================================//
////========Defines=======================================================================//
////========Forward Deceleration==========================================================//
//class TriangleMesh;
//class Mesh;
//enum class PrimitiveType;
///////////========================================================================/////////
/////////// Namespace                                                              /////////
///////////========================================================================/////////
//
//////////==========================================================================////////
//////////  Struct / Class                                                          ////////
//////////==========================================================================////////
//
///*!***************************************************************************************
//\par class: MeshManager
//\brief   Builds and manages all the meshes used by the VAO.
//*****************************************************************************************/
//class MeshManager
//{
//    ///////========================================================================///////
//    ///////   Public                                                               ///////
//    ///////========================================================================///////
//
//public:
//
//    MeshManager() {}
//
//    /*!***********************************************************************************
//    \brief  Creates a mesh from a file and gives it a name
//    \param  p_assetPath - the root path to the assets directory.
//    \param  p_meshLabel - the name given to the mesh
//    \param  p_filename  - the file to be loaded
//    \param  p_uvType    - the uv mapping used for this mesh
//    \return a shared pointer to the mesh that was created
//    *************************************************************************************/
//    //std::shared_ptr<TriangleMesh> createMesh(std::string p_filename);
//
//    //==================================================================================//
//    //        Getters & Setters                                                         //
//    //==================================================================================//
//    /*!***********************************************************************************
//    \brief  add a preconstructed mesh to the manager
//    \param  meshLabel  - the name of the mesh
//    \param  p_mesh       a shared pointer to the mesh you want to add
//    *************************************************************************************/
//    void addMesh(std::string meshLabel, std::shared_ptr<TriangleMesh> p_mesh);
//
//    std::shared_ptr<TriangleMesh> buildFullScreenQuad(std::string const &meshLabel);
//
//    std::shared_ptr<TriangleMesh> buildSphere(std::string const &meshLabel, DefaultUvType defaultUvType);
//
//    /*!***********************************************************************************
//    \brief  Gets a mesh from the manager
//    \param  p_label -  the name of the mesh you wan to get.
//    \return a shared pointer to the mesh that you want
//    *************************************************************************************/
//    std::shared_ptr<TriangleMesh> const getMesh(std::string p_label) const;
//    ///////========================================================================///////
//    ///////       Private                                                          ///////
//    ///////========================================================================///////
//
//private:
//    std::unordered_map<std::string, std::shared_ptr<TriangleMesh> > m_meshes; //!< holds meshes by name
// 
//};
//
