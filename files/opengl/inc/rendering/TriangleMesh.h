/*!***************************************************************************************
\file       TriangleMesh.hpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      This holds all data for constructing a mesh made of triangles
*****************************************************************************************/


#pragma once
//========Self Include==================================================================//
//========1st Party Includes============================================================//

#include "Mesh.h"
#include "ResourceManager.h"
enum class DefaultUvType;
//========3rd Party Includes============================================================//
//========Types=========================================================================//
//========Defines=======================================================================//
//========Forward Deceleration==========================================================//
//struct Vector3;

/////////========================================================================/////////
///////// Namespace                                                              /////////
/////////========================================================================/////////

////////==========================================================================////////
////////  Struct / Class                                                          ////////
////////==========================================================================////////

/*!***************************************************************************************
\par class: triangleMesh
\brief      This holds all data for constructing a mesh made of triangles.
*****************************************************************************************/
class TriangleMesh : public Resource, public Mesh
{

	///////============================================================================///////
	///////   Public                                                                   ///////
	///////============================================================================///////

public:



	/*!***************************************************************************************
	\brief  Constructor for triangle mesh
	\param  p_meshLabel - mesh name
	\param  p_bShape - bounding shape
	*****************************************************************************************/
	TriangleMesh() {};

	/*!***************************************************************************************
	\brief  Loads In an constructs the mesh
	\param  p_assetPath - Path to the assets folder
	\param  p_objFileName - The object file to be loaded in
*****************************************************************************************/
	void loadInFile(std::string const & p_assetPath, std::string const& p_objFileName);

	/*!***************************************************************************************
	\brief triangleMesh copy constructor
	\param p_other other triangleMesh to copy
	*****************************************************************************************/
	TriangleMesh(const TriangleMesh& p_other);

	~TriangleMesh() override = default;

	/*!***************************************************************************************
	\par struct: face
	\brief      This holds all data for a single triangle face.
	*****************************************************************************************/
	struct Face
	{
		int a;  //!<  Point a
		int b;  //!<  Point b
		int c;  //!<  Point c

		/*!***************************************************************************************
		\brief  Constructor for a face
		\param  a - First point
		\param  b - Second point
		\param  c - Third point
		*****************************************************************************************/
		Face(int a = -1, int b = -1, int c = -1)
			: a(a), b(b), c(c) {}
	};



	/*!***************************************************************************************
	\brief  Adds a face to the mesh
	\param  a - First point
	\param  b - Second point
	\param  c - Third point
	*****************************************************************************************/
	void addFaceToMesh(unsigned a, unsigned b, unsigned c);

	void addUVToMesh(float u, float v);

	void addPositionToMesh(float a, float b, float c);

	void addNormalToMesh(float x, float y, float z);



	/*!***************************************************************************************
	\brief  Gets all faces of triangle mesh
	\return A shared pointer to all the faces
	*****************************************************************************************/
	std::vector<Face> GetFaceData() const { return m_faces; } // TODO(cody): Make LowerCase

	/*!***************************************************************************************
	\brief   Gets the count of how many faces for this mesh
	\return  The size_t of the count
	*****************************************************************************************/
	std::size_t getFaceCount() const { return m_faces.size(); }



	/*!***************************************************************************************
	\brief Calulates all the face normals and calls centerMesh()
	\param p_uvType - The uv mapping used
	*****************************************************************************************/
	void preprocess(DefaultUvType p_uvType);
	void process();

	TriangleMesh* CalcUvBox();

	//void normalizeVertices();
	

	//======================================================================================//
	//        Operators                                                                     //
	//======================================================================================//
	/*!***************************************************************************************
	\brief triangleMesh assignment operator
	\param p_other other triangleMesh to be equal to
	\return Reference to lhs  triangleMesh
	*****************************************************************************************/
	TriangleMesh& operator=(const TriangleMesh& p_other);
	//triangleMesh* CalcTanBitan();

	///////============================================================================///////
	///////   Private                                                                  ///////
	///////============================================================================///////
	///
	bool normalsSet = false;
	bool uvsSet = false;

public:

	/////================================================================================/////
	/////    Data                                                                        /////
	/////================================================================================/////

	/*!***************************************************************************************
	\brief  Moves the mesh center to 0,0
	*****************************************************************************************/
	void centerMesh();



	Math::Vector3 m_center = { 0,0,0 };                //!<  The center of the mesh
	std::vector<Face> m_faces;                   //!<  Holds all the faces of the mesh
	std::vector<Math::Vector3> m_position;
	std::vector<Math::Vector3> m_triangleNormals;      //!<  holds all the normals of the faces
	std::vector<Math::Vector2> m_uvs;
	std::vector<Math::Vector3> m_tangent;
	std::vector<Math::Vector3> m_bitangent;


	std::vector<std::vector<std::vector<unsigned>>> m_indices;



};

