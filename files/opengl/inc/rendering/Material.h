/*!***************************************************************************************
\file       Material.hpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content © 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      Holds iformation about the objects color and how light interacts with it
*****************************************************************************************/
#pragma once
//========1st Party Includes============================================================//
#include "Color.h"
#include "ResourceManager.h"
//========3rd Party Includes============================================================//
//========Types=========================================================================//
//========Defines=======================================================================//
//========Forward Deceleration==========================================================//

////////==========================================================================////////
////////  Struct / Class                                                          ////////
////////==========================================================================////////


/*!***************************************************************************************
\par class: Material
\brief   This hold all data about how light interacts with an object material
*****************************************************************************************/
class Material : public Resource
{
    ///////============================================================================///////
    ///////   public                                                                   ///////
    ///////============================================================================///////
public:

    //======================================================================================//
    //        Constructor                                                                   //
    //======================================================================================//
    Material() = default;

    /*!***************************************************************************************
    \brief  Copy Constructor (default)
    \param  Material - the Material to be copyed
    *****************************************************************************************/
    Material(Material const& Material) = delete; 

    //======================================================================================//
    //        Destructor                                                                    //
    //======================================================================================//

    /*!***************************************************************************************
    \brief  Destructor (default)
    *****************************************************************************************/
    ~Material() = default;

    //======================================================================================//
    //        Getters & Setters                                                             //
    //======================================================================================//
    /*!***************************************************************************************
    \brief   Gets the Materials name
    \return  The name of the Material
    *****************************************************************************************/
    std::string getMaterialName() const { return m_MaterialName; }
    std::string getFileName() const { return m_fileName; }

    /*!***************************************************************************************
    \brief   Sets the Materials name
    \param   p_name - Name of Material
    \return  The Material whos name was set
    *****************************************************************************************/
    Material& setMaterialName(std::string const& p_name);

    /*!***************************************************************************************
    \brief   Gets the ambient color
    \return  A copy of the ambient color
    *****************************************************************************************/
    Math::Color getAmbientColor() const { return m_ambientColor; }

    /*!***************************************************************************************
    \brief   Sets the ambient color
    \param   p_color - color passed in
    \return  The Material whos ambient color was set
    *****************************************************************************************/
    Material& setAmbientColor(Math::Color const& p_color);

    /*!***************************************************************************************
    \brief   Gets the diffuse color
    \return  A copy of the diffuse color
    *****************************************************************************************/
    Math::Color getDiffuseColor() const { return m_diffuseColor; }

    /*!***************************************************************************************
    \brief   Sets the diffuse color
    \param   p_color - color passed in
    \return  The Material whos diffuse color was set
    *****************************************************************************************/
    Material& setDiffuseColor(Math::Color const& p_color);

    /*!***************************************************************************************
    \brief   Gets the specular color
    \return  A copy of the specular color
    *****************************************************************************************/
    Math::Color getSpecularColor() const { return m_specularColor; }

    /*!***************************************************************************************
    \brief   Sets the specular color
    \param   p_color - color passed in
    \return  The Material whos specular color was set
    *****************************************************************************************/
    Material& setSpecularColor(Math::Color const& p_color);

    /*!***************************************************************************************
    \brief   Get the emissive color
    \return  A copy of the emissive color
    *****************************************************************************************/
    Math::Color getEmissiveColor() const { return m_emissiveColor; }

    /*!***************************************************************************************
    \brief   Set the emissive color
    \param   p_color - color passed in
    \return  The Material whos emissive color was set
    *****************************************************************************************/
    Material& setEmissiveColor(Math::Color const& p_color);

    /*!***************************************************************************************
    \brief  Gets the specular exponent
    \return A copy of the specular exponent
    *****************************************************************************************/
    float getSpecularExponent() const { return m_specularExponent; }

    /*!***************************************************************************************
    \brief   Sets the specular exponent
    \param   p_pow - power of specular exponent
    \return  The Material whos specular exponent was set
    *****************************************************************************************/
    Material& setSpecularExponent(float p_pow);

    /*!***************************************************************************************
    \brief   Gets the IlluminationModel
    \return  A copy of the IlluminationModel number
    *****************************************************************************************/
    int getIlluminationModel() const { return m_illumModel; }

    /*!***************************************************************************************
    \brief   Sets the illumination model
    \param   p_index - index of illumination model
    \return  The Material whos IlluminationModel was set
    *****************************************************************************************/
    Material& setIlluminationModel(int p_index);

    bool hasDiffuseMap() { return m_hasDiffuseMap; }
    bool hasSpecularMap() { return m_hasSpecularMap; }
    bool hasNormalMap() { return m_hasNormalMap; }
    bool hasAmbiantOcclussionMap() { return m_hasAmbiantOcclussionMap; }
    bool hasHeightMap() { return m_hasHeightMap; }

    bool isLoaded() { return m_isLoaded;  }
   

    std::string getDiffuseMapName(){ return m_diffuseMap; }
    std::string getAmbiantOcclussionMapName() { return m_ambiantOcclussionMap; }
    std::string getSpecularMapName() { return m_specularMap; }
    std::string getNormalMapName() { return m_normalMap; }
    std::string getHeightMapName() { return m_heightMap; }

    void loadInFile(std::string const & p_assetPath, std::string const& p_fileName);

    ///////============================================================================///////
    ///////   Private                                                                  ///////
    ///////============================================================================///////

public:

    /////================================================================================/////
    /////     Data                                                                       /////
    /////================================================================================/////

    std::string m_MaterialName;     //!< The name of the Material
    std::string m_fileName;

    Math::Color m_ambientColor = Math::Color(0,0,0);           //!< The Color that gets reflected
    Math::Color m_diffuseColor = Math::Color(0, 0, 0);           //!< The color of the object
    Math::Color m_specularColor = Math::Color(0, 0, 0);          //!< hightlights on a shinny surface
    Math::Color m_emissiveColor = Math::Color(0, 0, 0);          //!< self-illumination color (neon)

    int m_illumModel = 0;               //!< allows to turn on and off parts of shader
    float m_specularExponent = 0;       //!< shinny factor

    std::string m_diffuseMap;
    std::string m_specularMap;
    std::string m_normalMap;
    std::string m_ambiantOcclussionMap;
    std::string m_heightMap;

    bool m_hasDiffuseMap = false;
    bool m_hasSpecularMap = false;
    bool m_hasNormalMap = false;
    bool m_hasAmbiantOcclussionMap = false;
    bool m_hasHeightMap = false;

    bool m_isLoaded = false;
};
