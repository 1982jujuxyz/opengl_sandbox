/*!***************************************************************************************
\file       VertexArrayObject.hpp
\author     Last-Name, First-Name
\date       6/19/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief  This is a work in progress example sheet for section headers.
*****************************************************************************************/
#pragma once
//========Self Include==================================================================//
//========1st Party Includes============================================================//
#include "VertexBufferObject.h"
#include "IndexBufferObject.h"
#include "ResourceManager.h"

//========3rd Party Includes============================================================//
//========Types=========================================================================//
//========Defines=======================================================================//
//========Forward Deceleration==========================================================//
class Mesh;
enum class Topology;

////////==========================================================================////////
////////   Class                                                          ////////
////////==========================================================================////////

/*!***************************************************************************************
\par class: vertexArrayObject
\brief      This is a stub class to be used as a template / example for class formating.
*****************************************************************************************/
class VertexArrayObject : public Resource
{

	///////============================================================================///////
	///////   Public                                                                   ///////
	///////============================================================================///////
public:


	//======================================================================================//
	//     Constructors                                                                     //
	//======================================================================================//

	/*!***************************************************************************************
	\brief  VAO constructor
	\param  vertexCount    - number of vertex for building buffer size
	\param  primitiveCount - the number of primitive shapes (eg. triangle)
	\param  vertexSize     - the size of a vertex
	\param topology        - what shap we are using
	*****************************************************************************************/
	VertexArrayObject();

	/*!***************************************************************************************
	\brief  Copy constructor (deleted)
	\param  other - the object to copy from
	*****************************************************************************************/
	VertexArrayObject(VertexArrayObject const & other) = delete;

	/*!***************************************************************************************
	\brief  Assigment operator ( delete)
	\param  other - the object to assign from
	\return A ref of the VAO
	*****************************************************************************************/
	VertexArrayObject & operator=(VertexArrayObject const & other) = delete;

	/*!***************************************************************************************
	\brief
	*****************************************************************************************/
	~VertexArrayObject();

	//void build(Mesh * mesh)

	//======================================================================================//
	//        Getters & Setters                                                             //
	//======================================================================================//

	/*!***************************************************************************************
	\brief  Gets the IBO
	\return ref to the IBO
	*****************************************************************************************/
	IndexBufferObject* getIndexBufferObject() { return m_ibo; }

	/*!***************************************************************************************
	\brief  Gets VBO
	\return ref to the VBO
	*****************************************************************************************/
	VertexBufferObject* getVertexBufferObject() { return m_vbo; }

	void InitializeIBO(Topology p_topology, std::size_t p_primitiveCount) { m_ibo = new IndexBufferObject(p_topology, p_primitiveCount); }

	void InitializeVBO(std::size_t p_vertexCount, std::size_t p_vertexSize) { m_vbo = new VertexBufferObject(p_vertexCount, p_vertexSize); }

	/////================================================================================/////
	/////     Functions                                                                  /////
	/////================================================================================/////

	/*!***************************************************************************************
	\brief  Builds VAO on GPU
	\param mesh - The mesh you want to build
	*****************************************************************************************/
	void build(std::weak_ptr<Mesh> p_mesh);

	/*!***************************************************************************************
	\brief  Binds CPU VAO to GPU VAO
	*****************************************************************************************/
	void bind() const;

	/*!***************************************************************************************
	\brief  Calls glDrawElements
	*****************************************************************************************/
	void render();

	/*!***************************************************************************************
	\brief  Unbinds from GPU VAO
	*****************************************************************************************/
	void unbind() const;

	///////============================================================================///////
	///////    Private                                                                 ///////
	///////============================================================================///////
private:

	/////================================================================================/////
	/////     Data                                                                       /////
	/////================================================================================/////
	unsigned m_handle;           //!< handle to GPU VAO

	IndexBufferObject * m_ibo = nullptr;     //!< IBO
	VertexBufferObject * m_vbo = nullptr;    //!< VBO
};
