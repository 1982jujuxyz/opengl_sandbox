/*!***************************************************************************************
\file       ShaderProgram.hpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      A CPU representation of the GPU program object that hold
			all shaders for pipline.
*****************************************************************************************/
#pragma once
//========1st Party Includes============================================================//
#include "rendering/Shader.h"
#include "ResourceManager.h"

namespace Math {
  class Color;
  class Matrix4;
	struct Vector4;
	struct Vector3;
}

//========3rd Party Includes============================================================//
//========Types=========================================================================//
//========Defines=======================================================================//
//========Forward Deceleration==========================================================//
class Color;
enum class  ShaderType;



////////==========================================================================////////
////////   Class                                                                  ////////
////////==========================================================================////////

/*!***************************************************************************************
\par class: ShaderProgram
\brief   A CPU representation of the GPU program object that hold all shaders for pipline.
*****************************************************************************************/
class ShaderProgram : public Resource
{

	///////============================================================================///////
	///////   Public                                                                   ///////
	///////============================================================================///////
public:

	/*!***************************************************************************************
	\brief  Constructor for shader program
	\param  p_assetPath    - The path to the assets folder
	\param  p_vertexName   - The name of the vertex shader file
	\param  p_fragmentName - The name of the fragment shader file.
	*****************************************************************************************/
	ShaderProgram();

	/////================================================================================/////
	/////     Functions                                                                  /////
	/////================================================================================/////

	/*!***************************************************************************************
	\brief  process the files(no loger in use)
	*****************************************************************************************/
	void process();

	/*!***************************************************************************************
	\brief  creates the ShaderProgram on the GPU
	*****************************************************************************************/
	void build();

	/*!***************************************************************************************
	\brief  binds the current shader program to the GPU ShaderProgram
	*****************************************************************************************/
	void bind() const;

	/*!***************************************************************************************
	\brief  unbinds the current shader program from the GPU ShaderProgram
	*****************************************************************************************/
	void unbind() const;

	/*!***************************************************************************************
 \brief  Sets Vec4 Uniforms on the shader
 \param  p_name - The name of the uniform
 \param  p_colr - The color to be loaded in
 *****************************************************************************************/
	void setUniforms(const std::string  & p_name, Math::Color p_colr);

	/*!***************************************************************************************
 \brief  Sets Vec4 Uniforms on the shader
 \param  p_name - The name of the uniform
 \param  p_vec3 - The vec3 to be loaded in
 *****************************************************************************************/
	void setUniforms(const std::string & p_name, Math::Vector3 p_vec3);

	/*!***************************************************************************************
	\brief  Sets Vec4 Uniforms on the shader
	\param  p_name - The name of the uniform
	\param  p_vec4 - The vec4 to be loaded in
	*****************************************************************************************/
	void setUniforms(const std::string & p_name, Math::Vector4 p_vec4);

	/*!***************************************************************************************
	\brief  Sets Vec4 Uniforms on the shader
	\param  p_name - The name of the uniform
	\param  p_mat4 - The mat4 to be loaded in
	*****************************************************************************************/
	void setUniforms(const std::string & p_name, Math::Matrix4 p_mat4);

	/*!***************************************************************************************
	\brief  Sets int Uniforms on the shader
	\param  p_name - The name of the uniform
	\param  p_value - The int to be loaded in
	*****************************************************************************************/
	void setUniforms(std::string const &p_name, int p_value);

	/*!***************************************************************************************
	\brief  Sets float Uniforms on the shader
	\param  p_name - The name of the uniform
	\param  p_value - The float to be loaded in
	*****************************************************************************************/
	void setUniforms(std::string const & p_name, float p_value);

	/*!***************************************************************************************
	\brief  Get a shader for the program
	\param  p_type - Type of shader
	\return The shader
	*****************************************************************************************/
	std::weak_ptr<Shader> getShader(ShaderType p_type);

	void AddVertexShader(std::weak_ptr<Shader>& p_vertexShader) { m_VertexShader = p_vertexShader; }
	void AddFragmentShader(std::weak_ptr<Shader>& p_fragmentShader) { m_fragmentShader = p_fragmentShader; }
	void AddGeometryShader(std::weak_ptr<Shader>& p_geometryShader) { m_geometryShader = p_geometryShader; }
	void AddTessellationControlShader(std::weak_ptr<Shader>& p_tessellationControlShader) { m_tessellationControlShader = p_tessellationControlShader; }
	void AddTessellationEvaluationShader(std::weak_ptr<Shader>& p_tessellationEvaluationShader) { m_tessellationEvaluationShader = p_tessellationEvaluationShader; }
	void AddComputeShader(std::weak_ptr<Shader>& p_computeShader) { m_computeShader = p_computeShader; }

	///////============================================================================///////
	///////     Private                                                                ///////
	///////============================================================================///////
private:

	/////================================================================================/////
	/////      Data                                                                      /////
	/////================================================================================/////
	GLint m_handle;           //!<  The handle to the GPU program
	std::weak_ptr<Shader> m_VertexShader;    //!<  The vertex shader
	std::weak_ptr<Shader> m_fragmentShader;  //!<  The fragment shader
	std::weak_ptr<Shader> m_geometryShader;
	std::weak_ptr<Shader> m_tessellationControlShader;
	std::weak_ptr<Shader> m_tessellationEvaluationShader;
	std::weak_ptr<Shader> m_computeShader;

   // std::unordered_map<std::string, unsigned> uniforms, attributes;

};
