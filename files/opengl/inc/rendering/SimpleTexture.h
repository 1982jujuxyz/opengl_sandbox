#pragma once

/*!***************************************************************************************
\file       Texture.cpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      Holds all information about a texture
*****************************************************************************************/
#pragma once

//========1st Party Includes============================================================//
#include "Utilities.h"

#include "Texture.h"
#include "rendering/TypeData.h"
#include "ResourceManager.h"

//========3rd Party Includes============================================================//
//========Types=========================================================================//
//========Defines=======================================================================//
//========Forward Deceleration==========================================================//



////////==========================================================================////////
////////  Class                                                                   ////////
////////==========================================================================////////
/*!***************************************************************************************
\par class: texture
\brief   Holds all information about a texture.
*****************************************************************************************/
class SimpleTexture : public Resource, public Texture
{
    ///////============================================================================///////
    ///////   Public                                                                   ///////
    ///////============================================================================///////
public:

    //======================================================================================//
    //        Constructor                                                                   //
    //======================================================================================//
    SimpleTexture();
    SimpleTexture(unsigned p_width, unsigned p_height, FormatType p_format);

    //SimpleTexture(const SimpleTexture& texture1) = delete;

    void build() override;
    void bind(unsigned int slot) override;
    void unbind() override;

    void load();

    ~SimpleTexture();

    int getBoundSlot() override { return m_boundSlot; }

    std::string  getTexturePath() const { return m_texturePath; }

    unsigned getWidth() const { return m_width; }
    unsigned getHeight() const { return m_height; }
    unsigned getColorChannels() const { return m_bpp; }

    unsigned char * getPixelData() const { return m_pixelData; };
    void setPixelData(unsigned char * data);

    unsigned getTextureHandle() const { return m_textureHandle;  }


    ///////============================================================================///////
    ///////   Private                                                                  ///////
    ///////============================================================================///////
public:

    void buildSingle();
    void buildCube();

    unsigned char * m_pixelData = NULL;
    unsigned m_width = 0;
    unsigned m_height = 0;
    unsigned m_bpp = 0;
    int m_boundSlot = -1;
	FormatType m_format = FormatType::enm_rgba;
    
    unsigned m_textureHandle = 0;
    std::string m_textureLabel;
    std::string m_texturePath;

};


///////============================================================================///////
///////   Public/Protected/Private                                                 ///////
///////============================================================================///////

//////==============================================================================//////
//////    (Non-)Static                                                              //////
//////==============================================================================//////

/////================================================================================/////
/////     Functions / Data                                                           /////
/////================================================================================/////

////==================================================================================////
////      (Non-)Configurable                                                          ////
////==================================================================================////

////==================================================================================////
////      Overridden/(Non-/Pure-)Virtual                                              ////
////==================================================================================////

//======================================================================================//
//        (Conversion )Constructor / Operators / Getters & Setters / Helper Destructor  //
//======================================================================================//

/*!***************************************************************************************
\brief  This just for giving an example of a function format.
\param exampleIn - dat old boring int
\return What is this returning?
*****************************************************************************************/

/*======================================================================================/
/         Other                                                                         /
//======================================================================================*/

// namespace <namespace name>
