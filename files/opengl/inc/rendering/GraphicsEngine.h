/*!***************************************************************************************
\file       GraphicsEngine.hpp
\author     Cody, Cannell
\date       6/19/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief  Holds all the managers and handles rendering for all objects
*****************************************************************************************/

#pragma once

//========1st Party Includes============================================================//

#include "CameraBase.h"
#include "Object.h"


namespace Component {
  struct Render;
  struct Material;
  class Camera;
}

class Transform;
struct Light;
class Camera;
//========Forward Deceleration==========================================================//
class MeshManager;
class LightManager;
class ShaderManager;
class TextureManager;
class VertexArrayManager;
class MaterialManager;
class TextureManager;
class FramebufferManager;

class CameraBase;
class Framebuffer;

class ShaderProgram;
class Scene;



////////==========================================================================////////
////////  Struct / Class                                                          ////////
////////==========================================================================////////

/*!***************************************************************************************
\par class: GraphicsEngine
\brief holds all the managers and  and handles rendering for all objects
*****************************************************************************************/
class GraphicsEngine
{
	///////=======================================================================////////
	///////   Public                                                              ////////
	///////=======================================================================////////
public:


	GraphicsEngine() {}

	////==========================================================================////
	////      Pure Virtual                                                        ////
	////==========================================================================////

	/*!*******************************************************************************
	\brief Engine system call to initalize graphics
	*********************************************************************************/
	void Initialize();

	/*!*******************************************************************************
	\brief Engine system call to update graphics
	*********************************************************************************/
	void Update(Scene * s);

	/*!*******************************************************************************
	\brief Engine system call to shutdown graphics
	*********************************************************************************/
	void Shutdown();

	///============================================================================///
	///       Helper                                                               ///
	///============================================================================///
	

	//==============================================================================//
	//         Getters & Setters                                                    //
	//==============================================================================//

	///*!*******************************************************************************
	//\brief  Gets the mesh manager
	//\return raw pointer to the mesh manager
	//*********************************************************************************/
	//MeshManager *  getMeshManager() { return m_meshManager; }

	///*!*******************************************************************************
	//\brief   Gets the shader manager
	//\return raw pointer to the shader manager
	//*********************************************************************************/
	//ShaderManager *  getShaderManager() { return m_shaderManager; }

	///*!***************************************************************************************
	//\brief  Gets the vertex array manager
	//\return raw pointer to the vertex array manager
	//*****************************************************************************************/
	//VertexArrayManager * getVertexArrayManager() { return m_vertexArrayManager; }

	///*!***************************************************************************************
	//\brief  Gets the Material manager
	//\return raw pointer to the manager system
	//*****************************************************************************************/
	//MaterialManager * getMaterialManager() { return m_MaterialManager; }

	///*!***************************************************************************************
	//\brief  Gets the texture manager
	//\return raw pointer to the texture system
	//*****************************************************************************************/
	//TextureManager * getTextureManager() { return m_textureManager; }

	/*!***************************************************************************************
	\brief  Gets the framebuffer manager
	\return raw pointer to the framebuffer system
	*****************************************************************************************/
	FramebufferManager * getFramebufferManager() { return m_framebufferManager; }

	/*!***************************************************************************************
  \brief Sets all the camera unforms like fog and near and far plain
  \param p_program - the shader program we send uniforms to
  \param p_cameraBase - the current active camera in the scene
  *****************************************************************************************/
	void SetCameraUniforms(std::weak_ptr<ShaderProgram> p_program, Object* p_cameraBase);
	void SetLightUniform(std::weak_ptr<ShaderProgram> p_program, std::vector<Object*>& p_lights);
	void setMaterialUniforms(std::weak_ptr<ShaderProgram> p_program, Component::Render* p_renderComponent);
	void setTextureUniforms(std::weak_ptr<ShaderProgram> p_program, Component::Render* p_renderComponent);
	void RenderSkybox(Scene* s);

	////////======================================================================////////
	////////   Private                                                            ////////
	////////======================================================================////////
private:

	/////============================================================================/////
	/////     Functions / Data                                                       /////
	/////============================================================================/////

	/*!***************************************************************************************
	\brief  This is basic rendering sending data through pipline to the screne
	*****************************************************************************************/

	//void renderToScreen(std::shared_ptr<ShaderProgram> p_ShaderProgram, Camera * p_camera);

	/*!***************************************************************************************
	\brief  The main render loop
	*****************************************************************************************/
	void RenderObjects(std::shared_ptr<ShaderProgram> p_ShaderProgram, Camera * p_camera);

	void ForwardRender();

	void DefferedRender();

	//VertexArrayManager *  m_vertexArrayManager = nullptr; //!< pointer to vertex manager
	//ShaderManager * m_shaderManager = nullptr;            //!< pointer to shader manager
	//MeshManager * m_meshManager = nullptr;                //!< pointer to mesh manager
	//MaterialManager * m_MaterialManager = nullptr;        //!< pointer to Material manager
	//TextureManager * m_textureManager = nullptr;          //!< pointer to texture manager
	FramebufferManager * m_framebufferManager = nullptr;  //!< pointer tp framebuffer manager



};



