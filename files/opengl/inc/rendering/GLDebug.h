#pragma once

struct GLDebug
{
    /*!***************************************************************************************
    \brief  Gets the last error of program
    \param  p_handle - Handle to the program object
    *****************************************************************************************/
    static void GetLastProgramError(GLint p_handle);

    /*!***************************************************************************************
    \brief  Gets the last error
    \param  p_handle - handle to the GPU shader object
    *****************************************************************************************/
    static void GetLastShaderError(GLint p_handle);

    static void GetLastProgramLinkError(GLint p_handle);

    static void getLastError();

    static void getLastFrameBufferError();
};
