///*!***************************************************************************************
//\file       ShaderManager.cpp
//\author     Cannell, Cody
//\date       6/19/18
//\copyright  All content © 2018-2019 DigiPen (USA) Corporation, all rights reserved.
//\par        Project: Boomerang
//\brief      Manages shaders and shader programes.
//*****************************************************************************************/
//#pragma once
//#include "TypeData.h"
////#include "ProgramPipeline.h"
////========Self Include==================================================================//
////========1st Party Includes============================================================//
////========3rd Party Includes============================================================//
////========Types=========================================================================//
////========Defines=======================================================================//
////========Forward Deceleration==========================================================//
//enum class PrimitiveType;
//enum class ShaderType;
//class Shader;
//class VertexShader;
//class FragmentShader;
//class ShaderProgram;
//
//
//////////==========================================================================////////
//////////  Class                                                                   ////////
//////////==========================================================================////////
//
//
//
///*!***************************************************************************************
//\par class: ShaderManager
//\brief      Manages shaders and shader programes
//*****************************************************************************************/
//class ShaderManager
//{
//	///////========================================================================///////
//	///////   Public                                                               ///////
//	///////========================================================================///////
//
//public:
//
//	//==================================================================================//
//	//        Constructor                                                               //
//	//==================================================================================//
//    ShaderManager() {}
//	/*!***************************************************************************************
//	\brief  clears map
//	*****************************************************************************************/
//    ~ShaderManager();
//    //ShaderManager(ShaderManager const& p_ShaderManager) = default;
//            /*static std::shared_ptr<ShaderProgram> LoadShaderGroup(ProgramType type,
//        std::vector<std::pair<std::string, std::string> >const& shaderFiles);*/
//
//    /*!***************************************************************************************
//    \brief  Creates a new shader programe
//    \param  p_assetPath    - The path to the assets folder
//    \param  p_type         - The type of object the program will build
//    \param  p_VertexName   - The name of the vertex shader file
//    \param  p_fragmentName - The name of the fragment shader file
//    \return shared pointer to the newly constructed shader program
//    *****************************************************************************************/
//    std::shared_ptr<ShaderProgram> createShaderProgram( ProgramType p_programType, std::weak_ptr<Shader> p_VertexName, std::weak_ptr<Shader> p_fragmentName);
//	//std::shared_ptr<programPipeline> createProgramPipelineObject(programType p_programType, std::string p_VertexName, std::string p_fragmentName);
//
//    /*!***************************************************************************************
//    \brief  Gets a shader program from map
//    \param  p_program -The shape program you want
//    \return shared pointer to the shader program
//    *****************************************************************************************/
//    std::shared_ptr<ShaderProgram> getShaderProgram(ProgramType p_program) const;
//	//std::shared_ptr<programPipeline> getProgramPipeline(programType p_programType) const;
//
//    /*!***************************************************************************************
//    \brief  Gets a shader from the program
//    \param  p_program - The program type
//    \param  p_type - The shader type
//    \return pointer to the shader
//    *****************************************************************************************/
//    std::weak_ptr<Shader> getShader(ProgramType p_program, ShaderType p_type) const;
//
/////////============================================================================///////
/////////    Private                                                                 ///////
/////////============================================================================///////
//
//private:
//
//	/////============================================================================/////
//	/////    Data                                                                    /////
//	/////============================================================================/////
//   std::unordered_map<ProgramType, std::shared_ptr<ShaderProgram> > m_ShaderPrograms; //!<  map of shader programs
//
//   //std::unordered_map<programType, std::shared_ptr<programPipeline> > m_programPipeline; //!<  map of shader programs
//
//  
//
//};
/////////============================================================================///////
/////////   Public/Protected/Private                                                 ///////
/////////============================================================================///////
//
////////==============================================================================//////
////////    (Non-)Static                                                              //////
////////==============================================================================//////
//
///////================================================================================/////
///////     Functions / Data                                                           /////
///////================================================================================/////
//
//////==================================================================================////
//////      (Non-)Configurable                                                          ////
//////==================================================================================////
//
//////==================================================================================////
//////      Overridden/(Non-/Pure-)Virtual                                              ////
//////==================================================================================////
//
////======================================================================================//
////        (Conversion )Constructor / Operators / Getters & Setters / Helper Destructor  //
////======================================================================================//
//
///*!***************************************************************************************
//\brief  This just for giving an example of a function format.
//\param exampleIn - dat old boring int
//\return What is this returning?
//*****************************************************************************************/
//
///*======================================================================================/
///         Other                                                                         /
////======================================================================================*/
//
//
//
//
