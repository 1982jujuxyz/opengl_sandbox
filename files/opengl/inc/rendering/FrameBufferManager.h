#pragma once
#include "Framebuffer.h"

#define MAXFRAMES 50

class Camera;

enum class FramebufferType
{
    Screen = 0,
    DeferredGBuffer,
    CaptureBuffer,
};

struct renderPool
{
    bool m_used;
    std::deque<std::shared_ptr<Framebuffer>> pool;
};


class FramebufferManager
{
public:
    FramebufferManager() {}
    
    std::shared_ptr<Framebuffer> createFramebuffer(FramebufferType type, unsigned int width, unsigned int height);
    std::shared_ptr<Framebuffer> getFramebuffer(FramebufferType type);

    unsigned createBufferPool();
    bool addFrameToBufferPool(unsigned p_index);

    int createBufferPool(const unsigned p_bufferSize);

    int attachRenderPool();

    void clearBuffer(unsigned p_index);

    std::deque<std::shared_ptr<Framebuffer>> & getBufferPool(const unsigned p_id);

    std::deque<std::shared_ptr<Framebuffer>> & getCapturePool(const unsigned p_id);

    void renderToScreen() { glBindFramebuffer(GL_FRAMEBUFFER, 0); } // bind screen Framebuffer

    void saveRenderToCapturePool(unsigned p_captureIndex, unsigned p_tapeIndex);

    unsigned createCapturePool(unsigned p_index);

    void resetBufferPool(unsigned p_index);
    

private:

    std::unordered_map<FramebufferType, std::shared_ptr<Framebuffer> > m_Framebuffers; //!< holds meshes by name


    std::vector<renderPool> m_renderPools;
    std::vector <std::deque<std::shared_ptr<Framebuffer>>> m_capturePools;


};


