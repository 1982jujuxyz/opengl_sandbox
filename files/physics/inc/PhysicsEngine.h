/*!**********************************************************************************************************
\file       PhysicsEngine.h
\author     Cody Cannell
\date       1/9/2020
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: OpenGLSandBox
\brief		The Engine that makes the world work
************************************************************************************************************/

class Scene;


class PhysicsEngine
{

public:
	void Initialize();

	void Update(Scene * s);

	void Shutdown();
	
};
