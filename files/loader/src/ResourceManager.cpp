#include "Precompiled.h"
#include "ResourceManager.h"



std::map<std::string, std::vector<std::string>> ResourceManager::m_assetNames;
fs::path ResourceManager::m_pathToAssetFolder;
std::map<std::string, std::string> ResourceManager::m_assetPaths;
std::map<std::string, std::shared_ptr<Resource>> ResourceManager::m_resourceMap;
std::map<std::type_index, std::function<void(std::string)>> ResourceManager::m_assetToLoaderMap;


void ResourceManager::Initialize(const fs::path& p_assetPath)
{
	//Create Loader Types

   // m_assetToLoaderMap.emplace(".mtl", new MaterialLoader());

    m_pathToAssetFolder = p_assetPath;

    CopyAssetPathsRecursively();
}

std::map<std::string, std::vector<std::string>> ResourceManager::GetAssetFileNames()
{
    return m_assetNames;
}


void ResourceManager::CopyAssetPathsRecursively()
{
    for (const auto& dirEntry : fs::recursive_directory_iterator(m_pathToAssetFolder))
    {
        const auto& filePath = dirEntry.path();
        if (!fs::is_directory(filePath))
        {
          const std::string & filename =   filePath.stem().string();    
          const std::string& filenameAndExtention = filePath.filename().string();
          const std::string & fileFolder = filePath.parent_path().stem().string();
          //const std::string & fileExtention = filePath.extension().string();
          
          m_assetNames[fileFolder].push_back(filename); // this is for populating dropdown lists
          m_assetPaths.emplace(std::make_pair( filenameAndExtention, filePath.string() ));

         // m_assets.emplace( filename , new Resource ( filename, m_assetToLoaderMap[fileExtention]));
        }
    }
}

//______________________________________________________________//

