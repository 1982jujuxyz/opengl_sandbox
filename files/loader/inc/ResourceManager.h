#pragma once

namespace fs = std::filesystem;



//class ResourceLoader
//{
//public:
//
//	virtual ~ResourceLoader() = default;
//	virtual void Load(Resource * p_resource) = 0;;
//	virtual void Unload(Resource* p_resource) = 0;
//};
//struct ResourceData
//{
//
//	std::string m_filename;
//	std::string m_name;
//
//	std::string* m_dataType;
//};



class Resource 
{

public:

	virtual ~Resource() = default;
	Resource() = default;

	Resource(const std::string& p_filename) :m_filename(p_filename) {}

	template <typename T>
	T* GetResourceData();

	void SetResourceName(std::string s) { m_filename = s; }
	void SetResourcePath(std::filesystem::path s) { m_resourcePath = s; }
  std::string GetResourceName() { return m_filename; }
	std::filesystem::path & GetResourcePath() { return m_resourcePath; }
	
private:

	std::filesystem::path m_resourcePath;
	std::string m_filename;

};



class ResourceHandle
{
public:
	ResourceHandle(std::string p_resourceName) : m_resourceName(std::move(p_resourceName)){}

private:
	
	std::string m_resourceName;
};


class ResourceManager 
{

public:
	
	static void Initialize(const fs::path& p_assetPath);
	static ResourceHandle * Load(fs::path p_path, std::string p_fileName); // gives back a
	static std::map<std::string, std::vector<std::string>> GetAssetFileNames();

	template <typename T>
	static std::weak_ptr<T> GetResource(std::string p_fileName);

	template <typename T>
	static void AddResource(std::string & p_name, Resource p_resource );

	template <typename T>
	static void RegisterFunction(std::function<void(std::filesystem::path &, std::weak_ptr<T>)> p_func);
	


private:

	static void CopyAssetPathsRecursively();

	// a list of resources
	static std::map<std::string, std::shared_ptr<Resource>> m_resourceMap;

	// list of avalible resources for pre-populating menus
	static std::map<std::string, std::vector<std::string>> m_assetNames;
	static std::map<std::string, std::string> m_assetPaths;

	static fs::path m_pathToAssetFolder;

	static std::map<std::type_index, std::function<void(std::string)>>  m_assetToLoaderMap;

};



template <typename T>
std::weak_ptr<T> ResourceManager::GetResource(std::string p_fileName)
{
	if(m_resourceMap.find(p_fileName) == m_resourceMap.end())
	{
		m_assetToLoaderMap[typeid(T)]( p_fileName);
	}
	     
	return std::reinterpret_pointer_cast<T>(m_resourceMap[p_fileName]);
}

template <typename T>
void ResourceManager::AddResource(std::string& p_name, Resource p_resource)
{
	m_resourceMap[p_name] = std::make_shared<T>(p_resource);
}




template <typename T>
 void ResourceManager::RegisterFunction(std::function<void(std::filesystem::path &, std::weak_ptr<T>)> p_func)
{
	
	 m_assetToLoaderMap[typeid(T)] = [p_func](std::filesystem::path s)
	 {
		 m_resourceMap[s.string()] = std::make_shared<T>();
		 m_resourceMap[s.string()]->SetResourceName(s.string());
		 m_resourceMap[s.string()]->SetResourcePath(std::filesystem::path(m_pathToAssetFolder.string() + s.string()));

		 std::weak_ptr<Resource> l_wp = m_resourceMap[s.string()];
		 p_func(m_resourceMap[s.string()]->GetResourcePath(), std::reinterpret_pointer_cast<T>(l_wp.lock()));
	 };
}

