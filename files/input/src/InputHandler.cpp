#include "Precompiled.h"
#include "InputHandler.h"



std::map<int, Command*> InputHandler::m_commands;
std::map<int, bool> InputHandler::m_keys;
std::function<void(float, float, float)> InputHandler::m_mouseCameraFunction;
MousePosition InputHandler::m_current = { 0, 0 };
MousePosition InputHandler::m_last = {0, 0};

bool InputHandler::m_mouseInWindow = false;
bool InputHandler::m_firstEnterInWindow = false;

bool InputHandler::m_mouseEnabled = false;

void Command::Execute()
{
	m_function();
}

void InputHandler::HandleInput()
{
	std::map<int, bool>::iterator it = m_keys.begin();

	while(it != m_keys.end())
	{
		if (it->second == true)
		{
			m_commands[it->first]->Execute();
		}

		++it;
	}
	
}

void InputHandler::Bind(int p_key, Command* p_command)
{
	m_commands[p_key] = p_command;
}

void InputHandler::RecieveInputFromGLFW(int key, int scancode, int action, int mods)
{

	// make sure key is registered
	auto l_command = m_commands.find(key);

	if(l_command != m_commands.end())
	{
		m_keys[key] = action != Released;
	}
	  
}

float pitch =0 , yaw = 0;
const float c_Pi = 3.1415926535897932384626433832795f;

float DegToRad(float degrees)
{
	return (c_Pi / 180.0f) * degrees;
}

void InputHandler::RecieveCursorPositionFromGLFW(double xpos, double ypos)
{
	if (!m_mouseEnabled)
	{
		m_last.x = xpos;
		m_last.y = ypos;
		return;
	}
		

	float xoffset = xpos - m_last.x;
	float yoffset = m_last.y - ypos;
	m_last.x = xpos;
	m_last.y = ypos;

	float sensitivity = 0.05;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	yaw += xoffset;
	pitch += yoffset;

	if (pitch > 89.0f)
		pitch = 89.0f;
	if (pitch < -89.0f)
		pitch = -89.0f;

	// these values are already normal
	float x = cos(DegToRad(yaw)) * cos(DegToRad(pitch));
	float y = sin(DegToRad(pitch));
	float z = sin(DegToRad(yaw)) * cos(DegToRad(pitch));

	//float norm = sqrt(x * x + y * y + z * z);

	  m_mouseCameraFunction(x, y, z);

  }

void InputHandler::RecieveMouseButtonFromGLFW(int button, int action, int mods)
{
	if (button == 0)
	{
		if (action)
		{
			m_mouseEnabled = true;
		}
		else
		{
			m_mouseEnabled = false;
		}
	}
}

void InputHandler::SetMouseCameraFunciton(std::function<void(float, float,float )> p_function)
{
	m_mouseCameraFunction = p_function;
}

void InputHandler::RecieveCursorEnterFromGLFW(int entered)
{
	if (!entered)
	{
		// The cursor entered the content area of the window
		m_mouseInWindow = true;
	}
	else
	{
		// The cursor left the content area of the window
		m_mouseInWindow = false;
		m_firstEnterInWindow = false;
	}
}





