//lowercase:
//doxygen:
//tablen:
#pragma once

#include <string>
#include <filesystem>
#include <map>
#include <utility>
#include <functional>
#include <typeindex>

#define UNREFERENCED_PARAMETER(X) (void)X



//#if COMPILING_DLL
//#define DLLEXPORT __declspec(dllexport)
//#else
//#define DLLEXPORT __declspec(dllimport)
//#endif
