#pragma once

struct MousePosition
{
  float x, y;
};

enum Key : int
{
  A = 'A', B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
  ESC = 256, /* F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,*/
  Grave = '`', Digit_1 = '1', Digit_2, Digit_3, Digit_4, Digit_5, Digit_6, Digit_7, Digit_8, Digit_9, Digit_0 = '0',
  Hyphen = '-', Equal = '=', /*Backspace, */
  Tab = '\t', Open_Bracket = '[', Close_Bracket = ']', Backslash = '\\',
  Semicolon = ';', Apostrophe = '\'', Enter = 257,
  Left_Shift = 340, Comma = ',', Period = '.', Slash = '/', /*Shift_Right, */
  Left_Control = 341, /*Alt_Left,*/ Space = ' ', /*Alt_Right, Control_Right,
  Insert, Home, Page_Up, Delete, End, Page_Down, */
  Up = 265, Left = 263, Down = 264, Right = 262,
  /*Num_Slash, Num_Asterisk, Num_Minus, Num_Plus, Num_Enter,
  Num_0, Num_1, Num_2, Num_3, Num_4, Num_5, Num_6, Num_7, Num_8, Num_9, Num_Period,*/

};

enum Action : int
{
  Released,
  Pressed,
  Repeat,

};

class Command
{
public:
   ~Command() {}

   Command(std::function<void()> p_function) : m_function(p_function) {}

   void Execute();

   std::function<void()> m_function;
};

class InputHandler
{
public:
  static void HandleInput();

  // Methods to bind commands...
  static void Bind(int p_key, Command* p_command);
  //static bool GenerateInputCommands(std::vector<Command *>& command_queue);
  static void RecieveInputFromGLFW(int key, int scancode, int action, int mods);
  static void RecieveCursorPositionFromGLFW(double xpos, double ypos);
  static void RecieveMouseButtonFromGLFW(int button, int action, int mods);
  static void RecieveCursorEnterFromGLFW(int entered);

  static void SetMouseCameraFunciton(std::function<void(float, float, float )> p_function);
  

private:


  static std::map<int, Command*> m_commands;
  static std::map<int, bool> m_keys;

  static bool m_mouseEnabled;

  static MousePosition m_current;
  static MousePosition m_last;

  

  static bool m_mouseInWindow;
  static bool m_firstEnterInWindow;

  static std::function<void(float, float, float)> m_mouseCameraFunction;

  //static std::vector<InputHandler*> _instances; // add multiple input handlers for different menu parts

};





