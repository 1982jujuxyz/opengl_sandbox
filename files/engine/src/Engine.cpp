#include "Precompiled.h"
#include "Engine.h"
#include "SystemBase.h"
#include "rendering/GraphicsEngine.h"
#include "PhysicsEngine.h"
#include "Scene.h"
#include "DeltaTime.h"
#include "ResourceManager.h"
#include "InputHandler.h"

#include <iostream>


void Engine::Initialize(const std::filesystem::path & p_assetPath)
{
	// set up Graphics System
	m_scene = new Scene();
	m_graphics = new GraphicsEngine();
	m_physics = new PhysicsEngine();

	// Initialize the Static Resource Manager
	ResourceManager::Initialize(p_assetPath);

	
	m_graphics->Initialize();
	
	
}

void Engine::Update()
{
	Time::FrameStart();

	//=== Update Physics ===//
	// update only on a fixed time step
	if(Time::FixedTime::Accumulate() > Time::FixedTime::FixedDuration())
	{
		m_physics->Update(m_scene);
	}
	//=== Update Graphics ===//
	m_graphics->Update(m_scene);

	

	auto dt = Time::DeltaTime();


	//=== Handle Messages ===//
	InputHandler::HandleInput();
	std::cout << dt << std::endl;
	Time::FrameEnd();
}

void Engine::Shutdown()
{
	m_graphics->Shutdown();
	delete m_scene;
	delete m_graphics;
	delete m_physics;
}
