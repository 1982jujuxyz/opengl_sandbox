#pragma once

// forward decelerations 
class GraphicsEngine;
class PhysicsEngine;
class Scene;

class Engine {

public:

	Engine() = default;

	void Initialize(const std::filesystem::path& p_assetPath);
	void Update();
	void Shutdown();

private:

	GraphicsEngine * m_graphics;
	PhysicsEngine* m_physics;
	Scene* m_scene;
};