//lowercase:
//doxygen:
//tablen:
#pragma once

#include <vector>
#include <string>
#include <memory>
#include <chrono>
#include <filesystem>
#include <map>
#include <typeindex>

#include <functional>
#include <algorithm>

#define UNREFERENCED_PARAMETER(X) (void)X



//#if COMPILING_DLL
//#define DLLEXPORT __declspec(dllexport)
//#else
//#define DLLEXPORT __declspec(dllimport)
//#endif
