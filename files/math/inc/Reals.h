#pragma once

namespace Math
{
	const float c_Pi = 3.1415926535897932384626433832795f;
	const float c_TwoPi = 2.0f * c_Pi;
	const float epsilon = 0.000001f;
}