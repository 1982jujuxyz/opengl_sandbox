#pragma once

/*!***************************************************************************************
\file       Quaternion.h
\author     Cannell, Cody
\date       8/21/18
\copyright  All content 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      This is the definition of the Quaternion class. ( column-major order )
*****************************************************************************************/

namespace Math
{
	struct Vector3;
	class Matrix4;
	struct Vector4;

	


	struct Quaternion
	{
		Quaternion();
		Quaternion(float x, float y, float z, float w);
		Quaternion(Vector3 axis, float angle);
		Matrix4 toMatrix4() const;

		void normalize();

		//======================================================================================//
		//       Operators                                                                      //
		//======================================================================================//

		float& operator[](unsigned index);
		float operator[](unsigned index) const;

		void operator+=(const Quaternion & rhs);
		Quaternion operator+(const Quaternion & rhs) const;
		Quaternion operator-(const Quaternion & rhs) const;

		Quaternion operator*(Quaternion quat) const;
		Quaternion operator*(const float rhs) const;
		Quaternion operator*(const Vector3 rhs) const;

		void operator*=(const	Quaternion rhs);
		void operator*=(const float rhs);
		void operator*=(const Vector3 rhs);
		bool operator==(const Quaternion& p_other) const;



		//==============================================================================//
		//        Getters & Setters                                                     //
		//==============================================================================//

		/*Vector3 getForward() const { return Vector3(0,0,1).rotate(*this); }
		Vector3 getUp() const { return Vector3(0,1,0).rotate(*this); }
		Vector3 getRight() const { return Vector3(1,0,0).rotate(*this); }*/



		Vector3 getForward() const;
		Vector3 getUp() const;
		Vector3 getRight() const;



		/*!***************************************************************************************
		   \brief  gets x component
		 *****************************************************************************************/
		float getX() const;

		/*!***************************************************************************************
		\brief  gets y component
		*****************************************************************************************/
		float getY() const;

		/*!***************************************************************************************
		\brief  gets z component
		*****************************************************************************************/
		float getZ() const;

		/*!***************************************************************************************
		\brief  gets w component
		*****************************************************************************************/
		float getW() const;

		/*!***************************************************************************************
		\brief  sets x component
		\param p_new_x - The new x element to set
		*****************************************************************************************/
		void setX(float p_new_x);

		/*!***************************************************************************************
		\brief  sets y component
		\param p_new_y - The new y element to set
		*****************************************************************************************/
		void setY(float p_new_y);

		/*!***************************************************************************************
		\brief  sets z component
		\param p_new_z - The new z element to set
		*****************************************************************************************/
		void setZ(float p_new_z);

		/*!***************************************************************************************
		\brief  sets w component
		\param p_new_w - The new z element to set
		*****************************************************************************************/
		void setW(float p_new_w);

		/*!***************************************************************************************
		\brief  sets offset for x component
		\param p_offset - Of set to change x by
		*****************************************************************************************/
		void offsetX( float p_offset);

		/*!***************************************************************************************
		\brief  sets offset for y component
		\param p_offset - Of set to change y by
		*****************************************************************************************/
		void offsetY( float p_offset);

		/*!***************************************************************************************
		\brief  sets offset for z component
		\param p_offset - Of set to change z by
		*****************************************************************************************/
		void offsetZ(float p_offset);


		//======================================================================================//
		//      Functions                                                                       //
		//======================================================================================//


		float* toFloatPtr();
		std::string toStringPtr() const;

		Vector4& toVector4();
		const Vector4& toVector4() const;

		void Conjugate();
		Quaternion Conjugated() const;

		//////============================================================================//////
		//////     Data                                                                   //////
		//////============================================================================//////

		float x, y, z, w;
	};

	Quaternion Lerp(const Quaternion & start, const Quaternion & end, float tValue);
	Quaternion Slerp(const Quaternion & start, const Quaternion & end, float tValue);
}



