#pragma once

namespace Math
{
	struct Quaternion;
	struct Vector3;

	float RadToDeg(float radians);
	float DegToRad(float degrees);
	Vector3 toEulerAngle(const Quaternion & q);
	Quaternion toQuaternion(Vector3 p_eular);

}


