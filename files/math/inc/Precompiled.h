//lowercase:
//doxygen:
//tablen:
#pragma once


#include <algorithm>
#include <array>
#include <atomic>
#include <bitset>
#include <assert.h>
#include <chrono>
#include <cstdarg>
#include <cstdlib>
#include <deque>
#include <exception>
#include <forward_list>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <numeric>
#include <queue>
#include <random>
#include <regex>
#include <set>
#include <sstream>
#include <stack>
#include <streambuf>
#include <string>
#include <thread>
#include <tuple>
#include <typeindex>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>





#define UNREFERENCED_PARAMETER(X) (void)X



//#if COMPILING_DLL
//#define DLLEXPORT __declspec(dllexport)
//#else
//#define DLLEXPORT __declspec(dllimport)
//#endif
