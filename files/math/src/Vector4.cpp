/*!***************************************************************************************
\file       Vector4.cpp
\author     Cody, Cannell
\date       5/22/18
\copyright  All content © 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: TBD
\brief      This is the definition of a simple Vector class of four values
*****************************************************************************************/
#include "Precompiled.h"
#include "Vector4.h"
#include "Vector3.h"


namespace Math
{
	Vector4::Vector4()
		: x(0), y(0), z(0), w(0)
	{	}

	Vector4::Vector4(float p_x, float p_y, float p_z, float p_w) : x(p_x), y(p_y), z(p_z), w(p_w)
	{}

	Vector4::Vector4(Vector3 p_vec3, float p_w) : x(p_vec3.x), y(p_vec3.y), z(p_vec3.z), w(p_w)
	{}

	Vector4::Vector4(const Vector4 & p_other) : x(p_other.x), y(p_other.y), z(p_other.z), w(p_other.w)
	{}

	Vector4::Vector4(const float * p_other)
	{
		x = p_other[0];
		y = p_other[1];
		z = p_other[2];
		w = p_other[3];
	}


	Vector4& Vector4::operator=(const Vector4 & rhs)
	{
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;
		w = rhs.w;

		return *this;
	}

	Vector4 Vector4::operator-() const
	{
		return Vector4(-x, -y, -z, -w);
	}


	void Vector4::operator+=(const Vector4 & p_rhs)
	{
		x += p_rhs.x;
		y += p_rhs.y;
		z += p_rhs.z;
		w += p_rhs.w;
	}

	void Vector4::operator-=(const Vector4 & p_rhs)
	{
		x -= p_rhs.x;
		y -= p_rhs.y;
		z -= p_rhs.z;
		w -= p_rhs.w;
	}

	void Vector4::operator*=(const Vector4 & p_rhs)
	{
		x *= p_rhs.x;
		y *= p_rhs.y;
		z *= p_rhs.z;
		w *= p_rhs.w;
	}

	void Vector4::operator/=(const Vector4 & p_rhs)
	{
		x /= p_rhs.x;
		y /= p_rhs.y;
		z /= p_rhs.z;
		w /= p_rhs.w;
	}

	void Vector4::operator+=(const float & p_rhs)
	{
		x += p_rhs;
		y += p_rhs;
		z += p_rhs;
		w += p_rhs;
	}

	void Vector4::operator-=(const float & p_rhs)
	{
		x -= p_rhs;
		y -= p_rhs;
		z -= p_rhs;
		w -= p_rhs;
	}

	void Vector4::operator*=(const float & p_rhs)
	{
		x *= p_rhs;
		y *= p_rhs;
		z *= p_rhs;
		w *= p_rhs;
	}

	void Vector4::operator/=(const float & p_rhs)
	{
		x /= p_rhs;
		y /= p_rhs;
		z /= p_rhs;
		w /= p_rhs;
	}

	Vector4 Vector4::operator+(const Vector4 & p_rhs) const
	{
		Vector4 l_temp = *this;
		l_temp += p_rhs;
		return l_temp;
	}

	Vector4 Vector4::operator-(const Vector4 & p_rhs) const
	{
		Vector4 l_temp = *this;
		l_temp -= p_rhs;
		return l_temp;
	}

	Vector4 Vector4::operator*(const Vector4 & p_rhs) const
	{
		Vector4 l_temp = *this;
		l_temp *= p_rhs;
		return l_temp;
	}

	Vector4 Vector4::operator/(const Vector4 & p_rhs) const
	{
		Vector4 temp = *this;
		temp /= p_rhs;
		return temp;
	}

	Vector4 Vector4::operator+(const float & p_rhs) const
	{
		Vector4 l_temp = *this;
		l_temp += p_rhs;
		return l_temp;
	}

	Vector4 Vector4::operator-(const float & p_rhs) const
	{
		Vector4 temp = *this;
		temp -= p_rhs;
		return temp;
	}

	Vector4 Vector4::operator*(const float & p_rhs) const
	{
		Vector4 l_temp = *this;
		l_temp *= p_rhs;
		return l_temp;
	}

	Vector4 Vector4::operator/(const float & p_rhs) const
	{
		Vector4 l_temp = *this;
		l_temp /= p_rhs;
		return l_temp;
	}

	float& Vector4::operator[](const unsigned & p_index)
	{
		return toFloatPtr()[p_index];
	}

	float Vector4::operator[](const unsigned & p_index) const
	{
		return toFloatPtr()[p_index];
	}

	bool Vector4::operator==(const Vector4& p_other) const
	{
		return x == p_other.x && y == p_other.y && z == p_other.z && w == p_other.w;
	}

	bool Vector4::operator!=(const Vector4& p_other) const
	{
		return x != p_other.x || y != p_other.y || z != p_other.z || w != p_other.w;
	}

	float Vector4::dotP(const Vector4 p_rhs) const
	{
		return x * p_rhs.x + y * p_rhs.y + z * p_rhs.z + w * p_rhs.w;
	}

	Vector4 Vector4::crossP(const Vector4 p_rhs) const
	{
		return Vector4(y * p_rhs.z - z * p_rhs.y, z * p_rhs.x - x * p_rhs.z, x * p_rhs.y - y * p_rhs.x, 1);
	}

	void Vector4::normalize()
	{
		float length = std::sqrt(x * x + y * y + z * z + w * w);

		*this /= length;
	}

	Vector4 Vector4::normalized()
	{
		float length = std::sqrt(x * x + y * y + z * z);

		return *this / length;

	}

	const float* Vector4::toFloatPtr() const
	{
		return reinterpret_cast<const float*>(this);
	}

	float * Vector4::toFloatPtr()
	{
		return reinterpret_cast<float*>(this);
	}

	std::string Vector4::toStringPtr() const
	{
		std::stringstream l_ss;
		l_ss << "(" << x << ", " << y << ", " << z << ", " << w << ")";
		return l_ss.str();
	}

	Vector3 Vector4::toVector3() { return Vector3(x, y, z); }
}

