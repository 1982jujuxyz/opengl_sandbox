﻿/*!***************************************************************************************
\file       Matrix4.cpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content © 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      This is the definition of the Matrix4 class. ( column-major order )
*****************************************************************************************/

#include "Precompiled.h"
#include "Matrix4.h"
#include "Quaternion.h"
#include "Vector4.h"
#include "Vector3.h"

namespace Math
{
	Matrix4::Matrix4()
	{
		m00 = 1;
		m01 = 0;
		m02 = 0;
		m03 = 0;
		m10 = 0;
		m11 = 1;
		m12 = 0;
		m13 = 0;
		m20 = 0;
		m21 = 0;
		m22 = 1;
		m23 = 0;
		m30 = 0;
		m31 = 0;
		m32 = 0;
		m33 = 1;
	}



	Matrix4::Matrix4(const float * p_other)
	{
		m00 = p_other[0];
		m01 = p_other[1];
		m02 = p_other[2];
		m03 = p_other[3];
		m10 = p_other[4];
		m11 = p_other[5];
		m12 = p_other[6];
		m13 = p_other[7];
		m20 = p_other[8];
		m21 = p_other[9];
		m22 = p_other[10];
		m23 = p_other[11];
		m30 = p_other[12];
		m31 = p_other[13];
		m32 = p_other[14];
		m33 = p_other[15];
	}



	Matrix4::Matrix4(const Vector3 forward, const Vector3 up, const Vector3 right)
	{

		m_mat4[0][0] = right.x;	m_mat4[0][1] = up.x; m_mat4[0][2] = forward.x; m_mat4[0][3] = 0;
		m_mat4[1][0] = right.y;	m_mat4[1][1] = up.y; m_mat4[1][2] = forward.y; m_mat4[1][3] = 0;
		m_mat4[2][0] = right.z;	m_mat4[2][1] = up.z; m_mat4[2][2] = forward.z; m_mat4[2][3] = 0;
		m_mat4[3][0] = 0;		    m_mat4[3][1] = 0;		 m_mat4[3][2] = 0;		     m_mat4[3][3] = 1;
	}

	Matrix4::Matrix4(const Matrix4& p_other)
	{
		m00 = p_other[0][0];
		m01 = p_other[0][1];
		m02 = p_other[0][2];
		m03 = p_other[0][3];
		m10 = p_other[1][0];
		m11 = p_other[1][1];
		m12 = p_other[1][2];
		m13 = p_other[1][3];
		m20 = p_other[2][0];
		m21 = p_other[2][1];
		m22 = p_other[2][2];
		m23 = p_other[2][3];
		m30 = p_other[3][0];
		m31 = p_other[3][1];
		m32 = p_other[3][2];
		m33 = p_other[3][3];
	}

	Vector3 Matrix4::right() const
	{
		return Vector3(m_mat4[0][0], m_mat4[1][0], m_mat4[2][0]);
	}

	Vector3 Matrix4::up() const
	{
		return Vector3(m_mat4[0][1], m_mat4[1][1], m_mat4[2][1]);
	}

	Vector3 Matrix4::forward() const 
	{
		return Vector3(m_mat4[0][2], m_mat4[1][2], m_mat4[2][2]);
	}

	Vector3 Matrix4::position() const
	{
		return Vector3(m_mat4[0][3], m_mat4[1][3], m_mat4[2][3]);
	}

	//void Matrix4::operator+=(const Matrix4& p_rhs)
	//{
	//    data += p_rhs.data;
	//}

	//void Matrix4::operator-=(const Matrix4& p_rhs)
	//{
	//    data -= p_rhs.data;
	//}

	const Vector4 Matrix4::operator[](unsigned p_rowIndex) const
	{
		return Vector4(m_mat4[p_rowIndex]);
	}

	Vector4 Matrix4::operator[](unsigned p_rowIndex)
	{
		return Vector4(m_mat4[p_rowIndex]);
	}

	Matrix4 Matrix4::operator*(const Matrix4& p_rhs) const
	{
		Matrix4 l_temp = *this;
		l_temp *= p_rhs;

		return l_temp;
	}

	Matrix4 Matrix4::concat(const Matrix4& p_rhs) const
	{
		Matrix4 ret;

		//  r_0 | i_0 i_1 i_2 i_3 |    i_0 | c_0 c_1 c_2 c_3 |  
		//  r_1 | i_0 i_1 i_2 i_3 | *  i_1 | c_0 c_1 c_2 c_3 | 
		//  r_2 | i_0 i_1 i_2 i_3 |    i_2 | c_0 c_1 c_2 c_3 | 
		//  r_3 | i_0 i_1 i_2 i_3 |    i_3 | c_0 c_1 c_2 c_3 | 

		for (int r = 0; r < 4; ++r)
		{
			for (int c = 0; c < 4; ++c)
			{
				float accumulator = 0;

				for (int i = 0; i < 4; ++i)
				{
					accumulator += m_mat4[r][i] * p_rhs.m_mat4[i][c];
				}

				ret.m_mat4[r][c] = accumulator;
			}
		}

		return ret;
	}

	//void Matrix4::operator/=(const Matrix4& rhs)
	//{
	//    data /= rhs.data;
	//}

	//void Matrix4::operator+=(const float& p_rhs)
	//{
	//    data += p_rhs;
	//}

	//void Matrix4::operator-=(const float& p_rhs)
	//{
	//    data -= p_rhs;
	//}

	void Matrix4::operator*=(const Matrix4& p_rhs)
	{
		Matrix4 & l_this = *this;
		l_this = concat(p_rhs);
	}

	void Matrix4::operator*=(const float& p_rhs)
	{
		Matrix4 & l_this = *this;
		l_this[0] *= p_rhs;
		l_this[1] *= p_rhs;
		l_this[2] *= p_rhs;
		l_this[3] *= p_rhs;
	}

	//void Matrix4::operator/=(const float& p_rhs)
	//{
	//    data /= p_rhs;
	//}

	  //Matrix4 Matrix4::operator+(const Matrix4& p_rhs) const
	  //{
	  //    Matrix4 temp = *this;
	  //    temp += p_rhs;
	  //    return temp;
	  //}

	  //Matrix4 Matrix4::operator-(const Matrix4& p_rhs) const
	  //{
	  //    Matrix4 temp = *this;
	  //    temp -= p_rhs;
	  //    return temp;
	  //}

	Vector4 Matrix4::operator*(const Vector4& p_rhs) const
	{
		return Vector4(m_mat4[0][0] * p_rhs[0] + m_mat4[0][1] * p_rhs[1] + m_mat4[0][2] * p_rhs[2] + m_mat4[0][3] * p_rhs[3],
			m_mat4[1][0] * p_rhs[0] + m_mat4[1][1] * p_rhs[1] + m_mat4[1][2] * p_rhs[2] + m_mat4[1][3] * p_rhs[3],
			m_mat4[2][0] * p_rhs[0] + m_mat4[2][1] * p_rhs[1] + m_mat4[2][2] * p_rhs[2] + m_mat4[2][3] * p_rhs[3],
			m_mat4[3][0] * p_rhs[0] + m_mat4[3][1] * p_rhs[1] + m_mat4[3][2] * p_rhs[2] + m_mat4[3][3] * p_rhs[3]);
	}

	//Matrix4 Matrix4::operator/(const Matrix4& p_rhs) const
	//{
	//    Matrix4 temp = *this;
	//    temp /= p_rhs;
	//    return temp;
	//}

	//Matrix4 Matrix4::operator+(const float& p_rhs) const
	//{
	//    Matrix4 temp = *this;
	//    temp += p_rhs;
	//    return temp;
	//}

	//Matrix4 Matrix4::operator-(const float& p_rhs) const
	//{
	//    Matrix4 temp = *this;
	//    temp -= p_rhs;
	//    return temp;
	//}

	Matrix4 Matrix4::operator*(const float& p_rhs) const
	{
		Matrix4 l_result = *this;

		l_result *= p_rhs;

		return l_result;
	}

	//Matrix4 Matrix4::operator/(const float& p_rhs) const
	//{
	//    Matrix4 temp = *this;
	//    temp /= p_rhs;
	//    return temp;
	//}

	bool Matrix4::operator==(const Matrix4& p_rhs) const
	{
		return m_mat4[0] == p_rhs.m_mat4[0]
			&& m_mat4[1] == p_rhs.m_mat4[1]
			&& m_mat4[2] == p_rhs.m_mat4[2]
			&& m_mat4[3] == p_rhs.m_mat4[3];
	}

	bool Matrix4::operator!=(const Matrix4& p_rhs) const
	{
		return !(*this == p_rhs);
	}

	static const float ZPI = 3.14159265358979323846f;
	static const float RAD2DEG = (180.f / ZPI);
	static const float DEG2RAD = (ZPI / 180.f);

	void Matrix4::decompose(Vector3 & translate, Quaternion & rotate, Vector3 & scale) const
	{
		/* Vector3 shear;
		decompose(translate, rotate, scale, shear);*/

		Matrix4 l_mat = *this;

		scale[0] = l_mat.right().distance();
		scale[1] = l_mat.up().distance();
		scale[2] = l_mat.forward().distance();

		l_mat.orthonormalize();



		translate[0] = l_mat.position().x;
		translate[1] = l_mat.position().y;
		translate[2] = l_mat.position().z;



		//Vector3 v = Vector3(mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1],
		//    mat[1][2] * mat[2][0] - mat[1][0] * mat[2][2],
		//    mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0]);

		//float dot = v.x * mat[0][0] + v.y * mat[0][1] + v.z *  mat[0][2];

		//if (dot < 0.0f)
		//{
		//    mat *= -1.0f;
		//    scale *= -1.0f;
		//}

		rotate = l_mat.toQuaternion().Conjugated();
	}

	void Matrix4::decompose(Vector3 & p_position, Quaternion & p_rotation, Vector3 & p_scale, Vector3 & p_shear) const
	{
		//    L(X)  U(Y)  F(Z)   T      <=  direction(axis)
		//  | V1.x  V2.x  V3.x  V4.x |
		//  | V1.y  V2.y  V3.y  V4.y |
		//  | V1.z  V2.z  V3.z  V4.z |
		//  | V1.w  V2.w  V3.w  V4.w |


		// Sx = ||<V1.x,V1.y,V1.z>||
		// Sy = ||<V2.x,V2.y,V2.z>||
		// Sz = ||<V3.x,V3.y,V3.z>||

				// translation
		p_position = Vector3(m_mat4[0][3], m_mat4[1][3], m_mat4[2][3]);



		//  m00, m01, m02, m03,
		//  m10, m11, m12, m13,
		//  m20, m21, m22, m23,
		//  m30, m31, m32, m33;

				 // rotation
		Matrix4 l_rotation;

		//        X'                                    Y'                              Z'
		l_rotation.m_mat4[0][0] = m_mat4[0][0];  l_rotation.m_mat4[0][1] = m_mat4[0][1];  l_rotation.m_mat4[0][2] = m_mat4[0][2];
		l_rotation.m_mat4[1][0] = m_mat4[1][0];  l_rotation.m_mat4[1][1] = m_mat4[1][1];  l_rotation.m_mat4[1][2] = m_mat4[1][2];
		l_rotation.m_mat4[2][0] = m_mat4[2][0];  l_rotation.m_mat4[2][1] = m_mat4[2][1];  l_rotation.m_mat4[2][2] = m_mat4[2][2];


		// get scale X from the magnitude of X
		p_scale.x = Vector3(m_mat4[0][0], m_mat4[1][0], m_mat4[2][0]).distance();

		//normalized X
		l_rotation.m_mat4[0][0] /= p_scale.x;
		l_rotation.m_mat4[1][0] /= p_scale.x;
		l_rotation.m_mat4[2][0] /= p_scale.x;

		//ShearXY is the dot product of X' and Y'
		p_shear.z = l_rotation[0][0] * l_rotation[0][1] +
			l_rotation[1][0] * l_rotation[1][1] +
			l_rotation[2][0] * l_rotation[2][1];


		//Make Y' orthogonal to X' by " Y' = Y' - (ShearXY * X') "
		l_rotation.m_mat4[0][1] -= p_shear.z * l_rotation[0][0];
		l_rotation.m_mat4[1][1] -= p_shear.z * l_rotation[0][1];
		l_rotation.m_mat4[2][1] -= p_shear.z * l_rotation[2][0];



		p_scale.y = Vector3(m_mat4[0][1], m_mat4[1][1], m_mat4[2][1]).distance();

		//normalized X
		l_rotation.m_mat4[0][1] /= p_scale.y;
		l_rotation.m_mat4[1][1] /= p_scale.y;
		l_rotation.m_mat4[2][1] /= p_scale.y;

		//ShearXY is divided by ScaleY to get it's final value
		p_shear.z /= p_scale.y;


		//ShearXZ is the dot product of X' and Z'
		p_shear.y = l_rotation[0][0] * l_rotation[0][2] +
			l_rotation[1][0] * l_rotation[1][2] +
			l_rotation[2][0] * l_rotation[2][2];

		//ShearYZ is the dot product of Y' and Z'
		p_shear.x = l_rotation[0][1] * l_rotation[0][2] +
			l_rotation[1][1] * l_rotation[1][2] +
			l_rotation[2][1] * l_rotation[2][2];

		//Make Z' orthogonal to X' by " Z' = Z' - (ShearXZ * X') "
		l_rotation.m_mat4[0][2] -= p_shear.y * l_rotation[0][0];
		l_rotation.m_mat4[1][2] -= p_shear.y * l_rotation[1][0];
		l_rotation.m_mat4[2][2] -= p_shear.y * l_rotation[2][0];

		//Make Z' orthogonal to Y' by " Z' = Z' - (ShearYZ * Y') "
		l_rotation.m_mat4[0][2] -= p_shear.x * l_rotation[0][1];
		l_rotation.m_mat4[1][2] -= p_shear.x * l_rotation[1][1];
		l_rotation.m_mat4[2][2] -= p_shear.x * l_rotation[2][1];


		p_scale.z = Vector3(m_mat4[0][2], m_mat4[1][2], m_mat4[2][2]).distance();

		//Z' is normalized
		l_rotation.m_mat4[0][2] /= p_scale.z;
		l_rotation.m_mat4[1][2] /= p_scale.z;
		l_rotation.m_mat4[2][2] /= p_scale.z;

		//ShearXZ is divided by ScaleZ to get it's final value
		p_shear.y /= p_scale.z;

		//ShearYZ is divided by ScaleZ to get it's final value
		p_shear.x /= p_scale.z;

		//If the determinant is negative, then the rotation and scale contain a flip
		Vector3 v = Vector3(
			l_rotation[1][1] * l_rotation[2][2] - l_rotation[2][1] * l_rotation[1][2],
			l_rotation[2][1] * l_rotation[0][2] - l_rotation[0][1] * l_rotation[2][2],
			l_rotation[0][1] * l_rotation[1][2] - l_rotation[1][1] * l_rotation[0][2]);

		float dot = v.x * l_rotation[0][0] + v.y * l_rotation[1][0] + v.z *  l_rotation[2][0];

		if (dot < 0.0f)
		{
			l_rotation *= -1.0f;
			p_scale *= -1.0f;
		}

		p_rotation = l_rotation.toQuaternion();

	}

	Quaternion Matrix4::toQuaternion() const
	{
		//  | 00 04 08 12 |
		//  | 01 05 09 13 |
		//  | 02 06 10 14 |
		//  | 03 07 11 15 |

		//    column major
		// | 1-2(Y*Y)-2(Z*Z)        2XY+2WZ          2XZ-2WY        0 | 
		// |     2XY-2WZ        1-2(X*X)-2(Z*Z)      2YZ+2WX        0 |
		// |     2XZ+2WY           2YZ-2WX        1-2(X*X)-2(Y*Y)   0 |
		// |        0                 0                  0          1 | 


		// get the trace
		//  trace = 1-2(Y*Y)-2(Z*Z) + 1-2(X*X)-2(Z*Z) + 1-2(X*X)-2(Y*Y) + 1
		//        = 4 - 4(x*x + y*y + z*z)
		// if rotation matrix  // (w * w + x * x + y * y + z * z = 1)
		// so trace = 4(W*W)
		// W = sqrt(trace);
		// X = (M21 - M12)/4W   Y = (M02 - M20)/ 4W  Z = (M10 - M01)/ 4W
		// if W = 0 then


		Quaternion q;

		float trace = m_mat4[0][0] + m_mat4[1][1] + m_mat4[2][2];

		float s;
		int i, j, k;

		if (trace > 0.0f)
		{
			s = sqrt(trace + 1.0f);
			q[3] = s * .5f;
			s = .5f / s;

			q[0] = (m_mat4[1][2] - m_mat4[2][1]) * s;
			q[1] = (m_mat4[2][0] - m_mat4[0][2]) * s;
			q[2] = (m_mat4[0][1] - m_mat4[1][0]) * s;

		}
		else
		{
			i = 0; // X = 0
			if (m_mat4[1][1] > m_mat4[0][0]) i = 1;
			if (m_mat4[2][2] > m_mat4[i][i]) i = 2;
			j = (i + 1) % 3; k = (j + 1) % 3;
			s = 2.0f * sqrt(m_mat4[i][i] - (m_mat4[j][j] + m_mat4[k][k]) + 1.0f);
			q[i] = 0.25f * s;

			q[3] = (m_mat4[j][k] - m_mat4[k][j]) / s;
			q[j] = (m_mat4[i][j] + m_mat4[j][i]) / s;
			q[k] = (m_mat4[i][k] + m_mat4[k][i]) / s;
		}

		return q;
	}

	/////========================================================================/////
	/////     Functions                                                          /////
	/////========================================================================/////
	Matrix4 Matrix4::build(const Vector3 &p_position, const Quaternion &p_rotation, const Vector3 &p_scale)
	{

		Matrix4 l_trans = Matrix4::translation(p_position);


		/* Matrix4 l_rotX = Matrix4::rotationAxis(Vector3::UnitX, p_rotation.getX());
		 Matrix4 l_rotY = Matrix4::rotationAxis(Vector3::UnitY, p_rotation.getY());
		 Matrix4 l_rotZ = Matrix4::rotationAxis(Vector3::UnitZ, p_rotation.getZ());*/

		 // convert eular rotation to Quaternion
		Matrix4 l_rot = p_rotation.toMatrix4();

		Matrix4 l_scal = Matrix4::scale(p_scale);

		Matrix4 l_ret = l_trans * l_rot *l_scal;
		return l_ret;

	}

	Matrix4 Matrix4::translation(const Vector3 & position)
	{

		Matrix4 l_result;

		l_result.m_mat4[0][3] = position.getX();
		l_result.m_mat4[1][3] = position.getY();
		l_result.m_mat4[2][3] = position.getZ();
		l_result.m_mat4[3][3] = 1;

		return l_result;
	}

	//Matrix4 Matrix4::rotationAxisX(float angle)
	//{
	//    Matrix4 temp;
	//
	//    temp[1][1] = glm::cos(angle);
	//    temp[1][2] = -glm::sin(angle);
	//    temp[2][1] = glm::sin(angle);
	//    temp[2][2] = glm::cos(angle);
	//
	//    return temp;
	//}
	//
	//Matrix4 Matrix4::rotationAxisY(float angle)
	//{
	//    Matrix4 temp;
	//
	//    temp[0][0] = glm::cos(angle);
	//    temp[0][2] = glm::sin(angle);
	//    temp[2][0] = -glm::sin(angle);
	//    temp[2][2] = glm::cos(angle);
	//
	//    return temp;
	//}
	//
	//Matrix4 Matrix4::rotationAxisZ(float angle)
	//{
	//    Matrix4 temp;
	//
	//    temp[0][0] = glm::cos(angle);
	//    temp[0][1] = -glm::sin(angle);
	//    temp[1][0] = glm::sin(angle);
	//    temp[1][1] = glm::cos(angle);
	//
	//    return temp;
	//}

	Matrix4 Matrix4::rotationAxis(const Vector3 & p_axis, float p_angle)
	{
		//https://en.wikipedia.org/wiki/Rotation_matrix
		//| [cos(0)+x^2(1-cos(0))] [xy(1-cos(0))-zsin(0)] [xz(1-cos(0))+ysin(0)] |
		//| [xy(1-cos(0))+zsin(0)] [cos(0)+y^2(1-cos(0))] [yz(1-cos(0))-xsin(0)] |
		//| [xz(1-cos(0))-ysin(0)] [yz(1-cos(0))+xsin(0)] [cos(0)+z^2(1-cos(0))] |

		Vector3 p_axis_norm = p_axis;
		p_axis_norm.normalize();

		float l_c0 = std::cos(p_angle);
		float l_ic0 = 1.0f - l_c0;
		float l_s0 = std::sin(p_angle);

		float l_x = p_axis_norm.getX();
		float l_y = p_axis_norm.getY();
		float l_z = p_axis_norm.getZ();

		float l_xsq = l_x * l_x;
		float l_ysq = l_y * l_y;
		float l_zsq = l_z * l_z;

		Matrix4 l_result;

		l_result.m_mat4[0][0] = l_c0 + (l_xsq * l_ic0);
		l_result.m_mat4[0][1] = (l_x*l_y*l_ic0) - l_z * l_s0;
		l_result.m_mat4[0][2] = (l_x*l_z*l_ic0) + l_y * l_s0;
		l_result.m_mat4[1][0] = (l_x*l_y*l_ic0) + l_z * l_s0;
		l_result.m_mat4[1][1] = l_c0 + (l_ysq * l_ic0);
		l_result.m_mat4[1][2] = (l_y*l_z*l_ic0) - l_x * l_s0;
		l_result.m_mat4[2][0] = (l_x*l_z*l_ic0) - l_y * l_s0;
		l_result.m_mat4[2][1] = (l_y*l_z*l_ic0) + l_x * l_s0;
		l_result.m_mat4[2][2] = l_c0 + (l_zsq * l_ic0);

		return l_result;
	}

	Matrix4 Matrix4::computeProjMatrix(float p_fov, float p_aspect, float p_znear, float p_zfar, bool)
	{

		//http://ogldev.atspace.co.uk/www/tutorial12/tutorial12.html

		Matrix4 l_mat;

		l_mat.m_mat4[0][0] = /*(2.0f * D)/ width;*/ /*(leftHand ? 1 : -1) **/   1 / (p_aspect * std::tan(p_fov / 2.0f));
		l_mat.m_mat4[1][1] = /*(2.0f * D) / height;*/  1 / std::tan(p_fov / 2.0f);
		l_mat.m_mat4[2][2] = (-p_znear - p_zfar) / (p_znear - p_zfar);
		l_mat.m_mat4[2][3] = (2.0f* p_znear *p_zfar) / (p_znear - p_zfar);
		l_mat.m_mat4[3][2] = 1.00000000f;
		l_mat.m_mat4[3][3] = 0;

		return l_mat;
	}

	Matrix4 Matrix4::scale(const Vector3 & p_scale)
	{
		Matrix4 l_result;
		l_result.m_mat4[0][0] = (p_scale.getX()) ? p_scale.getX() : 1;
		l_result.m_mat4[1][1] = (p_scale.getY()) ? p_scale.getY() : 1;
		l_result.m_mat4[2][2] = (p_scale.getZ()) ? p_scale.getZ() : 1;
		l_result.m_mat4[3][3] = 1;

		//glm::scale(result, scale);
		return l_result;
	}

	void Matrix4::orthonormalize()
	{
		Vector3 nforward = forward().normalized();
		Vector3 nup = up().normalized();
		Vector3 nright = right().normalized();

		m_mat4[0][0] = nright.x;
		m_mat4[0][1] = nup.x;
		m_mat4[0][2] = nforward.x;
		m_mat4[1][0] = nright.y;
		m_mat4[1][1] = nup.y;
		m_mat4[1][2] = nforward.y;
		m_mat4[2][0] = nright.z;
		m_mat4[2][1] = nup.z;
		m_mat4[2][2] = nforward.z;
	}

	Matrix4 Matrix4::inverse() const
	{
		Matrix4 inverted;
		float determinant = getDeterminant();
		//ErrorIf(Math::IsZero(determinant), "Matrix4 - Uninvertible matrix.");
		determinant = 1.0f / determinant;
		inverted.m00 = m12 * m23 * m31 - m13 * m22 * m31;
		inverted.m00 += m13 * m21 * m32 - m11 * m23 * m32;
		inverted.m00 += m11 * m22 * m33 - m12 * m21 * m33;
		inverted.m00 *= determinant;

		inverted.m01 = m03 * m22 * m31 - m02 * m23 * m31;
		inverted.m01 += m01 * m23 * m32 - m03 * m21 * m32;
		inverted.m01 += m02 * m21 * m33 - m01 * m22 * m33;
		inverted.m01 *= determinant;

		inverted.m02 = m02 * m13 * m31 - m03 * m12 * m31;
		inverted.m02 += m03 * m11 * m32 - m01 * m13 * m32;
		inverted.m02 += m01 * m12 * m33 - m02 * m11 * m33;
		inverted.m02 *= determinant;

		inverted.m03 = m03 * m12 * m21 - m02 * m13 * m21;
		inverted.m03 += m01 * m13 * m22 - m03 * m11 * m22;
		inverted.m03 += m02 * m11 * m23 - m01 * m12 * m23;
		inverted.m03 *= determinant;

		inverted.m10 = m13 * m22 * m30 - m12 * m23 * m30;
		inverted.m10 += m10 * m23 * m32 - m13 * m20 * m32;
		inverted.m10 += m12 * m20 * m33 - m10 * m22 * m33;
		inverted.m10 *= determinant;

		inverted.m11 = m02 * m23 * m30 - m03 * m22 * m30;
		inverted.m11 += m03 * m20 * m32 - m00 * m23 * m32;
		inverted.m11 += m00 * m22 * m33 - m02 * m20 * m33;
		inverted.m11 *= determinant;

		inverted.m12 = m03 * m12 * m30 - m02 * m13 * m30;
		inverted.m12 += m00 * m13 * m32 - m03 * m10 * m32;
		inverted.m12 += m02 * m10 * m33 - m00 * m12 * m33;
		inverted.m12 *= determinant;

		inverted.m13 = m02 * m13 * m20 - m03 * m12 * m20;
		inverted.m13 += m03 * m10 * m22 - m00 * m13 * m22;
		inverted.m13 += m00 * m12 * m23 - m02 * m10 * m23;
		inverted.m13 *= determinant;

		inverted.m20 = m11 * m23 * m30 - m13 * m21 * m30;
		inverted.m20 += m13 * m20 * m31 - m10 * m23 * m31;
		inverted.m20 += m10 * m21 * m33 - m11 * m20 * m33;
		inverted.m20 *= determinant;

		inverted.m21 = m03 * m21 * m30 - m01 * m23 * m30;
		inverted.m21 += m00 * m23 * m31 - m03 * m20 * m31;
		inverted.m21 += m01 * m20 * m33 - m00 * m21 * m33;
		inverted.m21 *= determinant;

		inverted.m22 = m01 * m13 * m30 - m03 * m11 * m30;
		inverted.m22 += m03 * m10 * m31 - m00 * m13 * m31;
		inverted.m22 += m00 * m11 * m33 - m01 * m10 * m33;
		inverted.m22 *= determinant;

		inverted.m23 = m03 * m11 * m20 - m01 * m13 * m20;
		inverted.m23 += m00 * m13 * m21 - m03 * m10 * m21;
		inverted.m23 += m01 * m10 * m23 - m00 * m11 * m23;
		inverted.m23 *= determinant;

		inverted.m30 = m12 * m21 * m30 - m11 * m22 * m30;
		inverted.m30 += m10 * m22 * m31 - m12 * m20 * m31;
		inverted.m30 += m11 * m20 * m32 - m10 * m21 * m32;
		inverted.m30 *= determinant;

		inverted.m31 = m01 * m22 * m30 - m02 * m21 * m30;
		inverted.m31 += m02 * m20 * m31 - m00 * m22 * m31;
		inverted.m31 += m00 * m21 * m32 - m01 * m20 * m32;
		inverted.m31 *= determinant;

		inverted.m32 = m02 * m11 * m30 - m01 * m12 * m30;
		inverted.m32 += m00 * m12 * m31 - m02 * m10 * m31;
		inverted.m32 += m01 * m10 * m32 - m00 * m11 * m32;
		inverted.m32 *= determinant;

		inverted.m33 = m01 * m12 * m20 - m02 * m11 * m20;
		inverted.m33 += m02 * m10 * m21 - m00 * m12 * m21;
		inverted.m33 += m00 * m11 * m22 - m01 * m10 * m22;
		inverted.m33 *= determinant;

		return inverted;
	}

	const Matrix4 & Matrix4::invert()
	{
		*this = inverse();
		return *this;
	}

	float Matrix4::getDeterminant() const
	{

		// https://stackoverflow.com/questions/2937702/i-want-to-find-determinant-of-4x4-matrix-in-c-sharp
		float det = m03 * m12 * m21 * m30 - m02 * m13 * m21 * m30;
		det += m01 * m13 * m22 * m30 - m03 * m11 * m22 * m30;
		det += m02 * m11 * m23 * m30 - m01 * m12 * m23 * m30;
		det += m02 * m13 * m20 * m31 - m03 * m12 * m20 * m31;
		det += m03 * m10 * m22 * m31 - m00 * m13 * m22 * m31;
		det += m00 * m12 * m23 * m31 - m02 * m10 * m23 * m31;
		det += m03 * m11 * m20 * m32 - m01 * m13 * m20 * m32;
		det += m00 * m13 * m21 * m32 - m03 * m10 * m21 * m32;
		det += m01 * m10 * m23 * m32 - m00 * m11 * m23 * m32;
		det += m01 * m12 * m20 * m33 - m02 * m11 * m20 * m33;
		det += m02 * m10 * m21 * m33 - m00 * m12 * m21 * m33;
		det += m00 * m11 * m22 * m33 - m01 * m10 * m22 * m33;

		return det;
	}

	Matrix4 Matrix4::transpose() const
	{
		return Matrix4::transpose(*this);
	}

	Matrix4 Matrix4::transpose(const Matrix4 & p_other)
	{
		Matrix4 l_result;
		l_result[0] = Vector4(p_other.m_mat4[0][0], p_other.m_mat4[1][0], p_other.m_mat4[2][0], p_other.m_mat4[3][0]);
		l_result[1] = Vector4(p_other.m_mat4[0][1], p_other.m_mat4[1][1], p_other.m_mat4[2][1], p_other.m_mat4[3][1]);
		l_result[2] = Vector4(p_other.m_mat4[0][2], p_other.m_mat4[1][2], p_other.m_mat4[2][2], p_other.m_mat4[3][2]);
		l_result[3] = Vector4(p_other.m_mat4[0][3], p_other.m_mat4[1][3], p_other.m_mat4[2][3], p_other.m_mat4[3][3]);
		return l_result;
	}

	float * Matrix4::toFloatPtr()
	{
		return (float*)this;
	}

	std::string Matrix4::toStringPtr() const
	{
		std::stringstream l_ss;

		l_ss << "|" << m_mat4[0][0] << " " << m_mat4[0][1] << " " << m_mat4[0][2] << " " << m_mat4[0][3] << "|\n"
			<< "|" << m_mat4[1][0] << " " << m_mat4[1][1] << " " << m_mat4[1][2] << " " << m_mat4[1][3] << "|\n"
			<< "|" << m_mat4[2][0] << " " << m_mat4[2][1] << " " << m_mat4[2][2] << " " << m_mat4[2][3] << "|\n"
			<< "|" << m_mat4[3][0] << " " << m_mat4[3][1] << " " << m_mat4[3][2] << " " << m_mat4[3][3] << "|";

		return l_ss.str();
	}

	Matrix4 Matrix4::GetIdentitiyMatrix()
	{
		return Matrix4();
	}

	std::ostream & Matrix4::operator<<(std::ostream & os) const
	{
		os << toStringPtr();
		return os;
	}

	//Matrix4 createRotationMatrixFromEularAngles(Vector3 p_eular)
	//{
	//    Quaternion l_q = toQuaternion(p_eular);
	//
	//    return l_q.toMatrix4();
	//}

	Matrix4 operator*(float lhs, const Matrix4 & rhs)
	{
		return rhs * lhs;
	}


	Matrix4 Matrix4::computeOrthographicMatrix(float p_right, float p_left, float p_top, float p_bottom, float p_znear, float p_zfar, bool)
	{
		Matrix4 l_mat;

		l_mat.m00 = -2.0f / (p_right - p_left);
		l_mat.m30 = 0;//-(p_right + p_left)/ (p_right - p_left);
		l_mat.m11 = 2.0f / (p_top - p_bottom);
		l_mat.m31 = 0;//-(p_top + p_bottom)/(p_top - p_bottom	);
		l_mat.m22 = 2.0f / (p_zfar - p_znear);
		l_mat.m31 = (p_zfar + p_znear) / (p_zfar - p_znear);
		l_mat.m33 = 1.0f;

		return l_mat;
	}

	Matrix4 Matrix4::computeModelMatrixFromLookAtVector(Vector3 p_position, Vector3 p_point)
	{

		Vector3 l_forward = (p_position- p_point).normalized();
		Vector3 l_rightVector = Vector3(0, 1, 0).crossP(l_forward);
		Vector3 l_upVector = l_forward.crossP(l_rightVector);
		// set the forward vector
		 
		Matrix4 l_m;

		l_m.m00 = l_rightVector.x;
		l_m.m01 = l_rightVector.y;
		l_m.m02 = l_rightVector.z;


		l_m.m10 = l_upVector.x;
		l_m.m11 = l_upVector.y;
		l_m.m12 = l_upVector.z;

		l_m.m20 = l_forward.x;
		l_m.m21 = l_forward.y;
		l_m.m22 = l_forward.z;
		

	  l_m.m30 = p_position.dotP(l_rightVector);
		l_m.m31 = p_position.dotP(l_upVector);
		l_m.m32 = p_position.dotP(l_forward);
		l_m.m33 = 1;
		
		return l_m;
	}

}

