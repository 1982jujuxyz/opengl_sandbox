/*!***************************************************************************************
\file       Matrix2.cpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      This is the definition of the Matrix2 class. ( column-major order )
*****************************************************************************************/
#include "Precompiled.h"	
#include "Matrix2.h"

namespace Math
{

	Matrix2::Matrix2()
	{
		m00 = 1;
		m01 = 0;
		m10 = 0;
		m11 = 1;
	}

	Matrix2::Matrix2(const Matrix2 & p_other)
	{
		m00 = p_other.m00;
		m01 = p_other.m01;
		m10 = p_other.m10;
		m11 = p_other.m11;
	}

	//==============================================================================//
	//        Getters & Setters                                                     //
	//==============================================================================//
	void Matrix2::operator+=(const Matrix2& p_rhs)
	{
		m00 += p_rhs.m00;
		m01 += p_rhs.m01;
		m10 += p_rhs.m10;
		m11 += p_rhs.m11;
	}

	void Matrix2::operator-=(const Matrix2& p_rhs)
	{
		m00 -= p_rhs.m00;
		m01 -= p_rhs.m01;
		m10 -= p_rhs.m10;
		m11 -= p_rhs.m11;
	}

	void Matrix2::operator*=(const Matrix2& p_rhs)
	{
		Matrix2 & l_this = *this;
		l_this = concat(p_rhs);
	}

	Matrix2 Matrix2::concat(const Matrix2& p_rhs) const
	{
		Matrix2 ret;

		ret.m00 = m00 * p_rhs.m00 + m01 * p_rhs.m10;
		ret.m01 = m00 * p_rhs.m01 + m01 * p_rhs.m11;
		ret.m10 = m10 * p_rhs.m00 + m11 * p_rhs.m10;
		ret.m11 = m10 * p_rhs.m01 + m11 * p_rhs.m11;

		return ret;
	}


	void Matrix2::operator/=(const Matrix2& rhs)
	{
		m00 /= rhs.m00;
		m01 /= rhs.m01;
		m10 /= rhs.m10;
		m11 /= rhs.m11;

	}

	void Matrix2::operator+=(const float& p_rhs)
	{
		m00 += p_rhs;
		m01 += p_rhs;
		m10 += p_rhs;
		m11 += p_rhs;
	}

	void Matrix2::operator-=(const float& p_rhs)
	{
		m00 -= p_rhs;
		m01 -= p_rhs;
		m10 -= p_rhs;
		m11 -= p_rhs;
	}

	void Matrix2::operator*=(const float& p_rhs)
	{
		m00 *= p_rhs;
		m01 *= p_rhs;
		m10 *= p_rhs;
		m11 *= p_rhs;
	}

	void Matrix2::operator/=(const float& p_rhs)
	{
		m00 /= p_rhs;
		m01 /= p_rhs;
		m10 /= p_rhs;
		m11 /= p_rhs;
	}

	Matrix2 Matrix2::operator+(const Matrix2& p_rhs) const
	{
		Matrix2 temp = *this;
		temp += p_rhs;
		return temp;
	}

	Matrix2 Matrix2::operator-(const Matrix2& p_rhs) const
	{
		Matrix2 temp = *this;
		temp -= p_rhs;
		return temp;
	}

	Matrix2 Matrix2::operator*(const Matrix2& p_rhs) const
	{
		Matrix2 temp = *this;
		temp *= p_rhs;
		return temp;
	}

	Matrix2 Matrix2::operator/(const Matrix2& p_rhs) const
	{
		Matrix2 temp = *this;
		temp /= p_rhs;
		return temp;
	}

	Matrix2 Matrix2::operator+(const float& p_rhs) const
	{
		Matrix2 temp = *this;
		temp += p_rhs;
		return temp;
	}

	Matrix2 Matrix2::operator-(const float& p_rhs) const
	{
		Matrix2 temp = *this;
		temp -= p_rhs;
		return temp;
	}

	Matrix2 Matrix2::operator*(const float& p_rhs) const
	{
		Matrix2 temp = *this;
		temp *= p_rhs;
		return temp;
	}

	Matrix2 Matrix2::operator/(const float& p_rhs) const
	{
		Matrix2 temp = *this;
		temp /= p_rhs;
		return temp;
	}

	/////========================================================================/////
	/////     Functions                                                          /////
	/////========================================================================/////



	Matrix2 Matrix2::scale(const Vector2 & p_scale)
	{
		Matrix2 l_result;
		l_result[0][0] = (p_scale.x) ? p_scale.x : 1;
		l_result[1][1] = (p_scale.y) ? p_scale.y : 1;

		return l_result;
	}

	float Matrix2::determinate()
	{
		return m00 * m11 - m01 * m10;
	}

	Matrix2 Matrix2::Concat(Matrix2 & rhs) const
	{
		Matrix2 ret;

		ret.m00 = vect[0].dotP(Vector2(rhs.m00, rhs.m10));
		ret.m01 = vect[0].dotP(Vector2(rhs.m01, rhs.m11));

		ret.m10 = vect[1].dotP(Vector2(rhs.m00, rhs.m10));
		ret.m11 = vect[1].dotP(Vector2(rhs.m01, rhs.m11));

		return ret;
	}

	Matrix2 Matrix2::inverse()
	{
		Matrix2 temp;

		temp.m00 = m11;
		temp.m01 = -m10;
		temp.m10 = -m01;
		temp.m11 = m00;
		temp *= 1 / determinate();

		return temp;
	}


	const float* Matrix2::toFloatPtr()
	{
		return reinterpret_cast<float*>(this);
	}


	Matrix2 Matrix2::GetIdentitiyMatrix()
	{
		return Matrix2();
	}

}

