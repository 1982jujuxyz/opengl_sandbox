/*!***************************************************************************************
\file       Vector2.cpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      This is the definition of the Vector2 class.
*****************************************************************************************/

#include "Precompiled.h"
#include "Vector2.h"


namespace Math
{

	Vector2::Vector2(float p_x, float p_y) : x(p_x), y(p_y)
	{ }


	float& Vector2::operator[](unsigned p_index)
	{
		return toFloatPtr()[p_index];
	}

	float Vector2::operator[](const unsigned & p_index) const
	{
		return toFloatPtr()[p_index];
	}

	Vector2 Vector2::operator-() const
	{
		return Vector2(-x, -y);
	}


	void Vector2::operator+=(const Vector2 & p_rhs)
	{
		x += p_rhs.x;
		y += p_rhs.y;
	}

	void Vector2::operator-=(const Vector2 & p_rhs)
	{
		x -= p_rhs.x;
		y -= p_rhs.y;
	}

	void Vector2::operator*=(const Vector2 & p_rhs)
	{
		x *= p_rhs.x;
		y *= p_rhs.y;
	}

	void Vector2::operator/=(const Vector2 & p_rhs)
	{
		x /= p_rhs.x;
		y /= p_rhs.y;
	}

	void Vector2::operator+=(const float & p_rhs)
	{
		x += p_rhs;
		y += p_rhs;
	}

	void Vector2::operator-=(const float & p_rhs)
	{
		x -= p_rhs;
		y -= p_rhs;
	}

	void Vector2::operator*=(const float & p_rhs)
	{
		x *= p_rhs;
		y *= p_rhs;
	}

	void Vector2::operator/=(const float & p_rhs)
	{
		x /= p_rhs;
		y /= p_rhs;
	}

	Vector2 Vector2::operator+(const Vector2 & p_rhs) const
	{
		Vector2 l_temp = *this;
		l_temp += p_rhs;
		return l_temp;
	}

	Vector2 Vector2::operator-(const Vector2 & p_rhs) const
	{
		Vector2 l_temp = *this;
		l_temp -= p_rhs;
		return l_temp;
	}

	Vector2 Vector2::operator*(const  Vector2 & p_rhs) const
	{
		Vector2 l_temp = *this;
		l_temp *= p_rhs;
		return l_temp;
	}

	Vector2 Vector2::operator/(const Vector2 & p_rhs) const
	{
		Vector2 l_temp = *this;
		l_temp /= p_rhs;
		return l_temp;
	}

	Vector2 Vector2::operator+(const float & p_rhs) const
	{
		Vector2 l_temp = *this;
		l_temp += p_rhs;
		return l_temp;
	}

	Vector2 Vector2::operator-(const float & p_rhs) const
	{
		Vector2 l_temp = *this;
		l_temp -= p_rhs;
		return l_temp;
	}

	Vector2 Vector2::operator*(const float & p_rhs) const
	{
		Vector2 l_temp = *this;
		l_temp *= p_rhs;
		return l_temp;
	}

	Vector2 Vector2::operator/(const float & p_rhs) const
	{
		Vector2 l_temp = *this;
		l_temp /= p_rhs;
		return l_temp;
	}

	//==============================================================================//
	//        Getters & Setters                                                     //
	//==============================================================================//

	/////========================================================================/////
	/////     Functions                                                          /////
	/////========================================================================/////
	float Vector2::distance() const
	{
		return sqrt(distanceSquared());
	}

	float Vector2::distanceSquared() const
	{
		return x * x + y * y;
	}

	float Vector2::dotP(const Vector2& p_rhs) const
	{
		return x * p_rhs.x + y * p_rhs.y;
	}


	Vector2 Vector2::normalized() const
	{

		float length = std::sqrt(x * x + y * y);

		return *this / length;
	}

	void Vector2::normalize()
	{

		float length = std::sqrt(x * x + y * y);

		*this /= length;
	}

	float const * Vector2::toFloatPtr() const
	{
		return reinterpret_cast<float const *>(this);
	}

	float * Vector2::toFloatPtr()
	{
		return reinterpret_cast<float*>(this);
	}

	std::string Vector2::toStringPtr()
	{
		std::stringstream l_ss;
		l_ss << "(" << x << ", " << y << ")";
		return l_ss.str();
	}

	
	const Vector2 Vector2::UnitX = Vector2(1.0f, 0.0f);
	const Vector2 Vector2::UnitY = Vector2(0.0f, 1.0f);

}


