#include "Precompiled.h"
#include "Quaternion.h"
#include "Vector3.h"
#include "Vector4.h"
#include "Matrix4.h"

namespace Math
{
	Quaternion::Quaternion()
		: x{ 0 }, y{ 0 }, z{ 0 }, w{ 1 }
	{
	}

	Quaternion::Quaternion(float p_x, float p_y, float p_z, float p_w)
	{
		x = p_x;
		y = p_y;
		z = p_z;
		w = p_w;
	}

	Quaternion::Quaternion(Vector3 axis, float angle)
	{
		axis.normalize();
		float l_sin = std::sin(angle * 0.5f);
		float l_cos = std::cos(angle * 0.5f);
		x = axis.x * l_sin;
		y = axis.y * l_sin;
		z = axis.z * l_sin;
		w = l_cos;
	}

	Matrix4 Quaternion::toMatrix4() const
	{
		return Matrix4(getForward(), getUp(), getRight());
	}

	void Quaternion::normalize()
	{
		float n = sqrt(x * x + y * y + z * z + w * w);
		x /= n;
		y /= n;
		z /= n;
		w /= n;
	}

	float& Quaternion::operator[](unsigned p_index)
	{
		return toVector4()[p_index];
	}

	float Quaternion::operator[](unsigned p_index) const
	{
		return toVector4()[p_index];
	}

	Vector4& Quaternion::toVector4()
	{
		return *(Vector4*)this;
	}

	const Vector4& Quaternion::toVector4() const
	{
		return *(Vector4*)this;
	}

	Vector3 Quaternion::getForward() const
	{
		return Vector3(2 * (x*z + w * y), 2 * (y*z - w * x), 1 - 2 * (x*x + y * y));
	}

	Vector3 Quaternion::getUp() const
	{
		return Vector3(2 * (x*y - w * z), 1 - 2 * (x*x + z * z), 2 * (y*z + w * x));
	}

	Vector3 Quaternion::getRight() const
	{
		return Vector3(1 - 2 * (y*y + z * z), 2 * (x*y + w * z), 2 * (x*z - w * y));
	}

	float Quaternion::getX() const { return x; }
	float Quaternion::getY() const { return y; }
	float Quaternion::getZ() const { return z; }
	float Quaternion::getW() const { return w; }

	void Quaternion::setX(float p_new_x) { x = p_new_x; }
	void Quaternion::setY(float p_new_y) { y = p_new_y; }
	void Quaternion::setZ(float p_new_z) { z = p_new_z; }
	void Quaternion::setW(float p_new_w) { w = p_new_w; }


	void Quaternion::operator+=(const Quaternion & rhs)
	{
		x += rhs.x;
		y += rhs.y;
		z += rhs.z;
		w += rhs.w;
	}

	Quaternion Quaternion::operator+(const Quaternion& rhs) const
	{
		return Quaternion(x + rhs.x, y + rhs.y, z + rhs.z, w + rhs.w);
	}

	Quaternion Quaternion::operator-(const Quaternion& rhs) const
	{
		return Quaternion(*this * rhs.Conjugated());
	}

	Quaternion Quaternion::operator*(const float rhs) const
	{
		return Quaternion(x * rhs, y * rhs, z * rhs, w * rhs);
	}

	Quaternion Quaternion::operator*(const Vector3 rhs) const
	{
		return Quaternion(w * rhs.x + y * rhs.z - z * rhs.y,
			w * rhs.y + z * rhs.x - x * rhs.z,
			w * rhs.z + x * rhs.y - y * rhs.x,
			-x * rhs.x - y * rhs.y - z * rhs.z);
	}

	Quaternion Quaternion::operator*(Quaternion quat) const
	{
		return Quaternion(w * quat.x + x * quat.w + y * quat.z - z * quat.y,
			w * quat.y + y * quat.w + z * quat.x - x * quat.z,
			w * quat.z + z * quat.w + x * quat.y - y * quat.x,
			w * quat.w - x * quat.x - y * quat.y - z * quat.z);
	}

	void Quaternion::operator*=(Quaternion rhs)
	{
		x = w * rhs.x + x * rhs.w + y * rhs.z - z * rhs.y;
		y = w * rhs.y + y * rhs.w + z * rhs.x - x * rhs.z;
		z = w * rhs.z + z * rhs.w + x * rhs.y - y * rhs.x;
		w = w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z;
		this->normalize();
	}

	void Quaternion::operator*=(const Math::Vector3 rhs)
	{
		x = w * rhs.x + y * rhs.z - z * rhs.y;
		y = w * rhs.y + z * rhs.x - x * rhs.z;
		z = w * rhs.z + x * rhs.y - y * rhs.x;
		w = -x * rhs.x - y * rhs.y - z * rhs.z;
	}

	void Quaternion::operator*=(float rhs)
	{
		x *= rhs;
		y *= rhs;
		z *= rhs;
		w *= rhs;
	}

	bool Quaternion::operator==(const Quaternion& p_other) const
	{
		return x == p_other.x && y == p_other.y && z == p_other.z && w == p_other.w;
	}

	void Quaternion::offsetX(float p_offset)
	{
		*this *= Quaternion(Vector3::UnitX, p_offset);
	}

	void Quaternion::offsetY(float p_offset)
	{
		*this *= Quaternion(Vector3::UnitY, p_offset);
	}

	void Quaternion::offsetZ(float p_offset)
	{
		*this *= Quaternion(Vector3::UnitZ, p_offset);
	}

	float* Quaternion::toFloatPtr()
	{
		return reinterpret_cast<float*>(this);
	}

	std::string Quaternion::toStringPtr() const
	{
		std::stringstream l_ss;
		l_ss << "(" << w << ", " << x << ", " << y << ", " << z << ")";
		return l_ss.str();
	}


	void Quaternion::Conjugate()
	{
		x *= -1.0f;
		y *= -1.0f;
		z *= -1.0f;
	}

	Quaternion Quaternion::Conjugated() const
	{
		return Quaternion(-x, -y, -z, w);
	}
}
