/*!***************************************************************************************
\file       Color.cpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content © 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      Used to make basic colors
*****************************************************************************************/
#include "Precompiled.h"
//========Self Include==================================================================//
#include "Color.h"
//========Static Deceleration==========================================================//
Math::Color const Math::  Color::red(1, 0, 0);
Math::Color const Math::  Color::green(0, 1, 0);
Math::Color const Math::  Color::blue(0, 0, 1);
Math::Color const Math::  Color::yellow(1, 1, 0);
Math::Color const Math::  Color::cyan(0, 1, 1);
Math::Color const Math::  Color::magenta(1, 0, 1);
Math::Color const Math::  Color::white(1, 1, 1);
Math::Color const Math::  Color::black(0, 0, 0);
Math::Color const Math::  Color::gray(0.5f, 0.5f, 0.5f);




