/*!***************************************************************************************
\file       Vector3.cpp
\author     Cannell, Cody
\date       6/19/18
\copyright  All content © 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief      This is the definition of the Vector3 class. 
*****************************************************************************************/

#include "Precompiled.h"
#include "Vector3.h"
#include "Quaternion.h"

namespace Math
{
	Vector3::Vector3() noexcept : x(0), y(0), z(0) {}

	Vector3::Vector3(float p_x, float p_y, float p_z) : x(p_x), y(p_y), z(p_z)
	{ }

	Vector3::Vector3(const Vector3 & p_other) : x(p_other.x), y(p_other.y), z(p_other.z)
	{ }

	Vector3& Vector3::operator=(const Vector3 & rhs)
	{
		x = rhs.x;
		y = rhs.y;
		z = rhs.z;

		return *this;
	}

	float& Vector3::operator[](const unsigned & p_index)
	{
		return toFloatPtr()[p_index];
	}

	float Vector3::operator[](const unsigned & p_index) const
	{
		return toFloatPtr()[p_index];
	}

	bool Vector3::operator==(const Vector3& p_other) const
	{
		return x == p_other.x && y == p_other.y && z == p_other.z;
	}

	bool Vector3::operator!=(const Vector3& p_other) const
	{
		return x != p_other.x || y != p_other.y || z != p_other.z;
	}

	Vector3 Vector3::operator-() const
	{
		return Vector3(-x, -y, -z);
	}


	void Vector3::operator+=(const Vector3 & p_rhs)
	{
		x += p_rhs.x;
		y += p_rhs.y;
		z += p_rhs.z;
	}

	void Vector3::operator-=(const Vector3 & p_rhs)
	{
		x -= p_rhs.x;
		y -= p_rhs.y;
		z -= p_rhs.z;
	}

	void Vector3::operator*=(const Vector3 & p_rhs)
	{
		x *= p_rhs.x;
		y *= p_rhs.y;
		z *= p_rhs.z;
	}

	void Vector3::operator/=(const Vector3 & p_rhs)
	{
		x /= p_rhs.x;
		y /= p_rhs.y;
		z /= p_rhs.z;
	}

	void Vector3::operator+=(const float & p_rhs)
	{
		x += p_rhs;
		y += p_rhs;
		z += p_rhs;
	}

	void Vector3::operator-=(const float & p_rhs)
	{
		x -= p_rhs;
		y -= p_rhs;
		z -= p_rhs;
	}

	void Vector3::operator*=(const float & p_rhs)
	{
		x *= p_rhs;
		y *= p_rhs;
		z *= p_rhs;
	}

	void Vector3::operator/=(const float & p_rhs)
	{
		x /= p_rhs;
		y /= p_rhs;
		z /= p_rhs;
	}

	Vector3 Vector3::operator+(const Vector3 & p_rhs) const
	{
		Vector3 l_temp = *this;
		l_temp += p_rhs;
		return l_temp;
	}

	Vector3 Vector3::operator-(const Vector3 & p_rhs) const
	{
		Vector3 l_temp = *this;
		l_temp -= p_rhs;
		return l_temp;
	}

	Vector3 Vector3::operator*(const  Vector3 & p_rhs) const
	{
		Vector3 l_temp = *this;
		l_temp *= p_rhs;
		return l_temp;
	}

	Vector3 Vector3::operator/(const Vector3 & p_rhs) const
	{
		Vector3 l_temp = *this;
		l_temp /= p_rhs;
		return l_temp;
	}

	Vector3 Vector3::operator+(const float & p_rhs) const
	{
		Vector3 l_temp = *this;
		l_temp += p_rhs;
		return l_temp;
	}

	Vector3 Vector3::operator-(const float & p_rhs) const
	{
		Vector3 l_temp = *this;
		l_temp -= p_rhs;
		return l_temp;
	}

	Vector3 Vector3::operator*(const float & p_rhs) const
	{
		Vector3 l_temp = *this;
		l_temp *= p_rhs;
		return l_temp;
	}

	Vector3 Vector3::operator/(const float & p_rhs) const
	{
		Vector3 l_temp = *this;
		l_temp /= p_rhs;
		return l_temp;
	}

	bool Vector3::operator==(const Vector3& p_rhs)
	{
		return x == p_rhs.x && y == p_rhs.y && z == p_rhs.z;
	}

	float Vector3::distance() const
	{
		return sqrt(distanceSquared());
	}

	float Vector3::distanceSquared() const
	{
		return x * x + y * y + z * z;
	}

	float Vector3::dotP(const Vector3 p_rhs) const
	{
		return x * p_rhs.x + y * p_rhs.y + z * p_rhs.z;
	}

	Vector3 Vector3::crossP(const Vector3 p_rhs) const
	{
		return Vector3(y * p_rhs.z - z * p_rhs.y, z * p_rhs.x - x * p_rhs.z, x * p_rhs.y - y * p_rhs.x);
	}

	Vector3 Vector3::normalized()
	{

		float length = std::sqrt(x * x + y * y + z * z);

		return *this / length;
	}

	void Vector3::normalize()
	{

		float length = std::sqrt(x * x + y * y + z * z);

		*this /= length;
	}

	const float* Vector3::toFloatPtr() const
	{
		return reinterpret_cast<const float*>(this);
	}

	float* Vector3::toFloatPtr()
	{
		return reinterpret_cast<float*>(this);
	}

	std::string Vector3::toStringPtr() const
	{
		std::stringstream l_ss;
		l_ss << "(" << x << ", " << y << ", " << z << ")";
		return l_ss.str();
	}

	const Vector3 Vector3::UnitX = Vector3(1.0f, 0.0f, 0.0f);
	const Vector3 Vector3::UnitY = Vector3(0.0f, 1.0f, 0.0f);
	const Vector3 Vector3::UnitZ = Vector3(0.0f, 0.0f, 1.0f);

	Vector3 operator*(float lhs, const Vector3 & rhs)
	{
		return rhs * lhs;
	}

	Vector3 Vector3::rotate(Quaternion rotation)
	{

		Quaternion conjugate = rotation.Conjugated();

		Quaternion w = rotation * *this * conjugate;

		return Vector3(w.x, w.y, w.z);

	}
}
       