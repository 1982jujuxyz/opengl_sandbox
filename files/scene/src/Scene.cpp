#include "Precompiled.h"
#include "Scene.h"
#include "Components/Transform.h"
#include "Components/Camera.h"
#include "Components/Render.h"
#include "Components/Material.h"
#include "Object.h"
#include "Vector3.h"
#include <glew-2.1.0/glew.h>
#include "Components/Light.h"
#include "InputHandler.h"
#include "DeltaTime.h"

const float movement_speed = 100.0f;
const float camera_speed = 100.0f;

void Scene::BuildDefaultScene()
{

  CreateMainCamera();
  CreateSkyBox();
  CreateDefaultLight();

  

  auto l_cameraTransform = m_mainCamera->GetComponent<Component::Transform>();
  auto l_cameraComp = m_mainCamera->GetComponent<Component::Camera>();

  // set up camera input
  auto moveCameraForward = [l_cameraTransform]() { l_cameraTransform->OffSetPosition(l_cameraTransform->getGlobalMatrix().forward() * (movement_speed * Time::DeltaTime())); };
  auto moveBackwards = [l_cameraTransform]() {l_cameraTransform->OffSetPosition(-l_cameraTransform->getGlobalMatrix().forward() * (movement_speed * Time::DeltaTime()));  };
  auto moveRight = [l_cameraTransform]() { l_cameraTransform->OffSetPosition(l_cameraTransform->getGlobalMatrix().right() * (movement_speed * Time::DeltaTime()));  };
  auto moveLeft = [l_cameraTransform]() { l_cameraTransform->OffSetPosition(-l_cameraTransform->getGlobalMatrix().right() * (movement_speed * Time::DeltaTime()));  };
  auto moveUp = [l_cameraTransform]() { l_cameraTransform->OffSetPosition(l_cameraTransform->getGlobalMatrix().up() * (movement_speed * Time::DeltaTime())); };
  auto moveDown = [l_cameraTransform]() { l_cameraTransform->OffSetPosition(-l_cameraTransform->getGlobalMatrix().up() * (movement_speed * Time::DeltaTime()));  };

  auto cameraLook = [l_cameraComp,l_cameraTransform](float x, float y, float z)
  {
    //l_cameraTransform->OffSetRotation();

    Math::Quaternion l_rot;
    Math::Vector3 l_trans;
    Math::Vector3 l_scale;

   // Math::Vector3 l_forwardVector = l_cameraTransform->getGlobalPosition() - (l_cameraTransform->getGlobalPosition() + Math::Vector3(x, y, z))

    l_cameraTransform->SetRotation(Math::Matrix4::computeModelMatrixFromLookAtVector(l_cameraTransform->getGlobalPosition(), (l_cameraTransform->getGlobalPosition() + Math::Vector3(x, y, z))));
   // l_cameraTransform->SetRotation(l_rot.toVector4().toVector3()*100.0f);
    
    l_cameraComp->process();
  };

  auto l_objectToRender = AddGameObjectToScene();
  auto l_transformComp = l_objectToRender->AddComponent<Component::Transform>();

  InputHandler::Bind(W, new Command(moveCameraForward));
  InputHandler::Bind(S, new Command(moveBackwards));
  InputHandler::Bind(A, new Command(moveLeft));
  InputHandler::Bind(D, new Command(moveRight));
  InputHandler::Bind(Space, new Command(moveUp));
  InputHandler::Bind(Left_Shift, new Command(moveDown));

  InputHandler::SetMouseCameraFunciton(cameraLook);



  l_transformComp->SetPosition(Math::Vector3(0, 0, 1));
  l_transformComp->SetScale(Math::Vector3(.1, .1, .1));
  l_transformComp->SetRotation(Math::Vector3(0, 5, 0));

  auto l_RenderComp = l_objectToRender->AddComponent<Component::Render>();
  l_RenderComp->m_modelName = "models/HEN_ry.obj";

  l_RenderComp->m_textureName = "textures/texture_uv.png";

}

Object* Scene::AddGameObjectToScene()
{
  m_gameObjects.push_back(new Object());

  return m_gameObjects.back();
}

void Scene::CreateMainCamera()
{
   m_mainCamera = new Object();
   auto l_cam = m_mainCamera->AddComponent<Component::Camera>();
   auto l_transformforCam = m_mainCamera->AddComponent<Component::Transform>();
   l_transformforCam->SetPosition(Math::Vector3(0, 0, -1));
   l_cam->setViewMatrix(l_transformforCam->getGlobalMatrix().inverse());
   
}

void Scene::CreateSkyBox()
{
  m_skyBox = new Object;
  auto l_skybox_render_component = m_skyBox->AddComponent<Component::Render>();
  auto l_skybox_transform_component = m_skyBox->AddComponent<Component::Transform>();
  auto l_skybox_material_component = m_skyBox->AddComponent<Component::Material>();


  l_skybox_render_component->m_texturePack = "textures/skybox_1.tpk";
  l_skybox_render_component->m_shaderProgram = "shaders/LightingProgram.ppk";
  l_skybox_render_component->m_modelName = "models/sampleCube.obj";
  l_skybox_render_component->m_materialName = "materials/basic.mtl";

  l_skybox_transform_component->SetPosition(Math::Vector3(0, 0, 0));
  l_skybox_transform_component->SetScale(Math::Vector3(100,100,100));




}


void Scene::CreateDefaultLight()
{
  auto l_light =  AddGameObjectToScene();
  l_light->AddComponent<Component::Light>();
  auto l_transform = l_light->AddComponent<Component::Transform>();
  l_transform->SetPosition({ 0, .4, 1 });
  l_transform->SetScale({ .1,.1,.1 });
  l_light->AddComponent<Component::Render>()->m_modelName = "models/HEN_ry.obj";
}