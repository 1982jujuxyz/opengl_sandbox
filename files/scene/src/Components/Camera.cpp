#include "Precompiled.h"
#include "Components/Camera.h"
#include "MathFunctions.h"


void Component::Camera::setProjectionMatrix(const Math::Matrix4& p_projMatrix)
{
	m_projectionMatrix = p_projMatrix;
}

void Component::Camera::setViewMatrix(const Math::Matrix4& p_viewMatrix)
{
	m_viewMatrix = p_viewMatrix;
}

void Component::Camera::setViewProjectionMatrix(const Math::Matrix4& p_viewProjMatrix)
{
	m_viewProjectionMatrix = p_viewProjMatrix;
}

void Component::Camera::setFieldOfViewDegree(float m_angleDegree)
{
	m_fieldOfView = Math::DegToRad(m_angleDegree);
	process();
}

float Component::Camera::getFieldOfViewDegree()
{
	return Math::RadToDeg(m_fieldOfView);
}

void Component::Camera::setDimension(float p_width, float p_height)
{
	m_width = p_width;
	m_height = p_height;

	process();
}

void Component::Camera::setWidth(float p_width)
{
	m_width = p_width;
	process();
}

void Component::Camera::setHeight(float p_height)
{
	m_height = p_height;
	process();
}

void Component::Camera::setNearPlaneDistance(float p_zNear)
{
	m_nearPlaneDist = p_zNear;
	process();
}

void Component::Camera::setFarPlaneDistance(float p_zFar)
{
	m_farPlaneDist = p_zFar;
	process();
}

void Component::Camera::process()
{

	//m_viewMatrix = trans->getGlobalMatrix().inverse();

	// Get the distance of viewport
	// float D = 2.0f * glm::tan(m_fieldOfView / 2.0f) / m_width;

	calcProjMatrix();

	calcViewProjMatrix();

	calcViewMatrix();

}

void Component::Camera::processNoTransform()
{
}

void Component::Camera::calcViewMatrix()
{

}

void Component::Camera::calcProjMatrix()
{
	m_projectionMatrix = Math::Matrix4::computeProjMatrix(m_fieldOfView, (m_width / m_height), m_nearPlaneDist, m_farPlaneDist, false);
}

void Component::Camera::calcViewProjMatrix()
{
	m_viewProjectionMatrix = m_viewMatrix * m_projectionMatrix;
}

void Component::Camera::calcCamPosition()
{
}

//void Camera::printData()
//{
//	logger l_logger("Camera");
//	l_logger.debug() << "fov: " << m_fieldOfView;
//	l_logger.debug() << "width: " << m_width;
//	l_logger.debug() << "height: " << m_height;
//	l_logger.debug() << "near: " << m_nearPlaneDist;
//	l_logger.debug() << "far: " << m_farPlaneDist << "\n";
//	printMatrices();
//
//}

//void Camera::printMatrices()
//{
//	logger l_logger("Camera");
//	l_logger.debug() << "viewMat: " << m_viewMatrix.toStringPtr() << "\n";
//	l_logger.debug() << "projMat: " << m_projectionMatrix.toStringPtr() << "\n";
//	l_logger.debug() << "vprojMat: " << m_viewProjectionMatrix.toStringPtr() << "\n";
//}

