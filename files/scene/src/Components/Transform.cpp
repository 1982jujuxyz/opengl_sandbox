#include "Precompiled.h"
#include "Components/Transform.h"
#include "MathFunctions.h"

void Component::Transform::SetRotation(const Math::Vector3& p_new_rotation)
{
	const Math::Quaternion xrot(Math::Vector3(1, 0, 0), p_new_rotation.x);
	const Math::Quaternion yrot(Math::Vector3(0, 1, 0), p_new_rotation.y);
	const Math::Quaternion zrot(Math::Vector3(0, 0, 1), p_new_rotation.z);

	local_rotation = xrot * yrot * zrot; MarkDirty();
}

void Component::Transform::SetRotation(const Math::Matrix4& p_new_rotation)
{
	local_rotation = p_new_rotation.toQuaternion(); MarkDirty();
}

void Component::Transform::SetPosition(const Math::Vector3& p_new_position)
{
	last_global_position = global_position;
	local_position = p_new_position; MarkDirty();
}

void Component::Transform::SetScale(const Math::Vector3& p_new_scale) { local_scale = p_new_scale; MarkDirty(); }

void Component::Transform::OffSetPosition(const Math::Vector3& p_new_offPosition)
{
	local_position += p_new_offPosition;
	MarkDirty();
}

void Component::Transform::OffSetRotation(const Math::Vector3& p_new_offRotation)
{

	//translate = glm::translate(translate, -eye);

	//viewMatrix = rotate * translate;
	Math::Quaternion pitch = Math::Quaternion({1,0,0}, p_new_offRotation.x);
	Math::Quaternion yaw = Math::Quaternion({ 0,1,0 }, p_new_offRotation.y);
	//Math::Quaternion roll = Math::Quaternion({ 0,0,1 }, p_new_offRotation.z);

	Math::Quaternion orientation = pitch * yaw;
	orientation.normalize();
	Math::Matrix4 rotate = orientation.toMatrix4();

	
	//Math::Quaternion q;
	//q.offsetX(p_new_offRotation.x);
	//q.offsetY(p_new_offRotation.y);
	//q.offsetZ(p_new_offRotation.y);

	local_rotation *= orientation;
	MarkDirty();
}

void Component::Transform::MarkDirty()
{
	is_dirty = true;
	//is_physicsDirty = true;

	//std::vector<std::shared_ptr<gameObject>> children = getGameObject().lock()->getChildren();

	/*for (auto& child : children) {
		child->getComponent<Transform>()->markDirty();
	}*/
}


void Component::Transform::CheckDirty()
{
	if (is_dirty)
	{
		//const auto getParentMat =
		//	[this]
		//{
		//	// get the parent's global matrix if available
		//	if (auto l_weak_parent = getGameObject(); !l_weak_parent.expired())
		//	{
		//		auto l_parent_ptr = l_weak_parent.lock()->getParent();
		//		if (!l_parent_ptr.expired())
		//		{
		//			auto l_parent_transform = l_parent_ptr.lock()->getComponent<Transform>();
		//			if (l_parent_transform)
		//			{
		//				return l_parent_transform->getGlobalMatrix();
		//			}
		//		}
		//	}

		//	return matrix4x4{}; // identity if no value
		//};



		// update matrices
		local_matrix = Math::Matrix4::build(local_position, local_rotation, local_scale);
		global_matrix = /*getParentMat() **/  local_matrix;

		// decompose global properties
		global_matrix.decompose(global_position, global_rotation, global_scale);

		// build last matrix with only some global properties
		global_matrix_no_transform = Math::Matrix4::build(Math::Vector3(0, 0, 0), global_rotation, global_scale);


		is_dirty = false;
	}
}

/*!***************************************************************************************
\brief  Retrieves the current position of the transform, relative to the parent.
\return vector3 - The (x,y,z) position of the transform, relative to the parent.
*****************************************************************************************/

const Math::Vector3& Component::Transform::getPosition() const { return local_position; }

/*!***************************************************************************************
\brief  Retrieves the current rotation of the transform, relative to the parent.
\return vector3 - The rotation about the axes (x,y,z) of the transform, relative to the parent.
*****************************************************************************************/

const Math::Quaternion& Component::Transform::getRotation() const { return local_rotation; }

/*!***************************************************************************************
\brief  Retrieves the current scale of the transform, relative to the parent.
\return vector3 - The scale in the dimensions (x,y,z) of the transform, relative to the parent.
*****************************************************************************************/

const Math::Vector3& Component::Transform::getScale() const { return local_scale; }

const Math::Matrix4& Component::Transform::getLocalMatrix()
{
	CheckDirty();
	return local_matrix;
}

const Math::Vector3& Component::Transform::getGlobalPosition()
{
	CheckDirty();
	return global_position;
}

const Math::Vector3& Component::Transform::getLastGlobalPosition()
{
	return last_global_position;
}

const Math::Quaternion& Component::Transform::getGlobalRotation()
{
	CheckDirty();
	return global_rotation;
}

const Math::Vector3& Component::Transform::getGlobalScale()
{
	CheckDirty();
	return global_scale;
}

const Math::Matrix4& Component::Transform::getGlobalMatrix()
{
	CheckDirty();
	return global_matrix;
}

const Math::Matrix4& Component::Transform::getGlobalMatrixNoTransform()
{
	CheckDirty();
	return global_matrix_no_transform;
}
