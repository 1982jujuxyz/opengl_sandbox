#pragma once
#include "ComponentBase.h"
#include <any>
#include <unordered_map>
#include <typeindex>
using ComponentID = long long int;

struct ComponentHandle
{
  ComponentID m_id;
};

template <typename T>
class ComponentPool
{
public:

  
  std::vector<T*>& GetComponentPool();
  T* AddComponentToPool(ComponentHandle& m_handle);
  T* GetComponentFromPool(const ComponentHandle& p_handle);

  void RemoveComponentFromPool(const ComponentHandle& p_id);

private:

  
  std::vector<T*> components;

};

template <typename T>
std::vector<T*>& ComponentPool<T>::GetComponentPool()
{
  return components;  
}

template <typename T>
T* ComponentPool<T>::AddComponentToPool(ComponentHandle& m_handle)
{
  components.push_back(new T());
  m_handle.m_id = static_cast<ComponentID>(components.size()-1);

  return components.back();
}


template <typename T>
T* ComponentPool<T>::GetComponentFromPool(const ComponentHandle& p_handle)
{
  // need to check to see if its in the vector
  return components[p_handle.m_id];
}


template <typename T>
void ComponentPool<T>::RemoveComponentFromPool(const ComponentHandle& p_id)
{

}


class ComponentPoolManager
{
public:
    template <typename T>
    static std::vector<T> & GetComponentPool();

    template <typename T>
    static T* GetComponentFromPool(ComponentHandle& p_handle);

    template <typename T>
    static T* AddComponentToPool(ComponentHandle& p_handle);


private:

  static std::unordered_map<std::type_index, std::any> m_pools;

};



template <typename T>
std::vector<T> & ComponentPoolManager::GetComponentPool()
{

 //=================== lambda ===================//
  //auto castToAnyPool = [](std::unordered_map<std::type_index, std::any>::const_iterator p_itter) -> ComponentPool<T> *
  //{
  //  ComponentPool<T> * pool_ptr = std::any_cast<ComponentPool<T>>(&p_itter->second);
  //  if (pool_ptr)
  //  {
  //    // print out error
  //  }
  //  return pool_ptr;
  //};
  //===============================================//


  auto TypeItter = m_pools.find(typeid(T));
  //TypeItter->second;

  if (TypeItter != m_pools.end())
  {
    auto & anycast = std::any_cast<ComponentPool<T>>(&TypeItter->second)->GetComponentPool();
    return anycast;
  }

  
  auto l_itter = m_pools.emplace(typeid(T), ComponentPool<T>());


  return std::any_cast<ComponentPool<T>>(&TypeItter->second)->GetComponentPool();

}


template <typename T>
T* ComponentPoolManager::GetComponentFromPool(ComponentHandle & p_handle)
{
  auto TypeItter = m_pools.find(typeid(T));


  if (TypeItter == m_pools.end())
  {
    std::cout << "error: pool " << typeid(T).name() << " does not exist" << std::endl;
  }

  return std::any_cast<ComponentPool<T>>(&TypeItter->second)->GetComponentFromPool(p_handle);
}


template <typename T>
T* ComponentPoolManager::AddComponentToPool(ComponentHandle& p_handle)
{
  auto TypeItter = m_pools.find(typeid(T));



  if(TypeItter == m_pools.end())
  {
    m_pools[typeid(T)] = ComponentPool<T>();
  }

  return std::any_cast<ComponentPool<T>>(&m_pools.find(typeid(T))->second)->AddComponentToPool(p_handle);
}




