#pragma once

#include "ComponentBase.h"
#include "Vector3.h"
#include "Matrix4.h"
#include "Quaternion.h"


namespace Component
{

	class Transform
	{

	public:
		/*!***************************************************************************************
	\brief  Assigns a new position to the transform.
	\param p_new_position - the new position for the transform.
	*****************************************************************************************/
		void SetPosition(const Math::Vector3& p_new_position);

		/*!***************************************************************************************
		\brief  Assigns a new rotation to the transform.
		\param p_new_rotation - the new rotation for the transform.
		*****************************************************************************************/
		void SetRotation(const Math::Vector3& p_new_rotation);

		void SetRotation(const Math::Matrix4& p_new_rotation);

		/*!***************************************************************************************
	\brief  Assigns a new scale to the transform.
	\param p_new_scale - the new scale for the transform.
	*****************************************************************************************/
		void SetScale(const Math::Vector3& p_new_scale);

		void OffSetPosition(const Math::Vector3& p_new_offPosition);
		void OffSetRotation(const Math::Vector3& p_new_offRotation);

		void MarkDirty();
		void CheckDirty();

		/*!***************************************************************************************
		\brief  Retrieves the current position of the transform, relative to the parent.
		\return vector3 - The (x,y,z) position of the transform, relative to the parent.
		*****************************************************************************************/
		const Math::Vector3& getPosition() const;
		/*!***************************************************************************************
		\brief  Retrieves the current rotation of the transform, relative to the parent.
		\return vector3 - The rotation about the axes (x,y,z) of the transform, relative to the parent.
		*****************************************************************************************/
		const Math::Quaternion& getRotation() const;
		/*!***************************************************************************************
		\brief  Retrieves the current scale of the transform, relative to the parent.
		\return vector3 - The scale in the dimensions (x,y,z) of the transform, relative to the parent.
		*****************************************************************************************/
		const Math::Vector3& getScale() const;
		/*!***************************************************************************************
		\brief  Retrieves the matrix that represents the current position, rotation and scale of the transform, relative to the parent.
		\return matrix4x4 - The affine matrix of the position, rotation and scale, relative to the parent.
		*****************************************************************************************/
		const Math::Matrix4& getLocalMatrix();

		/*!***************************************************************************************
		\brief  Retrieves the position of the transform within global space (accumulates all positions
				from the parent chain).
		\return vector3 - the (x,y,z) position of the transform in global space.
		*****************************************************************************************/
		const Math::Vector3& getGlobalPosition();

		/*!***************************************************************************************
		\brief  Retrieves the last position of the transform within global space (accumulates all positions
				from the parent chain).
		\return vector3 - the (x,y,z) position of the transform in global space.
		*****************************************************************************************/
		const Math::Vector3& getLastGlobalPosition();
		/*!***************************************************************************************
		\brief  Retrieves the rotation of the transform within global space (accumulates all rotations
				from the parent chain).
		\return vector3 - The rotation about the axes (x,y,z) of the transform in global space.
		*****************************************************************************************/
		const Math::Quaternion& getGlobalRotation();
		/*!***************************************************************************************
		\brief  Retrieves the scale of the transform within global space (accumulates all scales
				from the parent chain).
		\return vector3 - The scale in the dimensions (x,y,z) of the transform in global space.
		*****************************************************************************************/
		const Math::Vector3& getGlobalScale();
		/*!***************************************************************************************
		\brief  Retrieves the matrix that represents the global position, rotation and scale of the
				transform (accumulates all values from parent chain).
		\return matrix4x4 - The affine matrix of the position, rotation and scale in global space.
		*****************************************************************************************/
		const Math::Matrix4& getGlobalMatrix();

		const Math::Matrix4& getGlobalMatrixNoTransform();

	public:

		Math::Vector3 local_position; //!< The current local position of this transform (relative to parent)
		Math::Quaternion local_rotation; //!< The current local rotation of this transform (relative to parent)// yaw (Z), pitch (Y), roll (X)
		Math::Vector3 local_scale; //!< The current local scale of this transform (relative to parent)
		Math::Matrix4 local_matrix; //!< Affine matrix of the current local parameters (relative to parent)

		Math::Vector3 global_position; //!< The current global position of this transform.
		Math::Quaternion global_rotation; //!< The current global rotation of this transform.// yaw (Z), pitch (Y), roll (X)
		Math::Vector3 global_scale; //!< The current global scale of this transform.
		Math::Matrix4 global_matrix; //!< Affine matrix fo the current global parameters.
		Math::Matrix4 global_matrix_no_transform;

		Math::Vector3 last_global_position;

		bool is_dirty = false; //!< Indicator that the matrices are not consistent with vector values.

		//bool is_physicsDirty = false; //!< is_dirty flag for physics (bullet's transform)
	};
}