#include "Vector3.h"
#include "Matrix4.h"
#include "Color.h"
#include "ComponentBase.h"

const float Pi = 3.1415926535897932384626433832795f;

namespace Component
{
	[DebuggerDisplay("Camera", ViewMatrix = "{ m_viewMatrix}")]
	class Camera
	{
	public:
		~Camera() = default;

		void process();
		void processNoTransform();

		void calcViewMatrix();
		void calcProjMatrix();
		void calcViewProjMatrix();
		void calcCamPosition();

		/*!***************************************************************************************
		\brief  Gets the projection view matrix, the merge of the view matrix and the projection matrix.
		\return The matrix
		*****************************************************************************************/
		Math::Matrix4 getViewProjectionMatrix() const { return m_viewProjectionMatrix; }

		/*!***************************************************************************************
		\brief  Gets the view matrix.
		\return The matrix
		*****************************************************************************************/
		Math::Matrix4 getViewMatrix() const { return m_viewMatrix; }

		/*!***************************************************************************************
		\brief  Gets the projection matrix.
		\return The matrix
		*****************************************************************************************/
		Math::Matrix4 getProjectionMatrix() const { return m_projectionMatrix; }




		void setProjectionMatrix(const Math::Matrix4& p_projMatrix);
		void setViewMatrix(const class Math::Matrix4& p_viewMatrix);
		void setViewProjectionMatrix(const Math::Matrix4& p_viewProjMatrix);

		void setFieldOfViewDegree(float m_angleDegree);
		float getFieldOfViewDegree();

		void setDimension(float p_width, float p_height);

		float getWidth() const { return m_width; }
		void setWidth(float p_width);

		float getHeight() const { return m_height; }
		void setHeight(float p_height);

		float getNearPlaneDistance() const { return m_nearPlaneDist; }
		void setNearPlaneDistance(float p_zNear);

		float getFarPlaneDistance() const { return m_farPlaneDist; }
		void setFarPlaneDistance(float p_zFar);

		//virtual void setIsMainCamera(bool p_isMain) = 0;

		/*!***************************************************************************************
		\brief Prints camera data
		*****************************************************************************************/
		//virtual void printData();

		/*!***************************************************************************************
		\brief Prints matrices data
		*****************************************************************************************/
		//virtual void printMatrices();


		/*!***************************************************************************************
		\brief Retrieves the active status of this camera
		\return Returns true if the camera is active, false otherwise
		*****************************************************************************************/
		virtual bool getActive() { return m_active; }
		virtual void setActive(bool p_active) { m_active = p_active; }

	public:

		Math::Matrix4 m_viewMatrix;              //!< The view matrix
		Math::Matrix4 m_projectionMatrix;        //!< The projection matrix
		Math::Matrix4 m_viewProjectionMatrix;    //!< The view projection matrix
		Math::Matrix4 m_orthographicMatrix;   //!< The ortho matrix
		Math::Vector3 m_cameraPosition;						 //!< The current position of this camera

		Math::Vector3 m_viewVec = { 0,0,-1 };      //!< The view vector
		float m_fieldOfView = Pi / 4.0f;     //!< The FOV
		float m_width = 160.0f;              //!< The width of the lens
		float m_height = 90.0f;              //!< The height of the lens
		float m_nearPlaneDist = 0.1f;        //!< The distance to the near plane
		float m_farPlaneDist = 300.0f;      //!< The distance to the far plane
		Math::Color m_fogColor = Math::Color::gray;      //!< The fog color

		bool m_active = true;                //!< Is the camera currently active

	};
}


