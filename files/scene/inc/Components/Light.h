#pragma once
#include "Vector3.h"
#include "Color.h"
#include "Utilities.h"

enum class lighttype
{
  enm_directional = 0,
  enm_point = 1,
  enm_spot = 2,
  enm_count
};

namespace Component
{

  struct Light
  {
    bool m_isActive = true;
    bool m_castShadow = false;

    lighttype m_type = lighttype::enm_directional;
    float m_intensity = 2.0f;
    float m_radius = 10.0f;
    float m_innerAngle = c_Pi / 12.0f;//15 degree
    float m_outerAngle = c_Pi / 6.0f;//30 degree
    float m_spotFalloff = 2.0f;

    //distance attenuation will only be set when this is true.
    bool m_ifDecay = true;
    Math::Vector3 m_disAtten = { 1.0f,0.0014f, 0.000007f };//constant, linear, quadratic

    Math::Color m_diffuseColor = Math::Color(1, 1, 1);
    Math::Color m_ambientColor = Math::Color(0, 0, 0);
    Math::Color m_specularColor = Math::Color(0, 0, 0);

    static Math::Color m_globalAmbient;
  };
}