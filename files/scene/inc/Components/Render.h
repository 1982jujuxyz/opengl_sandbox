#pragma once
#include <functional>


namespace Component
{

	struct Render
	{

		//programType m_programType = programType::enm_forward;


		std::function<void()> m_preRenderGLCalls;
		std::function<void()> m_postRenderGLCalls;

		//std::string m_fontName = "arial.ttf";
		std::string m_materialName = "materials/basic.mtl";
		//std::string m_defaultMaterialName = m_materialName;  //this allows us to change the material then get back to the original one
		std::string m_texturePack = "textures/texture_uv.tpk";
		std::string m_textureName = "textures/texture_uv.png";
		std::string m_modelName = "models/sampleCube.obj";
		std::string m_shaderProgram = "shaders/LightingProgram.ppk";

	};
}
