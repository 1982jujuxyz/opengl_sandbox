#pragma once

#include "ComponentPool.h"

class Object;

class ComponentId;



//class ObjectHandle
//{
//
//
//public:
//  Object * GetObject() {}
//
//  template<typename T>
//  Component* GetComponent();
//
//private:
//  std::string m_name;
//  ObjectId id = 0;
//};

class Object
{
public:
  template <typename T>
  T* AddComponent();

  template <typename T>
  T* GetComponent();

  template <typename T>
  bool HasComponentType();
private:

  std::unordered_map<std::type_index, ComponentHandle> m_components;
};

template <typename T>
T* Object::AddComponent()
{
  // create handle
  auto TypeItter = m_components.find(typeid(T));

  if (TypeItter == m_components.end())
  {
    m_components[typeid(T)] = ComponentHandle();
  }

  return ComponentPoolManager::AddComponentToPool<T>(m_components[typeid(T)]);
}

template <typename T>
T* Object::GetComponent()
{
    //todo(Check): Put Component Check
   if(HasComponentType<T>())
   {
     return ComponentPoolManager::GetComponentFromPool<T>(m_components[typeid(T)]);
   }

 return nullptr;
}

template <typename T>
bool Object::HasComponentType()
{
  auto TypeItter = m_components.find(typeid(T));

  if (TypeItter == m_components.end())
  {
    return false;
  }

  return true;
}
