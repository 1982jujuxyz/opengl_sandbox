#pragma once
#include "Object.h"

class GraphicsEngine;

class Scene
{
public:
	
	Scene() { BuildDefaultScene(); };

	void BuildDefaultScene();
	
	std::vector<Object*> getAllGameObjects() const  { return m_gameObjects; }
	//std::vector<Object*> getRenderObject() const  { return m_renderObjects; }

	Object* AddGameObjectToScene();
	Object* AddRenderObjectToScene();

	template <typename T>
	std::vector<Object*> GetObjectsByComponent();

	Object* GetMainCamera() const { return m_mainCamera; }
	Object* GetSkyBox() const { return m_skyBox; }

	void CreateMainCamera();
	void CreateSkyBox();
	void CreateDefaultLight();


private:

	Object* m_mainCamera;  // current active camera for our scene
	Object* m_skyBox;
	std::vector<Object*> m_gameObjects; //All current game objects in our scene

};

template <typename T>
std::vector<Object*> Scene::GetObjectsByComponent()
{
	std::vector<Object*> l_objectContainer;

	for(Object * l_object : m_gameObjects)
	{
		if(l_object->HasComponentType<T>())
		  l_objectContainer.push_back(l_object);
	}

	return l_objectContainer;
}
