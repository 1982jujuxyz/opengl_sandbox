//lowercase:
//doxygen:
//tablen:
#pragma once

#include <vector>
#include <string>
#include <map>
#include <typeindex>
#include <iostream>



#define UNREFERENCED_PARAMETER(X) (void)X

using ObjectId = long long int;



//#if COMPILING_DLL
//#define DLLEXPORT __declspec(dllexport)
//#else
//#define DLLEXPORT __declspec(dllimport)
//#endif
