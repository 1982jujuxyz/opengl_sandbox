


class Time
{
	
public:
	static void FrameStart() { now = std::chrono::steady_clock::now(); }
	static float DeltaTime() { return std::chrono::duration_cast<std::chrono::microseconds>(now - previous).count() / 1000000.0f; }
	static void FrameEnd() { previous = std::chrono::steady_clock::now(); }
	

	struct FixedTime
	{
		static float FixedDuration() { return fixedDuration.count(); }
		static float Accumulate() { accumulation += fixedDuration.count(); return accumulation;  }
	};

private:
	static std::chrono::steady_clock::time_point now;
	static std::chrono::steady_clock::time_point previous;
	static std::chrono::duration<float, std::ratio<1, 30>> fixedDuration;
	static float accumulation;
};







