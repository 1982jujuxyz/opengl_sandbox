/*!***************************************************************************************
\file       Utilities.cpp
\author     Moore, Michael-Paul
\date       7/31/18
\copyright  All content � 2018-2019 DigiPen (USA) Corporation, all rights reserved.
\par        Project: Boomerang
\brief  Functionality for your utility belt.
*****************************************************************************************/
#include "precompiled.h"
//========Self Include==================================================================//
#define d_objIDLength 12

//========3rd Party Includes============================================================//

#include "Utilities.h"
#pragma comment(lib, "Rpcrt4.lib")
//#include <windows.h>

namespace Utilities 
{

}
std::string ReadInFile(const std::string &path)
{
    std::ifstream file(path, std::ios::in | std::ios::binary);
    std::stringstream buffer;
		buffer << file.rdbuf();
		return  buffer.str();
}
/*!***************************************************************************************
		\brief  Creates new UUID
		\return unsigned long objID - new ID
*****************************************************************************************/
objID getNextID()
{
	UUID l_uuid;
	UuidCreate(&l_uuid);
	return l_uuid.Data1;
}

/*!***************************************************************************************
		\brief  Takes an objID and coverts it to a std::string
		\param p_id - The objId to convert
		\return std::string
*****************************************************************************************/
std::string objIDtoString(objID p_id)
{
	std::string l_objString = std::to_string(p_id);
	if (l_objString.length() < d_objIDLength)
	{
		std::string l_newString;
		int l_appendAmount = d_objIDLength - static_cast<int>(l_objString.length());
		for (int i = 0; i < l_appendAmount; ++i)
		{
			l_newString.append("0");
		}
		l_objString = l_newString.append(l_objString);
	}
	return l_objString;
}

std::chrono::duration<int, std::nano> FPStoFrequency(fps p_fps)
{
	if (p_fps)
	{
		const int nano_seconds_in_a_second = 1000000000;
		const std::chrono::duration<int, std::nano> dur_nano_sec(nano_seconds_in_a_second);
		return dur_nano_sec / p_fps;
	}

	return {};
}
