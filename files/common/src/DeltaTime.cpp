#include "Precompiled.h"
#include "DeltaTime.h"

std::chrono::steady_clock::time_point Time::now;
std::chrono::steady_clock::time_point Time::previous;
std::chrono::duration<float, std::ratio<1, 30>> Time::fixedDuration{ 1 };
float Time::accumulation = 0;