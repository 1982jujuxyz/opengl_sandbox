#include "Precompiled.h"
#include "framework/Application.h"
#include "Engine.h"
#include "Scene.h"
#include "Components/Camera.h"
#include "ComponentPool.h"
#include "Components/Transform.h"
#include "DeltaTime.h"
#include "InputHandler.h"


static const std::filesystem::path c_assetPath = "../../assets/";

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void cursor_position_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
void cursor_enter_callback(GLFWwindow* window, int entered);

void Application::Initialize(int argc, char* argv[], std::string const& title, std::string const& p_assetPath, unsigned width, unsigned height)
{
	m_windowTitle = title;
	m_windowWidth = width;
	m_windowHeight = height;

	// set the callback functions
	m_keyDownCallBack = key_callback;
	m_cursorPositionCallBack = cursor_position_callback;
	m_mouseButtonCallBack = mouse_button_callback;
	m_cursorEnterCallBack = cursor_enter_callback;



	// GLFW

	glfwInit();

	// Hints
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	m_windowHandle = glfwCreateWindow(m_windowWidth, m_windowHeight, m_windowTitle.c_str(), nullptr, nullptr);

	int l_screenWidth, l_screenHeight;
	glfwGetFramebufferSize(m_windowHandle, &l_screenWidth, &l_screenHeight);

	if(m_windowHandle == nullptr)
	{
		glfwTerminate();

		std::cout << "glfwCreateWindow failed\n";
		return;
	}

	glfwMakeContextCurrent(m_windowHandle);

	glewExperimental = GL_TRUE;

	if(GLEW_OK != glewInit())
	{
		std::cout << "GLEW failed\n";
		return;
	}

	glViewport(0, 0, l_screenWidth, l_screenHeight);

	TwInit(TW_OPENGL, nullptr);

	m_engine = new Engine();

	m_engine->Initialize(c_assetPath);
		
}

void Application::Run()
{

	glfwSetKeyCallback(m_windowHandle, key_callback);
	glfwSetCursorPosCallback(m_windowHandle, cursor_position_callback);
	glfwSetMouseButtonCallback(m_windowHandle, mouse_button_callback);
	glfwSetCursorEnterCallback(m_windowHandle, cursor_enter_callback);
	

	while(!glfwWindowShouldClose(m_windowHandle))
	{
		glfwPollEvents();

		//render
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		if(m_engine)
			m_engine->Update();

		glfwSwapBuffers(m_windowHandle);
	}
}

void Application::Close()
{

	m_engine->Shutdown();
}


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	// do exit code here
	InputHandler::RecieveInputFromGLFW(key, scancode, action, mods);
}

void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
	InputHandler::RecieveCursorPositionFromGLFW(xpos, ypos);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if(button == 0)
	{
		if(action)
		{
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		}
		else
		{
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		}
	}


	InputHandler::RecieveMouseButtonFromGLFW(button, action, mods);
}

void cursor_enter_callback(GLFWwindow* window, int entered)
{
	InputHandler::RecieveCursorEnterFromGLFW(entered);
}