#include "Precompiled.h"
#include "framework/Application.h"



static const unsigned c_DefaultWindowWidth = 1280;
static const unsigned c_DefaultWindowHeight = 760;
static const std::string c_BasePath = "../../";

int main(int argc, char * argv[])
{
	Application* app = &Application::GetInstance();

	app->Initialize(argc, argv, "OpenGLSandbox", c_BasePath, c_DefaultWindowWidth, c_DefaultWindowWidth);
	app->Run();

	return 0;
}


