#pragma once
#include "framework/singleton.h"
#include <glew-2.1.0/glew.h>
#include <glfw-3.2.1/glfw3.h>
#include <AntTweakBar-1.16/AntTweakBar.h>

class Engine;

class Application : public Singleton<Application>
{

public:

	using OnKeyDownCallBack = void(*)(GLFWwindow * window, int key, int scancode, int action, int mods);
	using CursorPositionCallBack = void(*)(GLFWwindow * window, double xpos, double ypos);
	using MouseButtonCallBack = void(*)(GLFWwindow * window, int button, int action, int mods);
	using CursorEnterCallBack = void(*)(GLFWwindow * window, int entered);

	Application() = default;

	void Initialize(int argc, char* argv[], std::string const& p_title, std::string const& p_assetPath, unsigned p_width, unsigned p_height);

	void Run();

	void Close();

private:
	

	std::string m_windowTitle;
	std::string m_BasePath;

	unsigned m_windowHeight;
	unsigned m_windowWidth;

	GLFWwindow * m_windowHandle;
	Engine * m_engine;

	OnKeyDownCallBack m_keyDownCallBack = nullptr;
	CursorPositionCallBack m_cursorPositionCallBack = nullptr;
	MouseButtonCallBack m_mouseButtonCallBack = nullptr;
	CursorEnterCallBack m_cursorEnterCallBack = nullptr;

};
