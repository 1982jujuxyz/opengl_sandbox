#version 430 core

#define MaxLights 64 // maximum possible lights this shader supports
#define DIRECTIONAL 0
#define POINT 1
#define SPOT 2

layout(location = 0) out vec4 vFragColor;

// in vec4 WorldNormal;
 in vec3 uvNormal;
 in vec2 UV;
 in vec4 fragPosition;

in vec4 WorldNormal;

//in vec4 WorldPosition;
//in vec2 Uv1;//fixed border line
//in mat4 TBN;
//in vec4 PhongLightingColor;

uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;
uniform sampler2D normalTexture;

uniform samplerCube cubeTexture;
uniform sampler2D simpleTexture;

uniform bool skybox;
uniform bool hasTexture;
// represents material properties of the surface passed by the application

uniform struct
{
  bool receiveLight;
  
  vec4 ambientColor; // how much ambient light to absorb
  vec4 diffuseColor; // how much diffuse light to absorb
  vec4 emissiveColor;
  vec4 specularColor;
  float specularExponent; // shininess  
  
  bool hasDiffuseTexture;
  bool hasSpecularTexture;
  bool hasNormalMapTexture;

}material;

uniform struct
{
   float fogAmmount;
  vec4 position;
  vec4 i_fog;
  float z_near;
  float z_far;
}camera;


struct Light
{
  bool isActive;
  bool ifDecay;
  vec4 position;
  vec4 direction; 
  vec4 ambient;  // ambient light cast onto objects
  vec4 diffuse;  // diffuse light cast onto objects
  float phi;     // innerAngle
  float theta;   // outerAngle
  float falloff; // for spotlight
  

  vec4 specular;
  int type;
  float intensity;
  vec3 disAtten;
};

uniform Light Lights[32]; // support UP TO 32 lights
uniform int lightCount; // # of lights enabled
uniform vec4 globalAmbient;
//uniform vec4 TintColor;
//uniform int ShadingModel;


vec4 CalcILocal(int index, vec4 normal, vec4 V, vec2 uv, bool optimize)
{

  Light light = Lights[index];
 
  vec4 L, R;
  
  //if(optimize)
    //V = normalize((L + V) * .5f); // half angle

  vec4 ambientColor = material.ambientColor;
  vec4 diffuseColor = material.diffuseColor;
  vec4 specularColor = material.specularColor;


  if(material.hasDiffuseTexture)
  {
      ambientColor = texture(diffuseTexture, uv);
      diffuseColor = texture(diffuseTexture, uv);
  }
  if(material.hasSpecularTexture)
  {
    specularColor = texture(specularTexture,uv);
    specularColor *= specularColor;
  }
 
 
  vec4 lightDirection = normalize(light.direction);
  
  float lightDist = length(light.position - fragPosition);
  
  float attFactor = 1;
  float spotlightEffect = 0;



  switch (light.type)
  {

      case DIRECTIONAL:

              L = -lightDirection;

                 // no attenuation 
              attFactor = 1;

                 // no spotlight
              spotlightEffect = 1;

            break;

        case POINT: 

              L = normalize(light.position - fragPosition);

              if(light.ifDecay)
              {
                  // calculate attenuation
                 float denominator =  light.disAtten.x + light.disAtten.y * lightDist + light.disAtten.z * pow(lightDist, 2);
                 attFactor= min(1/denominator, 1);
              }
               

                  // no spotlight
               spotlightEffect = 1;

            break;

        case SPOT:

              L = normalize(light.position - fragPosition);
                  // calculate attenuation
              if(light.ifDecay)
              {
                  float denominator =  light.disAtten.x + light.disAtten.y * lightDist + light.disAtten.z * pow(lightDist, 2);
                  attFactor= min(1/denominator, 1);
              }
               

               
               float alpha = max(dot(lightDirection , -L),0);
                  // spotlight



              if(alpha > cos(light.theta))
                  spotlightEffect = 1;
              else if (alpha < cos(light.phi))
                  spotlightEffect = 0;
              else
                  spotlightEffect = pow((alpha - cos(light.phi))/(cos(light.theta) - cos(light.phi)), light.falloff);

            break;

        default:

                    // no attenuation 
               attFactor = 1;

                // no spotlight
               spotlightEffect = 1;

            break;
      }



      //R = normalize(2*dot(normal, L)*normal - L);

      R = normalize(reflect(-L,normal));

      vec4 Iambient = light.ambient * ambientColor;

      vec4 Idiffuse = light.diffuse * diffuseColor* max(dot(normal,L), 0.1) * attFactor;

      //float cosAngle =  pow(clamp(dot(R,V), 0.0f,1.0f), material.specularExponent);

      vec4 Ispecular =  light.specular * specularColor * pow(clamp(dot(R,V), 0.0f,1.0f), material.specularExponent);

     return light.intensity * (attFactor * Iambient + attFactor * spotlightEffect * (Idiffuse + Ispecular));

    // return    (Idiffuse * dot(R,V));

}

void blinn_shading()
{
   vec4 normal = normalize(WorldNormal);

   vec4 V = normalize(camera.position - fragPosition);

   vec4 color = vec4(0,0,0,1);

   vec2 uv = UV;

   for(int i = 0; i < lightCount; ++i)
      color += CalcILocal(i, normal, V, uv, true);

    color += material.emissiveColor +  globalAmbient  *  material.ambientColor;

    float s = (camera.z_far - distance(camera.position, fragPosition)) / (camera.z_far - camera.z_near);

    vec4 Ifinal = s * color + (1 - s)* camera.i_fog;

   vFragColor = Ifinal;//(normal + 1)/2;
}


void main()
{

//vec4 normal = WorldNormal;
  //vFragColor = normal;

  if(skybox){
     vFragColor = texture(cubeTexture, uvNormal); 
     return;
  }
     
  blinn_shading();
}
